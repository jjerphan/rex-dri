.PHONY: documentation

setup:
	bash server/envs/init.sh

clear_setup:
	rm server/envs/db.env
	rm server/envs/django.env

up: setup
	docker-compose up --build

dev: up

down_dev:
	docker-compose down

init_dev_data:
	docker-compose exec backend sh -c "cd backend && ./manage.py shell < init_dev_data.py"

docker-pull:
	docker-compose pull

reformat_backend:
	docker-compose exec backend sh -c "cd backend && black ."

test_backend:
	docker-compose exec backend sh -c "cd backend && pytest --cov --cov-config .coveragerc --cov-report term   base_app/ backend_app/"

test_frontend:
	docker-compose exec frontend sh -c "cd frontend && npm run test"

check_backend:
	docker-compose exec backend sh -c "cd backend && ./manage.py check"

lint_backend:
	docker-compose exec backend sh -c "cd backend && flake8"

lint_frontend:
	docker-compose exec frontend sh -c "cd frontend && npm run lint"

build_frontend:
	docker-compose exec frontend sh -c "cd frontend && npm run build"

shell_backend:
	docker-compose exec backend sh -c "cd backend && bash"

shell_frontend:
	docker-compose exec frontend sh -c "cd frontend && sh"

django_shell:
	docker-compose exec backend sh -c "cd backend && ./manage.py shell"

# Create or update UML diagrams for the documentation
documentation:
	docker-compose exec backend bash -c "cd documentation && make extract_django"
	docker-compose exec gen_doc_uml bash -c "cd /usr/src/app/documentation && make convert_to_svg"

documentation_clean:
	docker-compose exec backend bash -c "cd documentation && make clean"

prod_yml = -f ./server/docker-compose.prod.yml

prod: setup
	$(info In production, we need to reset the webpack-stats.json file to make sure the front is up-to-date)
	sudo rm -f frontend/webpack-stats.json
	docker-compose $(prod_yml) up --build -d

down_prod:
	docker-compose $(prod_yml) down

prod_docker_logs:
	docker-compose $(prod_yml) logs

shell_prod_logs:
	docker-compose $(prod_yml) exec logs_rotation /bin/sh -c "cd /var/log && /bin/sh"

shell_backend_prod:
	docker-compose $(prod_yml) exec backend sh -c "cd backend && bash"
