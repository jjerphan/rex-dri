FROM nginx:1.15.12-alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY assets.conf /etc/nginx/conf.d/assets.conf
COPY dev.conf /etc/nginx/conf.d/default.conf
