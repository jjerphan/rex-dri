FROM nginx:1.15.12-alpine

# Remove the symbolic linking of nginx logs to stdout :)
RUN rm -rf /var/log/nginx && mkdir /var/log/nginx

WORKDIR /usr/src

COPY uwsgi_params /usr/src/uwsgi_params
COPY nginx.conf /etc/nginx/nginx.conf
COPY assets.conf /etc/nginx/conf.d/assets.conf
COPY prod.conf /etc/nginx/conf.d/default.conf
