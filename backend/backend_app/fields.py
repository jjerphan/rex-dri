# inspired by : https://github.com/devkral/django-simple-jsonfield
# MIT license


from django.core.exceptions import ValidationError
from django.db import models
import json


class JSONField(models.TextField):
    __qualname__ = "JSONField"
    __name__ = "JSONField"
    """ JSON field implementation on top of django textfield """

    def to_dict(self, value):
        """ convert json string to python dictionary """
        try:
            return json.loads(value)
        except json.decoder.JSONDecodeError as e:
            raise ValidationError(message=str(e), code="json_error")

    def to_json(self, value):
        """ convert python dictionary to json string """
        return json.dumps(value)

    def from_db_value(self, value, expression, connection):
        """ convert string from db to python dictionary """
        if value is None:
            return value
        return self.to_dict(value)

    def to_python(self, value):
        """ convert model input value to python dictionary """
        if isinstance(value, (dict, list)):
            return value
        if value is None:
            return value
        return self.to_dict(value)

    def get_prep_value(self, value):
        """convert python dictionary to string before writing to db"""
        return self.to_json(value)
