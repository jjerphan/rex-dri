from django.db import models

from backend_app.models.abstract.base import (
    BaseModel,
    BaseModelSerializer,
    BaseModelViewSet,
)
from backend_app.models.department import Department
from backend_app.permissions.app_permissions import ReadOnly


class Specialty(BaseModel):
    code = models.CharField(max_length=6)
    department = models.ForeignKey(Department, on_delete=models.PROTECT)
    name = models.CharField(max_length=100)
    active = models.BooleanField()

    class Meta:
        unique_together = ("code", "department")


class SpecialtySerializer(BaseModelSerializer):
    class Meta:
        model = Specialty
        fields = "__all__"


class SpecialtyViewSet(BaseModelViewSet):
    queryset = Specialty.objects.all()  # pylint: disable=E1101
    serializer_class = SpecialtySerializer
    permission_classes = (ReadOnly,)
    end_point_route = "specialties"
