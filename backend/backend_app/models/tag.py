from django.db import models

from backend_app.models.abstract.base import (
    BaseModel,
    BaseModelSerializer,
    BaseModelViewSet,
)
from backend_app.permissions.app_permissions import ReadOnly
from backend_app.validation.utils import get_schema_for_tag
from backend_app.validation.validators import TagNameValidator


class Tag(BaseModel):
    """
    Simple model to store available "tags" in the app.

    The schema associated with each tag (name) must be defined in `tags_schemas_collection.json`
    """

    name = models.CharField(
        max_length=100, unique=True, validators=[TagNameValidator()]
    )

    @property
    def schema(self):
        """
        Simple property to return the schema associated with a tag
        :return:
        """
        return get_schema_for_tag(self.name)


class TagSerializer(BaseModelSerializer):
    class Meta:
        model = Tag
        fields = BaseModelSerializer.Meta.fields + ("name", "schema")


class TagViewSet(BaseModelViewSet):
    queryset = Tag.objects.all()  # pylint: disable=E1101
    serializer_class = TagSerializer
    permission_classes = (ReadOnly,)
    end_point_route = "tags"
