from django.conf import settings
from django.db import models
from rest_framework import serializers, viewsets

from backend_app.custom.mySerializerWithJSON import MySerializerWithJSON
from backend_app.permissions.default import DEFAULT_VIEWSET_PERMISSIONS


class BaseModel(models.Model):
    """
    All models in the app inherits from this one.

    As of now, this model doesn't have any special fields.
    It is basically here to have a coherent naming convention. In fact some
    high level behaviors have been implemented in the corresponding Serializer and
    viewset.
    """

    # Look at the documentation about config files to know more about this
    # https://rex-dri.gitlab.utc.fr/rex-dri/documentation/#/Application/Backend/moderation_and_versioning?id=model-level
    # http://localhost:5000/#/Application/Backend/moderation_and_versioning?id=model-level
    moderation_level = settings.DEFAULT_MODEL_MODERATION_LEVEL

    class Meta:
        abstract = True


class BaseModelSerializer(MySerializerWithJSON):
    """
    Serializer to go along the BaseModel model. This serializer make sure some
    relevant data is always returned.
    """

    obj_info = serializers.SerializerMethodField()

    # For easier handling on the client side, we force an id field
    # this is useful when a model has a dedicated primary key
    id = serializers.SerializerMethodField()

    def get_obj_info(self, obj) -> dict:
        """
        Serializer for the `obj_info` *dynamic* field.
        `obj` is required in the function definition, but it's not used.

        For all object return by the backend api, we add a custom `obj_info`
        field. The default value are chown below.

        This methods is overrided in EssentialModuleSerializer for
        a smarter behavior.
        """
        return {"user_can_edit": False, "user_can_moderate": False}

    def get_id(self, obj: BaseModel):
        """
        Serializer for the id field.
        """
        return obj.pk

    def get_user_from_request(self):
        """
        Function to retrieve the user from the request
        """
        return self.context["request"].user

    class Meta:
        model = BaseModel
        fields = ("obj_info", "id")


class BaseModelViewSet(viewsets.ModelViewSet):
    """
    Custom default viewset
    """

    serializer_class = BaseModelSerializer
    permission_classes = None

    # We store the api endpoint route directly in the viewset classes
    # so that we can easily access them
    end_point_route = None

    def get_permissions(self):
        """
        We override the permission getter to make sure we add the default
        app viewsets permissions
        """
        if self.permission_classes is None:
            return [DEFAULT_VIEWSET_PERMISSIONS()]
        else:
            return [
                (DEFAULT_VIEWSET_PERMISSIONS & p)() for p in self.permission_classes
            ]
