from django.db import models

from backend_app.fields import JSONField
from backend_app.models.abstract.versionedEssentialModule import (
    VersionedEssentialModule,
    VersionedEssentialModuleSerializer,
    VersionedEssentialModuleViewSet,
)
from backend_app.validation.validators import UsefulLinksValidator

IMPORTANCE_LEVEL = (("-", "normal"), ("+", "important"), ("++", "IMPORTANT"))

useful_link_validator = UsefulLinksValidator()


class Module(VersionedEssentialModule):
    """
    Abstract module that provides defaults fields:
    Title, comment, useful_links and importance_level

    Those field will be inherited.

    All Basic modules are also "versioned" modules
    """

    title = models.CharField(default="", blank=True, max_length=150)
    comment = models.CharField(default="", blank=True, max_length=5000)
    useful_links = JSONField(default=list, validators=[useful_link_validator])
    importance_level = models.CharField(
        max_length=2, choices=IMPORTANCE_LEVEL, default="-"
    )

    class Meta:
        abstract = True


class ModuleSerializer(VersionedEssentialModuleSerializer):
    """
    Custom serializer that performs checks on the Basic module filed
    """

    class Meta:
        model = Module
        fields = VersionedEssentialModuleSerializer.Meta.fields + (
            "title",
            "comment",
            "useful_links",
            "importance_level",
        )


class ModuleViewSet(VersionedEssentialModuleViewSet):
    """
    Viewset for the Basic Module
    """

    serializer_class = ModuleSerializer
