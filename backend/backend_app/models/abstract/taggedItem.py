from django.db import models

from backend_app.fields import JSONField
from backend_app.models.abstract.module import Module, ModuleSerializer, ModuleViewSet
from backend_app.models.tag import Tag
from backend_app.validation.validators import TaggedItemValidator

VALIDATOR = TaggedItemValidator()


def validate_tagged_item(tag_name, content):
    VALIDATOR(tag_name, content)


class TaggedItem(Module):
    """
    Abstract model to represent a tagged item
    """

    tag = models.ForeignKey(Tag, related_name="+", on_delete=models.PROTECT)
    content = JSONField(default=dict)

    def save(self, *args, **kwargs):
        """
        Custom save function to ensure consistency of the content with the tag.
        """
        validate_tagged_item(self.tag.name, self.content)
        return super().save(*args, **kwargs)

    class Meta:
        abstract = True


class TaggedItemSerializer(ModuleSerializer):
    """
    Serializer for tagged items
    """

    def validate(self, attrs):
        attrs = super().validate(attrs)
        validate_tagged_item(attrs["tag"].name, attrs["content"])
        return attrs


class TaggedItemViewSet(ModuleViewSet):
    """
    Tagged item viewset
    """

    def get_queryset(self):
        """
        Extend the queryset for a bit of optimization
        """
        return super().get_queryset().prefetch_related("tag")
