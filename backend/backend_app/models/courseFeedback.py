from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from backend_app.models.abstract.essentialModule import EssentialModule
from backend_app.models.course import Course
from backend_app.models.language import Language


class CourseFeedback(EssentialModule):
    course = models.OneToOneField(
        Course, on_delete=models.CASCADE, default=0, related_name="course_feedback"
    )
    language = models.ForeignKey(
        Language, on_delete=models.SET_NULL, related_name="courses", null=True
    )
    comment = models.TextField(null=True, max_length=1500)
    adequation = models.IntegerField(
        default=0, validators=[MinValueValidator(-5), MaxValueValidator(5)]
    )
    working_dose = models.IntegerField(
        default=0, validators=[MinValueValidator(-5), MaxValueValidator(5)]
    )
    language_following_ease = models.IntegerField(
        default=0, validators=[MinValueValidator(-5), MaxValueValidator(5)]
    )
