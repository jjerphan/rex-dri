from django.db import models
from rest_framework import serializers

from backend_app.models.abstract.module import Module, ModuleSerializer, ModuleViewSet
from backend_app.models.university import University


def semester_error(semester_name):
    raise serializers.ValidationError(
        "Inconsistent {} semester dates".format(semester_name)
    )


class UniversitySemestersDates(Module):
    university = models.OneToOneField(
        University,
        on_delete=models.CASCADE,
        related_name="university_semesters_dates",
        primary_key=True,
        null=False,
    )

    spring_begin = models.DateField(null=True, blank=True)
    spring_end = models.DateField(null=True, blank=True)
    autumn_begin = models.DateField(null=True, blank=True)
    autumn_end = models.DateField(null=True, blank=True)


class UniversitySemestersDatesSerializer(ModuleSerializer):
    def validate(self, attrs):
        attrs = super().validate(attrs)

        s_b, s_e = attrs["spring_begin"], attrs["spring_end"]
        a_b, a_e = attrs["autumn_begin"], attrs["autumn_end"]

        def test(begin, end, name):
            ok = (begin is None and end is None) or (
                begin is not None and end is not None
            )
            if not ok:
                semester_error(name)
            if begin is not None and (begin > end):
                semester_error(name)

        test(s_b, s_e, "spring")
        test(a_b, a_e, "autumn")

        return attrs

    class Meta:
        model = UniversitySemestersDates
        fields = "__all__"


class UniversitySemestersDatesViewSet(ModuleViewSet):
    queryset = UniversitySemestersDates.objects.all()  # pylint: disable=E1101
    serializer_class = UniversitySemestersDatesSerializer
    end_point_route = "universitiesSemestersDates"
