from django.db import models

from backend_app.models.abstract.taggedItem import (
    TaggedItem,
    TaggedItemSerializer,
    TaggedItemViewSet,
)
from backend_app.models.country import Country


class CountryTaggedItem(TaggedItem):
    country = models.ForeignKey(
        Country, on_delete=models.PROTECT, related_name="country_tagged_items"
    )

    class Meta:
        unique_together = ("country", "tag", "importance_level")


class CountryTaggedItemSerializer(TaggedItemSerializer):
    class Meta:
        model = CountryTaggedItem
        fields = "__all__"


class CountryTaggedItemViewSet(TaggedItemViewSet):
    queryset = CountryTaggedItem.objects.all()  # pylint: disable=E1101
    serializer_class = CountryTaggedItemSerializer
    end_point_route = "countryTaggedItems"
    filterset_fields = ("country",)
