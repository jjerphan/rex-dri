from django.db import models

from backend_app.models.abstract.base import (
    BaseModel,
    BaseModelSerializer,
    BaseModelViewSet,
)
from backend_app.permissions.app_permissions import ReadOnly


# Data model based on : https://unstats.un.org/unsd/methodology/m49/overview/


class Country(BaseModel):
    name = models.CharField(max_length=200)
    iso_alpha2_code = models.CharField(primary_key=True, max_length=2)
    iso_alpha3_code = models.CharField(
        unique=True, max_length=3, default="", blank=True
    )
    region_name = models.CharField(max_length=200)
    region_un_code = models.CharField(max_length=3)
    sub_region_name = models.CharField(max_length=200, default="", blank=True)
    sub_region_un_code = models.CharField(max_length=3, default="", blank=True)
    intermediate_region_name = models.CharField(max_length=200, default="", blank=True)
    intermediate_region_un_code = models.CharField(max_length=3, default="", blank=True)


class CountrySerializer(BaseModelSerializer):
    class Meta:
        model = Country
        fields = "__all__"


class CountryViewSet(BaseModelViewSet):
    queryset = Country.objects.all()  # pylint: disable=E1101
    serializer_class = CountrySerializer
    permission_classes = (ReadOnly,)
    end_point_route = "countries"
