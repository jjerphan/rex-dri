from django.db import models

from backend_app.models.abstract.base import BaseModel
from backend_app.models.exchange import Exchange


class Course(BaseModel):
    exchange = models.ForeignKey(
        Exchange, on_delete=models.CASCADE, related_name="exchange_courses", null=True
    )
    utc_exchange_id = models.IntegerField()
    course_id = models.IntegerField()
    code = models.CharField(max_length=10)
    title = models.CharField(default="", null=True, blank=True, max_length=200)
    link = models.URLField(null=True, blank=True, max_length=500)
    nb_credit = models.PositiveIntegerField(default=0)
    category = models.CharField(null=True, blank=True, max_length=5)
    profile = models.CharField(null=True, blank=True, max_length=10)
    tsh_profile = models.CharField(null=True, blank=True, max_length=21)
    student_login = models.CharField(null=True, blank=True, max_length=8)
