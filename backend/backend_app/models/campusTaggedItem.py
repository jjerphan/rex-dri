from django.db import models

from backend_app.models.abstract.taggedItem import (
    TaggedItem,
    TaggedItemSerializer,
    TaggedItemViewSet,
)
from backend_app.models.campus import Campus


class CampusTaggedItem(TaggedItem):
    campus = models.ForeignKey(
        Campus, on_delete=models.PROTECT, related_name="campus_tagged_items"
    )

    class Meta:
        unique_together = ("campus", "tag", "importance_level")


class CampusTaggedItemSerializer(TaggedItemSerializer):
    class Meta:
        model = CampusTaggedItem
        fields = "__all__"


class CampusTaggedItemViewSet(TaggedItemViewSet):
    queryset = CampusTaggedItem.objects.all()  # pylint: disable=E1101
    serializer_class = CampusTaggedItemSerializer
    end_point_route = "campusTaggedItems"
    filterset_fields = ("campus",)
