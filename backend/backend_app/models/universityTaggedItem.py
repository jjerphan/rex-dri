from django.db import models

from backend_app.models.abstract.taggedItem import (
    TaggedItem,
    TaggedItemSerializer,
    TaggedItemViewSet,
)
from backend_app.models.university import University


class UniversityTaggedItem(TaggedItem):
    university = models.ForeignKey(
        University, on_delete=models.PROTECT, related_name="university_tagged_items"
    )

    class Meta:
        unique_together = ("university", "tag", "importance_level")


class UniversityTaggedItemSerializer(TaggedItemSerializer):
    class Meta:
        model = UniversityTaggedItem
        fields = "__all__"


class UniversityTaggedItemViewSet(TaggedItemViewSet):
    queryset = UniversityTaggedItem.objects.all()  # pylint: disable=E1101
    serializer_class = UniversityTaggedItemSerializer
    # permission_classes = (IsOwner,)
    end_point_route = "universityTaggedItems"
    filterset_fields = ("university",)
