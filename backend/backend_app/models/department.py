from django.db import models

from backend_app.models.abstract.base import (
    BaseModel,
    BaseModelSerializer,
    BaseModelViewSet,
)
from backend_app.permissions.app_permissions import ReadOnly


class Department(BaseModel):
    code = models.CharField(primary_key=True, max_length=6)
    name = models.CharField(max_length=100)
    active = models.BooleanField()


class DepartmentSerializer(BaseModelSerializer):
    class Meta:
        model = Department
        fields = "__all__"


class DepartmentViewSet(BaseModelViewSet):
    queryset = Department.objects.all()  # pylint: disable=E1101
    serializer_class = DepartmentSerializer
    permission_classes = (ReadOnly,)
    end_point_route = "departments"
