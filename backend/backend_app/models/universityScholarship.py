from django.db import models

from backend_app.models.abstract.scholarship import (
    Scholarship,
    ScholarshipSerializer,
    ScholarshipViewSet,
)
from backend_app.models.university import University


class UniversityScholarship(Scholarship):
    universities = models.ManyToManyField(
        University, related_name="university_scholarships"
    )


class UniversityScholarshipSerializer(ScholarshipSerializer):
    class Meta:
        model = UniversityScholarship
        fields = "__all__"


class UniversityScholarshipViewSet(ScholarshipViewSet):
    queryset = UniversityScholarship.objects.all()  # pylint: disable=E1101
    serializer_class = UniversityScholarshipSerializer
    end_point_route = "universityScholarships"
    filterset_fields = ("universities",)
