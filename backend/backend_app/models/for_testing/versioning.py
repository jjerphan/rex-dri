from django.db import models

import reversion

from backend_app.models.abstract.versionedEssentialModule import (
    VersionedEssentialModule,
    VersionedEssentialModuleSerializer,
    VersionedEssentialModuleViewSet,
)


@reversion.register()
class ForTestingVersioning(VersionedEssentialModule):
    """
        Simple model for testing purposes (versioning)
    """

    moderation_level = 1

    bbb = models.CharField(max_length=100)


class ForTestingVersioningSerializer(VersionedEssentialModuleSerializer):
    """
        Simple Serializer for testing purposes (versioning)
    """

    class Meta:
        model = ForTestingVersioning
        fields = "__all__"


class ForTestingVersioningViewSet(VersionedEssentialModuleViewSet):
    """
        Simple Viewset for testing purposes (versioning)
    """

    serializer_class = ForTestingVersioningSerializer
    queryset = ForTestingVersioning.objects.all()
    end_point_route = "test/versioning"
