from django.db import models

from backend_app.models.abstract.essentialModule import (
    EssentialModule,
    EssentialModuleSerializer,
    EssentialModuleViewSet,
)


class ForTestingModeration(EssentialModule):
    """
        Simple model for testing purposes
    """

    moderation_level = 1

    aaa = models.CharField(max_length=100)


class ForTestingModerationSerializer(EssentialModuleSerializer):
    """
        Simple serializer for testing purpose
    """

    class Meta:
        model = ForTestingModeration
        fields = "__all__"


class ForTestingModerationViewSet(EssentialModuleViewSet):
    """
        Simple Viewset for testing purpose
    """

    serializer_class = ForTestingModerationSerializer
    queryset = ForTestingModeration.objects.all()
    end_point_route = "test/moderation"
