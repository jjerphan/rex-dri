from django.db import models

from backend_app.models.abstract.base import (
    BaseModel,
    BaseModelSerializer,
    BaseModelViewSet,
)
from backend_app.models.shared import SEMESTER_OPTIONS
from backend_app.models.specialty import Specialty
from backend_app.models.university import University
from backend_app.permissions.app_permissions import ReadOnly


class Offer(BaseModel):
    university = models.ForeignKey(University, on_delete=models.PROTECT)
    year = models.PositiveIntegerField(default=2018)
    semester = models.CharField(max_length=2, choices=SEMESTER_OPTIONS, default="a")

    nb_seats_offered = models.PositiveIntegerField()
    # null => exchange not possible
    nb_seats_offered_exchange = models.PositiveIntegerField(null=True)
    nb_seats_offered_double_degree = models.PositiveIntegerField(null=True)

    specialties = models.ManyToManyField(Specialty, related_name="has_seats_at_univ")


class OfferSerializer(BaseModelSerializer):
    class Meta:
        model = Offer
        fields = "__all__"


class OfferViewSet(BaseModelViewSet):
    queryset = Offer.objects.all()  # pylint: disable=E1101
    serializer_class = OfferSerializer
    permission_classes = (ReadOnly,)
    end_point_route = "offers"
