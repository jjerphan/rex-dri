from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

from backend_app.models.abstract.module import Module, ModuleSerializer, ModuleViewSet
from backend_app.models.city import City
from backend_app.models.university import University
from backend_app.permissions.app_permissions import ReadOnly


class Campus(Module):
    is_main_campus = models.BooleanField(null=False)
    name = models.CharField(max_length=200, default="", blank=True)
    city = models.ForeignKey(City, on_delete=models.PROTECT, null=False)
    university = models.ForeignKey(
        University,
        on_delete=models.PROTECT,
        null=False,
        related_name="university_campuses",
    )

    lat = models.DecimalField(
        max_digits=10,
        decimal_places=6,
        validators=[MinValueValidator(-85.05112878), MaxValueValidator(85.05112878)],
    )

    lon = models.DecimalField(
        max_digits=10,
        decimal_places=6,
        validators=[MinValueValidator(-180), MaxValueValidator(180)],
    )

    def location(self):
        return {"lat": self.lat, "lon": self.lon}

    class Meta:
        unique_together = ("is_main_campus", "university")


class CampusSerializer(ModuleSerializer):
    class Meta:
        model = Campus
        fields = "__all__"


class CampusViewSet(ModuleViewSet):
    queryset = Campus.objects.all()  # pylint: disable=E1101
    serializer_class = CampusSerializer
    end_point_route = "campuses"


class MainCampusViewSet(ModuleViewSet):
    queryset = Campus.objects.filter(is_main_campus=True)
    serializer_class = CampusSerializer
    permission_classes = (ReadOnly,)
    end_point_route = "mainCampuses"
