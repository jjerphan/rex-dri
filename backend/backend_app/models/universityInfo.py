from django.core.validators import MinValueValidator
from django.db import models

from backend_app.models.abstract.module import Module, ModuleSerializer, ModuleViewSet
from backend_app.models.currency import Currency
from backend_app.models.university import University


class UniversityInfo(Module):
    university = models.OneToOneField(
        University,
        on_delete=models.CASCADE,
        related_name="university_info",
        primary_key=True,
        null=False,
    )

    cost_exchange = models.DecimalField(
        decimal_places=2, max_digits=20, validators=[MinValueValidator(0)], null=True
    )

    cost_double_degree = models.DecimalField(
        decimal_places=2, max_digits=20, validators=[MinValueValidator(0)], null=True
    )

    costs_currency = models.ForeignKey(Currency, on_delete=models.PROTECT, null=True)


class UniversityInfoSerializer(ModuleSerializer):
    class Meta:
        model = UniversityInfo
        fields = "__all__"


class UniversityInfoViewSet(ModuleViewSet):
    queryset = UniversityInfo.objects.all()  # pylint: disable=E1101
    serializer_class = UniversityInfoSerializer
    # permission_classes = (ReadOnly,)
    end_point_route = "universityInfo"
