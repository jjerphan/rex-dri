from django.db import models

from backend_app.models.abstract.scholarship import (
    Scholarship,
    ScholarshipSerializer,
    ScholarshipViewSet,
)
from backend_app.models.country import Country


class CountryScholarship(Scholarship):
    countries = models.ManyToManyField(Country, related_name="country_scholarships")


class CountryScholarshipSerializer(ScholarshipSerializer):
    class Meta:
        model = CountryScholarship
        fields = "__all__"


class CountryScholarshipViewSet(ScholarshipViewSet):
    queryset = CountryScholarship.objects.all()  # pylint: disable=E1101
    serializer_class = CountryScholarshipSerializer
    end_point_route = "countryScholarships"
    filterset_fields = ("countries",)
