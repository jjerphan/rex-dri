from django.db import models

from backend_app.models.abstract.base import BaseModel
from backend_app.models.shared import SEMESTER_OPTIONS
from backend_app.models.university import University
from base_app.models import User


class Exchange(BaseModel):
    # This model should be filled with data from the ENT
    utc_univ_id = models.ForeignKey(University, on_delete=models.PROTECT)
    utc_departure_id = models.IntegerField()
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    year = models.PositiveIntegerField(default=2018)
    semester = models.CharField(max_length=5, choices=SEMESTER_OPTIONS, default="a")
    duration = models.PositiveIntegerField()
    dual_degree = models.BooleanField()
    master_obtained = models.BooleanField()
    student_major = models.CharField(max_length=20)
    student_minor = models.CharField(max_length=7)
    student_option = models.CharField(max_length=7)

    utc_allow_courses = models.BooleanField()
    utc_allow_login = models.BooleanField()
