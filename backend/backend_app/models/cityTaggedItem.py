from django.db import models

from backend_app.models.abstract.taggedItem import (
    TaggedItem,
    TaggedItemSerializer,
    TaggedItemViewSet,
)
from backend_app.models.city import City


class CityTaggedItem(TaggedItem):
    city = models.ForeignKey(
        City, on_delete=models.PROTECT, related_name="city_tagged_items"
    )

    class Meta:
        unique_together = ("city", "tag", "importance_level")


class CityTaggedItemSerializer(TaggedItemSerializer):
    class Meta:
        model = CityTaggedItem
        fields = "__all__"


class CityTaggedItemViewSet(TaggedItemViewSet):
    queryset = CityTaggedItem.objects.all()  # pylint: disable=E1101
    serializer_class = CityTaggedItemSerializer
    end_point_route = "cityTaggedItems"
    filterset_fields = ("city",)
