from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

from backend_app.models.abstract.essentialModule import (
    EssentialModule,
    EssentialModuleSerializer,
    EssentialModuleViewSet,
)
from backend_app.models.exchange import Exchange
from backend_app.serializers import ExchangeSerializer
from backend_app.models.university import University


class ExchangeFeedback(EssentialModule):
    university = models.ForeignKey(University, on_delete=models.PROTECT, default=0)
    exchange = models.OneToOneField(
        Exchange,
        on_delete=models.CASCADE,
        related_name="feedbacks",
        primary_key=True,
        null=False,
        default=0,
    )
    general_comment = models.TextField(null=True, max_length=1500)
    academical_level_appreciation = models.IntegerField(
        validators=[MinValueValidator(-5), MaxValueValidator(5)]
    )
    foreign_student_welcome = models.PositiveIntegerField(
        validators=[MaxValueValidator(10)]
    )
    cultural_interest = models.PositiveIntegerField(validators=[MaxValueValidator(10)])


class ExchangeFeedbackSerializer(EssentialModuleSerializer):
    exchange = ExchangeSerializer(read_only=True)

    class Meta:
        model = ExchangeFeedback
        fields = "__all__"


class ExchangeFeedbackViewSet(EssentialModuleViewSet):
    queryset = ExchangeFeedback.objects.all().prefetch_related(
        "exchange",
        "exchange__exchange_courses",
        "exchange__exchange_courses__course_feedback",
    )  # pylint: disable=E1101
    serializer_class = ExchangeFeedbackSerializer
    end_point_route = "exchangeFeedbacks"
    filterset_fields = ("university",)
