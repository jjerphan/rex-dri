from django.db import models

from backend_app.models.abstract.base import (
    BaseModel,
    BaseModelSerializer,
    BaseModelViewSet,
)
from backend_app.models.country import Country
from backend_app.permissions.app_permissions import ReadOnly, IsStaff


class City(BaseModel):
    name = models.CharField(max_length=200)
    local_name = models.CharField(max_length=200, default="", blank=True)
    # We add an area to distinguish similarly named cities
    # in a country
    area = models.CharField(max_length=200, default="", blank=True)
    country = models.ForeignKey(Country, on_delete=models.PROTECT)


class CitySerializer(BaseModelSerializer):
    class Meta:
        model = City
        fields = "__all__"


class CityViewSet(BaseModelViewSet):
    queryset = City.objects.all()  # pylint: disable=E1101
    serializer_class = CitySerializer
    permission_classes = (IsStaff | ReadOnly,)
    end_point_route = "cities"
