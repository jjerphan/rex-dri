from django.db import models

from backend_app.models.abstract.module import Module, ModuleSerializer, ModuleViewSet
from backend_app.models.country import Country
from backend_app.permissions.app_permissions import IsStaff, IsDri, NoPost


class CountryDri(Module):
    countries = models.ManyToManyField(Country, related_name="country_dri")


class CountryDriSerializer(ModuleSerializer):
    class Meta:
        model = CountryDri
        fields = "__all__"


class CountryDriViewSet(ModuleViewSet):
    queryset = CountryDri.objects.all()  # pylint: disable=E1101
    serializer_class = CountryDriSerializer
    permission_classes = (IsStaff | IsDri | NoPost,)
    end_point_route = "countryDri"
    filterset_fields = ("countries",)
