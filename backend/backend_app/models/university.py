from django.conf import settings
from django.db import models

from backend_app.models.abstract.essentialModule import (
    EssentialModule,
    EssentialModuleSerializer,
    EssentialModuleViewSet,
)
from backend_app.validation.validators import PathExtensionValidator


class University(EssentialModule):
    """
    Model storing information about universities
    """

    name = models.CharField(max_length=200)
    acronym = models.CharField(max_length=20, default="", blank=True)
    logo = models.URLField(
        default="",
        blank=True,
        validators=[PathExtensionValidator(settings.ALLOWED_PHOTOS_EXTENSION)],
    )
    website = models.URLField(default="", blank=True, max_length=300)
    utc_id = models.IntegerField(unique=True)


class UniversitySerializer(EssentialModuleSerializer):
    class Meta:
        model = University
        exclude = ("utc_id",)


class UniversityViewSet(EssentialModuleViewSet):
    serializer_class = UniversitySerializer
    queryset = University.objects.all()  # pylint: disable=E1101
    end_point_route = "universities"
