from django.db import models
from rest_framework import serializers
from rest_framework.response import Response

from backend_app.models.abstract.base import (
    BaseModel,
    BaseModelSerializer,
    BaseModelViewSet,
)
from backend_app.permissions.app_permissions import NoDelete, IsStaff, IsOwner, ReadOnly
from backend_app.validation.validators import ImageValidator
from base_app.models import User


#########
# Models
#########


class AbstractFile(BaseModel):
    owner = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    file = models.FileField(upload_to="files/%Y/%m/%d/", blank=True, null=True)
    title = models.CharField(max_length=200, default="", blank=True, null=True)
    licence = models.CharField(max_length=100, default="", blank=True, null=True)
    description = models.CharField(max_length=500, default="", blank=True, null=True)

    class Meta:
        abstract = True


class File(AbstractFile):
    pass


class Picture(AbstractFile):
    file = models.FileField(
        upload_to="pictures/%Y/%m/%d/",
        blank=True,
        null=True,
        validators=[ImageValidator()],
    )


#########
# Serializers
#########


class FileSerializer(BaseModelSerializer):
    owner = serializers.StringRelatedField(read_only=True)

    def save(self, *args, **kwargs):
        instance = super().save(*args, **kwargs)
        instance.owner = self.get_user_from_request()
        instance.save()
        return instance

    class Meta:
        model = File
        fields = BaseModelSerializer.Meta.fields + (
            "owner",
            "file",
            "title",
            "licence",
            "description",
        )


class PictureSerializer(FileSerializer):
    class Meta:
        model = Picture
        fields = FileSerializer.Meta.fields


class FileSerializerFileReadOnly(FileSerializer):
    file = serializers.FileField(read_only=True)


class PictureSerializerFileReadOnly(PictureSerializer):
    file = serializers.FileField(read_only=True)


#########
# ViewSets
#########


class BaseFileViewSet(BaseModelViewSet):
    _serializer_not_read_only = None
    _serializer_read_only = None

    def get_serializer_class(self):
        """
        Custom get serializer to make file field readonly after it has been created
        """
        if hasattr(self, "request") and self.request.method == "PUT":
            return self._serializer_read_only
        else:
            return self._serializer_not_read_only

    def list(self, request, *args, **kwargs):
        # Prevent the querying of all objects.
        return Response(list())

    permission_classes = (NoDelete | IsStaff, IsOwner | IsStaff | ReadOnly)


class FileViewSet(BaseModelViewSet):
    _serializer_not_read_only = PictureSerializer
    _serializer_read_only = PictureSerializer
    queryset = File.objects.all()
    end_point_route = "files"


class PictureViewSet(BaseFileViewSet):
    _serializer_not_read_only = PictureSerializer
    _serializer_read_only = PictureSerializer
    queryset = Picture.objects.all()
    end_point_route = "pictures"
