from django.contrib import admin
from reversion_compare.admin import CompareVersionAdmin

from backend_app.models.abstract.versionedEssentialModule import (
    VersionedEssentialModule,
)
from backend_app.models.campus import Campus
from backend_app.models.campusTaggedItem import CampusTaggedItem
from backend_app.models.city import City
from backend_app.models.cityTaggedItem import CityTaggedItem
from backend_app.models.country import Country
from backend_app.models.countryDri import CountryDri
from backend_app.models.countryScholarship import CountryScholarship
from backend_app.models.countryTaggedItem import CountryTaggedItem
from backend_app.models.course import Course
from backend_app.models.courseFeedback import CourseFeedback
from backend_app.models.currency import Currency
from backend_app.models.department import Department
from backend_app.models.exchange import Exchange
from backend_app.models.exchangeFeedback import ExchangeFeedback
from backend_app.models.for_testing.moderation import ForTestingModeration
from backend_app.models.for_testing.versioning import ForTestingVersioning
from backend_app.models.offer import Offer
from backend_app.models.pendingModeration import PendingModeration
from backend_app.models.recommendationList import RecommendationList
from backend_app.models.specialty import Specialty
from backend_app.models.tag import Tag
from backend_app.models.university import University
from backend_app.models.universityDri import UniversityDri
from backend_app.models.universityInfo import UniversityInfo
from backend_app.models.universityScholarship import UniversityScholarship
from backend_app.models.universitySemestersDates import UniversitySemestersDates
from backend_app.models.universityTaggedItem import UniversityTaggedItem
from backend_app.models.userData import UserData
from backend_app.models.version import Version

ALL_MODELS = [
    Campus,
    CampusTaggedItem,
    City,
    CityTaggedItem,
    Country,
    CountryDri,
    CountryScholarship,
    CountryTaggedItem,
    Course,
    CourseFeedback,
    Currency,
    Department,
    Offer,
    PendingModeration,
    Exchange,
    ExchangeFeedback,
    RecommendationList,
    Specialty,
    Tag,
    University,
    UniversityDri,
    UniversityInfo,
    UniversityScholarship,
    UniversitySemestersDates,
    UniversityTaggedItem,
    UserData,
    Version,
]

# We also register testing to models to make sure migrations are created for them

ALL_MODELS += [ForTestingModeration, ForTestingVersioning]

CLASSIC_MODELS = filter(
    lambda m: not issubclass(m, VersionedEssentialModule), ALL_MODELS
)
VERSIONED_MODELS = filter(lambda m: issubclass(m, VersionedEssentialModule), ALL_MODELS)

#######
# Register the models
#######

for Model in CLASSIC_MODELS:
    # Register the model in the admin in a standard way
    admin.site.register(Model)

for Model in VERSIONED_MODELS:
    # Register the model in the admin with versioning
    admin.site.register(Model, CompareVersionAdmin)
