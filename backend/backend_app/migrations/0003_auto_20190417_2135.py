# Generated by Django 2.1.7 on 2019-04-17 19:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [("backend_app", "0002_auto_20190417_2125")]

    operations = [
        migrations.RenameField(
            model_name="campustaggeditem", old_name="custom_content", new_name="content"
        ),
        migrations.RenameField(
            model_name="citytaggeditem", old_name="custom_content", new_name="content"
        ),
        migrations.RenameField(
            model_name="countrytaggeditem",
            old_name="custom_content",
            new_name="content",
        ),
        migrations.RenameField(
            model_name="universitytaggeditem",
            old_name="custom_content",
            new_name="content",
        ),
    ]
