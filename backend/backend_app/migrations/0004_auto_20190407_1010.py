# Generated by Django 2.1.7 on 2019-04-07 08:10

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [("backend_app", "0003_merge_20190406_1636")]

    operations = [
        migrations.RenameField(
            model_name="coursefeedback", old_name="work_dose", new_name="working_dose"
        ),
        migrations.RemoveField(model_name="course", name="description"),
        migrations.RemoveField(model_name="course", name="has_pending_moderation"),
        migrations.RemoveField(model_name="course", name="moderated_by"),
        migrations.RemoveField(model_name="course", name="moderated_on"),
        migrations.RemoveField(model_name="course", name="obj_moderation_level"),
        migrations.RemoveField(model_name="course", name="updated_by"),
        migrations.RemoveField(model_name="course", name="updated_on"),
        migrations.RemoveField(model_name="coursefeedback", name="departure"),
        migrations.RemoveField(model_name="exchange", name="has_pending_moderation"),
        migrations.RemoveField(model_name="exchange", name="is_anonymous"),
        migrations.RemoveField(model_name="exchange", name="moderated_by"),
        migrations.RemoveField(model_name="exchange", name="moderated_on"),
        migrations.RemoveField(model_name="exchange", name="obj_moderation_level"),
        migrations.RemoveField(model_name="exchange", name="updated_by"),
        migrations.RemoveField(model_name="exchange", name="updated_on"),
        migrations.RemoveField(model_name="exchangefeedback", name="departure"),
        migrations.RemoveField(model_name="exchangefeedback", name="id"),
        migrations.AddField(
            model_name="course",
            name="category",
            field=models.CharField(blank=True, max_length=5, null=True),
        ),
        migrations.AddField(
            model_name="coursefeedback",
            name="course",
            field=models.ForeignKey(
                default=0,
                on_delete=django.db.models.deletion.CASCADE,
                to="backend_app.Course",
            ),
        ),
        migrations.AddField(
            model_name="coursefeedback",
            name="university",
            field=models.ForeignKey(
                default=0,
                on_delete=django.db.models.deletion.PROTECT,
                to="backend_app.University",
            ),
        ),
        migrations.AddField(
            model_name="exchangefeedback",
            name="exchange",
            field=models.OneToOneField(
                default=0,
                on_delete=django.db.models.deletion.CASCADE,
                primary_key=True,
                related_name="feedbacks",
                serialize=False,
                to="backend_app.Exchange",
            ),
        ),
        migrations.AddField(
            model_name="exchangefeedback",
            name="university",
            field=models.ForeignKey(
                default=0,
                on_delete=django.db.models.deletion.PROTECT,
                to="backend_app.University",
            ),
        ),
        migrations.AlterField(
            model_name="exchange",
            name="semester",
            field=models.CharField(
                choices=[("a", "autumn"), ("p", "spring")], default="a", max_length=5
            ),
        ),
        migrations.AlterField(
            model_name="exchange",
            name="student_major",
            field=models.CharField(max_length=20),
        ),
        migrations.AlterField(
            model_name="exchange",
            name="student_minor",
            field=models.CharField(max_length=7),
        ),
        migrations.AlterField(
            model_name="exchange",
            name="student_option",
            field=models.CharField(max_length=7),
        ),
        migrations.AlterField(
            model_name="exchange",
            name="user",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to=settings.AUTH_USER_MODEL,
            ),
        ),
    ]
