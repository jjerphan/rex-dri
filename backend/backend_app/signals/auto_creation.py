from backend_app.models.course import Course
from backend_app.models.courseFeedback import CourseFeedback
from backend_app.models.university import University
from backend_app.models.universityInfo import UniversityInfo
from backend_app.models.universitySemestersDates import UniversitySemestersDates
from backend_app.models.userData import UserData

from django.db.models.signals import post_save

from base_app.models import User


def create_univ_modules(sender, instance, created, **kwargs):
    if created:
        UniversityInfo.objects.create(university=instance)
        UniversitySemestersDates.objects.create(university=instance)


def create_user_modules(sender, instance, created, **kwargs):
    if created:
        UserData.objects.create(owner=instance)


def create_course_feedback_modules(sender, instance, created, **kwargs):
    if created:
        CourseFeedback.objects.create(course=instance)


post_save.connect(create_univ_modules, sender=University)
post_save.connect(create_user_modules, sender=User)
post_save.connect(create_course_feedback_modules, sender=Course)
