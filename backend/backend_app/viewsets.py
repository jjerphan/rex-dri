import logging

from django.conf import settings
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from backend_app.checks import check_viewsets
from backend_app.models.abstract.essentialModule import EssentialModuleViewSet
from backend_app.models.campus import CampusViewSet, MainCampusViewSet
from backend_app.models.campusTaggedItem import CampusTaggedItemViewSet
from backend_app.models.city import CityViewSet
from backend_app.models.cityTaggedItem import CityTaggedItemViewSet
from backend_app.models.country import CountryViewSet
from backend_app.models.countryDri import CountryDriViewSet
from backend_app.models.countryScholarship import CountryScholarshipViewSet
from backend_app.models.countryTaggedItem import CountryTaggedItemViewSet
from backend_app.models.courseFeedback import CourseFeedback
from backend_app.models.currency import CurrencyViewSet
from backend_app.models.department import DepartmentViewSet
from backend_app.models.exchangeFeedback import ExchangeFeedbackViewSet
from backend_app.models.file_picture import FileViewSet, PictureViewSet
from backend_app.models.for_testing.moderation import ForTestingModerationViewSet
from backend_app.models.for_testing.versioning import ForTestingVersioningViewSet
from backend_app.models.language import LanguageViewSet
from backend_app.models.offer import OfferViewSet
from backend_app.models.pendingModeration import (
    PendingModerationViewSet,
    PendingModerationObjViewSet,
)
from backend_app.models.recommendationList import (
    RecommendationListViewSet,
    RecommendationList,
)
from backend_app.models.specialty import SpecialtyViewSet
from backend_app.models.tag import TagViewSet
from backend_app.models.university import UniversityViewSet
from backend_app.models.universityDri import UniversityDriViewSet
from backend_app.models.universityInfo import UniversityInfoViewSet
from backend_app.models.universityScholarship import UniversityScholarshipViewSet
from backend_app.models.universitySemestersDates import UniversitySemestersDatesViewSet
from backend_app.models.universityTaggedItem import UniversityTaggedItemViewSet
from backend_app.models.userData import UserDataViewSet
from backend_app.models.version import VersionViewSet
from backend_app.permissions.app_permissions import ReadOnly, IsStaff
from backend_app.serializers import CourseFeedbackSerializer
from backend_app.settings.defaults import OBJ_MODERATION_PERMISSIONS
from base_app.models import UserViewset, User


class CourseFeedbackViewSet(EssentialModuleViewSet):
    queryset = CourseFeedback.objects.all().prefetch_related()  # pylint: disable=E1101
    serializer_class = CourseFeedbackSerializer
    end_point_route = "courseFeedbacks"


ALL_API_VIEWSETS = [
    UserViewset,
    CampusViewSet,
    MainCampusViewSet,
    CampusTaggedItemViewSet,
    CityViewSet,
    CityTaggedItemViewSet,
    CountryViewSet,
    CountryDriViewSet,
    CountryScholarshipViewSet,
    CountryTaggedItemViewSet,
    CourseFeedbackViewSet,
    CurrencyViewSet,
    DepartmentViewSet,
    OfferViewSet,
    LanguageViewSet,
    PendingModerationViewSet,
    PendingModerationObjViewSet,
    FileViewSet,
    PictureViewSet,
    ExchangeFeedbackViewSet,
    RecommendationListViewSet,
    SpecialtyViewSet,
    TagViewSet,
    UniversityViewSet,
    UniversityDriViewSet,
    UniversityInfoViewSet,
    UniversityScholarshipViewSet,
    UniversitySemestersDatesViewSet,
    UniversityTaggedItemViewSet,
    UserDataViewSet,
    VersionViewSet,
]

if settings.TESTING:
    # We only register viewsets in a testing environment
    ALL_API_VIEWSETS += [ForTestingModerationViewSet, ForTestingVersioningViewSet]


class AppModerationStatusViewSet(ViewSet):
    """
    Viewset to know what is the app moderation status
    """

    # Since AppModerationStatusViewSet doesn't inherit from BaseModelViewSet
    # We need to link here the correct permissions
    permission_classes = (ReadOnly,)
    end_point_route = "serverModerationStatus"

    def list(self, request):
        return Response(
            {
                "activated": settings.MODERATION_ACTIVATED,
                "moderator_level": OBJ_MODERATION_PERMISSIONS["moderator"],
            }
        )


class LogFrontendErrorsViewSet(ViewSet):
    """
    Viewset to handle the logging of errors coming from the frontend.
    """

    permission_classes = tuple()
    end_point_route = "frontendErrors"

    def create(self, request):
        logger = logging.getLogger("frontend")
        data = request.data
        if "componentStack" in data.keys():
            logger.error(request.data["componentStack"])
        else:
            logger.error(request.data)
        return Response(status=201)


class BannedUserViewSet(ViewSet):
    """
    Viewset to be able to ban and un-ban users from the site
    """

    end_point_route = "banned_users"
    permission_classes = (IsStaff,)

    def list(self, request, **kwargs):
        return Response(
            [
                dict(user_id=user.pk, user_login=user.username)
                for user in User.objects.filter(is_banned=True)
            ]
        )

    def update(self, request, pk=None):
        if pk is None:
            return Response(status=403)

        user = User.objects.get(pk=pk)
        if user.is_staff:
            # Prevent ban of admin users
            return Response(status=403)
        user.is_banned = True
        user.save()
        return Response(status=200)

    def delete(self, request, pk=None):
        if pk is None:
            return Response(status=403)

        user = User.objects.get(pk=pk)
        user.is_banned = False
        user.save()
        return Response(status=200)


class DeleteUserViewSet(ViewSet):
    """
    Viewset to handle account deletion
    """

    end_point_route = "emptyUserAccount"
    permission_classes = tuple()

    def create(self, request):
        """
        Line up the user from the request for deletion
        """
        user = request.user
        user.delete_next_time = True
        user.save()
        return Response(status=201)

    def update(self, request):
        # Here only to have the correct routes automatically generated, not to be used.
        return Response(status=403)

    def delete(self, request, pk="osef"):  # don't delete this unused argument!
        """
        Un-Line up the user from the request for deletion
        """
        user = request.user
        user.delete_next_time = False
        user.save()
        return Response(status=200)


class RecommendationListChangeFollowerViewSet(ViewSet):
    """
    Viewset to be able to add or delete followers on
    a recommendation list
    """

    # Since RecommendationListChangeFollowerViewSet doesn't inherit from BaseModelViewSet
    # We need to link here the correct permissions
    end_point_route = "recommendationListChangeFollower"
    permission_classes = tuple()

    def update(self, request, pk=None):
        if pk is None:
            return Response(status=403)

        recommendation = RecommendationList.objects.get(pk=pk)
        if recommendation.is_public:
            recommendation.followers.add(request.user)
            recommendation.save()
            return Response(status=200)
        else:
            return Response(status=403)

    def delete(self, request, pk=None):
        if pk is None:
            return Response(status=403)
        # can delete folower even if list not public
        recommendation = RecommendationList.objects.get(pk=pk)
        recommendation.followers.remove(request.user)
        recommendation.save()
        return Response(status=200)


ALL_API_VIEW_VIEWSETS = [
    AppModerationStatusViewSet,
    LogFrontendErrorsViewSet,
    BannedUserViewSet,
    RecommendationListChangeFollowerViewSet,
    DeleteUserViewSet,
]

ALL_VIEWSETS = ALL_API_VIEWSETS + ALL_API_VIEW_VIEWSETS

check_viewsets(ALL_VIEWSETS)
