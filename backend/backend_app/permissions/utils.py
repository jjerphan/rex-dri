class Request(object):
    def __init__(self, user, method):
        self.user = user
        self.method = method


class FakeUser(object):
    is_authenticated = True

    def __init__(self, cached_groups, is_staff=False):
        self.cached_groups = cached_groups
        self.is_staff = is_staff
