#
#
# WARNING: don't merge this in utils, you would have cyclic imports errors
#
#


from django.conf import settings

from backend_app.settings.defaults import OBJ_MODERATION_PERMISSIONS
from backend_app.utils import get_user_level


def is_moderation_required(model, obj_in_db, user, user_level=None) -> bool:
    """
    Function to tell if moderation is required for obj instance given a user.

    If obj_in_db is None, it means that the obj hasn't been created yet.
    """

    # 0: check that we are giving the function correct parameters
    if obj_in_db is not None:
        assert type(obj_in_db) == model

    # First we retrieve the model moderation level
    model_moderation_level = model.moderation_level

    if model_moderation_level == 0:
        return False
    else:
        if user_level is None:
            user_level = get_user_level(user)

        # At this point we have to check the obj_moderation_level
        if obj_in_db is not None:
            obj_moderation_level = obj_in_db.obj_moderation_level
            if user_level < obj_moderation_level:
                return True

        if model_moderation_level == 1:
            if settings.MODERATION_ACTIVATED:
                return not user_level >= OBJ_MODERATION_PERMISSIONS["moderator"]
            else:
                return False
        elif model_moderation_level == 2:
            return not user_level >= OBJ_MODERATION_PERMISSIONS["moderator"]
        else:
            raise Exception("No other moderation level should be defined...")
