from django.apps import AppConfig


class BackendAppConfig(AppConfig):
    name = "backend_app"

    def ready(self):
        import backend_app.signals.squash_revisions  # noqa:F401
        import backend_app.signals.auto_creation  # noqa:F401
        import backend_app.signals.update_nb_version  # noqa:F401
