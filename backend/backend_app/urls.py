from django.conf.urls import include, url
from rest_framework import routers
from rest_framework.documentation import include_docs_urls

from backend_app.viewsets import ALL_API_VIEWSETS, ALL_API_VIEW_VIEWSETS

#######
# Building the API routing
#######

urlpatterns = [url(r"^api-doc/", include_docs_urls(title="REX-DRI API"))]

# router will hold all api related endpoints
router = routers.DefaultRouter()

for v in ALL_API_VIEWSETS + ALL_API_VIEW_VIEWSETS:
    router.register(v.end_point_route, v, str(v))

# Add all the endpoints for the base api
urlpatterns.append(url(r"^api/", include(router.urls)))
