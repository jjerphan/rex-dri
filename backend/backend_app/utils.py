import json
import re
from os.path import join

from backend_app.settings.defaults import OBJ_MODERATION_PERMISSIONS
from base_app.settings.dir_locations import REPO_ROOT_DIR


def get_user_level(user) -> int:
    """
    Returns the user level as int.
    """
    if user.is_staff:
        return OBJ_MODERATION_PERMISSIONS["staff"]
    elif is_member("DRI", user):
        return OBJ_MODERATION_PERMISSIONS["DRI"]
    elif is_member("Moderators", user):
        return OBJ_MODERATION_PERMISSIONS["moderator"]
    else:
        return OBJ_MODERATION_PERMISSIONS["authenticated_user"]


def is_member(group_name: str, user) -> bool:
    """
    Function to know if a user is part of a specific group.
    """
    return group_name in user.cached_groups
    # before:
    # When we were using the standard django model
    # return user.groups.filter(name=group_name).exists()


def clean_route(route):
    """
    Function to clean the route as it is stored in the viewsets.

    :param route: string
    :return: string
    """

    # Remove required parameters
    out = re.sub(r"\(.*\)", "", route)
    # Clean the string
    out = out.replace("//", "/")
    return out.rstrip("/")


def get_default_theme_settings():
    with open(join(REPO_ROOT_DIR, "frontend/src/config/defaultTheme.json"), "r") as f:
        return json.load(f)
