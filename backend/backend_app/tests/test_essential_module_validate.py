from django.core.exceptions import ValidationError
from django.test import TestCase

import pytest
from backend_app.models.abstract.essentialModule import validate_obj_model_lv
from backend_app.settings.defaults import OBJ_MODERATION_PERMISSIONS


class BaseModelTestCase(TestCase):
    def test_essential_module_validation(self):
        with pytest.raises(ValidationError):
            value = "bernard"
            validate_obj_model_lv(value)

        for key in OBJ_MODERATION_PERMISSIONS:
            validate_obj_model_lv(OBJ_MODERATION_PERMISSIONS[key])
