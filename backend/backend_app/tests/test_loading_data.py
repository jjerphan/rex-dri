from django.test import TestCase
from backend_app.load_data.load_all import load_all


class ModerationTestCase(TestCase):
    def test_everything_loads(self):
        try:
            load_all()
        except Exception:
            self.fail("load_all() raised an Exception unexpectedly!")
