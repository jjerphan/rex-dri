from os.path import join

import pytest
from django.core.exceptions import ValidationError

from backend_app.validation.validators import (
    UsefulLinksValidator,
    PathExtensionValidator,
    TagNameValidator,
    PhotosValidator,
    TaggedItemValidator,
    ThemeValidator,
    ImageValidator,
    RecommendationListJsonContentValidator,
)
from base_app.settings.dir_locations import BACKEND_ROOT_DIR


class TestUsefulLinksValidator(object):
    validator = UsefulLinksValidator()

    def test_empty_array_ok(self):
        data = []
        self.validator(data)

    def test_empty_object_not_ok(self):
        data = {}
        with pytest.raises(ValidationError):
            self.validator(data)

    def test_one_ok(self):
        data = [{"description": "Coucou", "url": "https://utc.fr"}]
        self.validator(data)

    def test_fails_with_more_data(self):
        data = [{"description": "Coucou", "url": "https://utc.fr", "lolilol": True}]
        with pytest.raises(ValidationError):
            self.validator(data)

    def test_not_url(self):
        data = [{"description": "Coucou", "url": "utc"}]
        with pytest.raises(ValidationError):
            self.validator(data)


class TestThemeValidator(object):
    validator = ThemeValidator()

    @staticmethod
    def build(mode, ligth_primary, light_secondary, dark_primary, dark_secondary):
        return dict(
            mode=mode,
            light=dict(primary=ligth_primary, secondary=light_secondary),
            dark=dict(primary=dark_primary, secondary=dark_secondary),
        )

    def test_ok(self):
        data = self.build("dark", "#123456", "#123456", "#123456", "#123456")
        self.validator(data)

    def test_ok_2(self):
        data = self.build("light", "#123456", "#123456", "#123456", "#123456")
        self.validator(data)

    def test_not_ok_mode(self):
        data = self.build("darkos", "#123456", "#123456", "#123456", "#123456")
        with pytest.raises(ValidationError):
            self.validator(data)

    def test_not_ok_color(self):
        data = self.build("dark", "#1zzzzz", "#123456", "#123456", "#123456")
        with pytest.raises(ValidationError):
            self.validator(data)

    def test_not_ok_extra_1(self):
        data = self.build("dark", "#123456", "#123456", "#123456", "#123456")
        data["lol"] = "lolilol"
        with pytest.raises(ValidationError):
            self.validator(data)

    def test_not_ok_extra_in_palette(self):
        data = self.build("dark", "#123456", "#123456", "#123456", "#123456")
        data["light"]["lol"] = "lolilol"
        with pytest.raises(ValidationError):
            self.validator(data)


class TestRecommendationListContentValidator(object):
    validator = RecommendationListJsonContentValidator()

    @staticmethod
    def expect_error(data):
        with pytest.raises(ValidationError):
            TestRecommendationListContentValidator.validator(data)

    @staticmethod
    def get_valid_text_block():
        return dict(type="text-block", content="coucou")

    @staticmethod
    def get_valid_univ_block(university=1, appreciation=0):
        return dict(
            type="univ-block",
            content=dict(university=university, appreciation=appreciation),
        )

    def test_ok(self):
        data = []
        self.validator(data)

    def test_not_ok(self):
        data = {}
        self.expect_error(data)

    def test_ok_text_block(self):
        data = [self.get_valid_text_block()]
        self.validator(data)

    def test_ok_univ_block(self):
        data = [self.get_valid_univ_block()]
        self.validator(data)

    def test_ok_both_block(self):
        data = [self.get_valid_univ_block(), self.get_valid_text_block()]
        self.validator(data)

    def test_ok_null_appreciation_univ(self):
        data = [self.get_valid_univ_block(appreciation=None)]
        self.validator(data)

    def test_ok_many(self):
        data = [self.get_valid_text_block()] * 99
        self.validator(data)

    def test_not_ok_too_many(self):
        data = [self.get_valid_text_block()] * 105
        self.expect_error(data)


class TestPhotosValidator(object):
    validator = PhotosValidator()

    def test_empty_array_ok(self):
        data = []
        self.validator(data)

    def test_empty_object_not_ok(self):
        data = {}
        with pytest.raises(ValidationError):
            self.validator(data)

    def test_one_ok(self):
        data = [{"title": "Coucou", "url": "https://utc.fr/logo.svg"}]
        self.validator(data)

    def test_not_ok(self):
        data = [{"title": "Coucou", "url": "https://utc.fr/logosvg"}]
        with pytest.raises(ValidationError):
            self.validator(data)

    def test_fails_with_more_data(self):
        data = [{"title": "Coucou", "url": "https://utc.fr/logo.svg", "héhé": "houhou"}]
        with pytest.raises(ValidationError):
            self.validator(data)


class TestTaggedItemValidator(object):
    validator = TaggedItemValidator()

    def test_photos_tag_ok(self):
        data = [{"title": "Coucou", "url": "https://utc.fr/logo.svg"}]
        self.validator("photos", data)

    def test_photos_tag_not_ok(self):
        data = [{"title": "Coucou", "url": "//utc.fr/logo.svg"}]
        with pytest.raises(ValidationError):
            self.validator("photos", data)

    def test_unknown_tag(self):
        data = []
        with pytest.raises(ValidationError):
            self.validator("photossqsdknrncLQSKDJNCZIURYNCCH", data)


class TestPathExtensionValidator(object):
    validator = PathExtensionValidator(["svg"])

    def test_svg_ok(self):
        self.validator("test.svg")

    def test_no_extension(self):
        with pytest.raises(ValidationError):
            self.validator("test")

    def test_multiple(self):
        with pytest.raises(ValidationError):
            self.validator("test.svg.zero")


class TestTagNameValidator(object):
    validator = TagNameValidator()

    def test_on_valid_tagname(self):
        # Might need to change this at one point
        # it transport is no more available
        self.validator("transport")

    def test_on_invalid_tagname(self):
        # ie a tag name that doesn't have a matching schema
        with pytest.raises(ValidationError):
            self.validator("qmlskdjmlqfinzoieuncmlkqsdnkuy")


class TestImageValidator(object):
    validator = ImageValidator()

    # With file path input

    def test_on_valid_png_fp(self):
        self.validator(
            join(BACKEND_ROOT_DIR, "base_app/static/base_app/favicon/favicon-16x16.png")
        )

    def test_on_valid_svg_fp(self):
        self.validator(
            join(BACKEND_ROOT_DIR, "base_app/static/base_app/favicon/favicon.svg")
        )

    def test_on_invalid_file_fp(self):
        with pytest.raises(ValidationError):
            self.validator(__file__)

    # With file input directly

    def test_on_valid_png(self):
        with open(
            join(
                BACKEND_ROOT_DIR, "base_app/static/base_app/favicon/favicon-16x16.png"
            ),
            "rb",
        ) as f:
            self.validator(f)

    def test_on_valid_svg(self):
        with open(
            join(BACKEND_ROOT_DIR, "base_app/static/base_app/favicon/favicon.svg"), "rb"
        ) as f:
            self.validator(f)

    def test_on_invalid_file(self):
        with open(__file__, "rb") as f:
            with pytest.raises(ValidationError):
                self.validator(f)
