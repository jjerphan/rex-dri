from backend_app.models.for_testing.versioning import ForTestingVersioning
from reversion.models import Version
from backend_app.signals.squash_revisions import squash_revision_by_user
import reversion
from backend_app.tests.utils import WithUserTestCase
import json


class SquashVersionsTestCase(WithUserTestCase):
    def setUp(self):
        self.obj = ForTestingVersioning(bbb="v1")
        with reversion.create_revision():
            self.obj.save()
            reversion.set_user(self.authenticated_user)

    def get_versions(self, obj):
        return (
            Version.objects.get_for_object(obj)
            .select_related("revision")
            .order_by("-revision__date_created")
        )

    def get_version_data(self, version):
        return json.loads(version.serialized_data)[0]["fields"]

    def test_no_squashing(self):
        """
        Test to check that when two different users successively
        Modify a model, no squashing is performed
        nb_versions is incremented
        """
        with reversion.create_revision():
            self.obj.bbb = "v2 other user"
            self.obj.save()
            reversion.set_user(self.authenticated_user_2)

        squash_revision_by_user(None, self.obj)

        versions = self.get_versions(self.obj)
        self.assertEqual(len(versions), 2)
        self.assertEqual(len(versions), self.obj.nb_versions)
        first_edit = self.get_version_data(versions[1])
        second_edit = self.get_version_data(versions[0])

        self.assertEqual(first_edit["bbb"], "v1")
        self.assertEqual(second_edit["bbb"], "v2 other user")

    def test_squashing(self):
        """
        Test to check that when a user save two different
        versions of a same model, squashing is performed
        nb_versions is not incremented
        """
        with reversion.create_revision():
            self.obj.bbb = "v2"
            self.obj.save()
            reversion.set_user(self.authenticated_user)

        squash_revision_by_user(None, self.obj)

        versions = self.get_versions(self.obj)
        self.assertEqual(len(versions), 1)
        self.assertEqual(len(versions), self.obj.nb_versions)
        version_data = self.get_version_data(versions[0])
        self.assertEqual(version_data["bbb"], "v2")
