from backend_app.tests.utils import WithUserTestCase


class ReadAccessTestCase(WithUserTestCase):
    @classmethod
    def setUpMoreTestData(cls):
        cls.api_moderation = "/api/test/moderation/"

    def test_read(self):
        """
        Basic tests to check read access rights
        On BaseModelViewset
        """
        response = self.staff_client.get(self.api_moderation)
        self.assertEqual(response.status_code, 200)

        response = self.moderator_client.get(self.api_moderation)
        self.assertEqual(response.status_code, 200)

        response = self.authenticated_client.get(self.api_moderation)
        self.assertEqual(response.status_code, 200)

        response = self.unauthenticated_client.get(self.api_moderation)
        self.assertEqual(response.status_code, 403)
