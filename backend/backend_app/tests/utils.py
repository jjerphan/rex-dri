from django.contrib.auth.models import Group
from django.test import TestCase
from rest_framework.test import APIClient

from base_app.models import User


class WithUserTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        password = "123456"

        cls.staff_user = User.objects.create_user(
            username="staff_member", email="master@master.fr", password=password
        )
        cls.staff_user.is_staff = True
        cls.staff_user.save()

        cls.moderator_user = User.objects.create_user(
            username="moderator_member",
            email="moderator@moderator.fr",
            password=password,
        )
        cls.moderator_group = Group.objects.get_or_create(name="Moderators")[0]
        cls.moderator_group.user_set.add(cls.moderator_user)
        cls.moderator_group.save()

        cls.dri_user = User.objects.create_user(
            username="dri_member", email="dri@dri.fr", password=password
        )
        cls.dri_group = Group.objects.get_or_create(name="DRI")[0]
        cls.dri_group.user_set.add(cls.dri_user)
        cls.dri_group.save()

        cls.authenticated_user = User.objects.create_user(
            username="authenticated_user",
            email="authenticated@authenticated.fr",
            password=password,
        )

        cls.authenticated_user_2 = User.objects.create_user(
            username="authenticated_user_2",
            email="authenticated_2@authenticated.fr",
            password=password,
        )

        cls.staff_client = APIClient()
        cls.staff_client.login(username=cls.staff_user.username, password=password)

        cls.moderator_client = APIClient()
        cls.moderator_client.login(
            username=cls.moderator_user.username, password=password
        )

        cls.dri_client = APIClient()
        cls.dri_client.login(username=cls.dri_user.username, password=password)

        cls.authenticated_client = APIClient()
        cls.authenticated_client.login(
            username=cls.authenticated_user.username, password=password
        )

        cls.authenticated_client_2 = APIClient()
        cls.authenticated_client_2.login(
            username=cls.authenticated_user_2.username, password=password
        )

        for user in [
            cls.staff_user,
            cls.moderator_user,
            cls.dri_user,
            cls.authenticated_user,
            cls.authenticated_user_2,
        ]:
            user.has_validated_cgu_rgpd = True
            user.save()

        cls.unauthenticated_client = APIClient()

        cls.setUpMoreTestData()

    @classmethod
    def setUpMoreTestData(cls):
        pass
