from backend_app.models.abstract.base import BaseModelSerializer
from backend_app.models.abstract.essentialModule import EssentialModuleSerializer
from backend_app.models.course import Course
from backend_app.models.courseFeedback import CourseFeedback
from backend_app.models.exchange import Exchange


class CourseFeedbackSerializer(EssentialModuleSerializer):
    class Meta:
        model = CourseFeedback
        fields = EssentialModuleSerializer.Meta.fields + (
            "language",
            "comment",
            "adequation",
            "working_dose",
            "language_following_ease",
        )


class CourseSerializer(BaseModelSerializer):
    course_feedback = CourseFeedbackSerializer(many=False, read_only=True)

    class Meta:
        model = Course
        fields = BaseModelSerializer.Meta.fields + (
            "course_feedback",
            "code",
            "title",
            "link",
            "nb_credit",
            "category",
            "profile",
            "tsh_profile",
        )
        read_only_fields = ("course_feedback",)


class ExchangeSerializer(BaseModelSerializer):
    exchange_courses = CourseSerializer(many=True, read_only=True)

    class Meta:
        model = Exchange
        fields = BaseModelSerializer.Meta.fields + (
            "year",
            "semester",
            "duration",
            "dual_degree",
            "master_obtained",
            "student_major",
            "student_minor",
            "student_option",
            "exchange_courses",
        )
