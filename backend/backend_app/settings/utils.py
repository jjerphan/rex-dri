from os.path import dirname, join, realpath

import yaml

CURRENT_DIR = dirname(realpath(__file__))


def get_yml_file(name):
    """
    Helper function to load config files.
    """
    with open(join(CURRENT_DIR, name), "r") as f:
        return yaml.load(f)
