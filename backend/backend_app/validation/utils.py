from jsonschema import RefResolver

from backend_app.validation.schemas import DEFINITIONS_SCHEMA, TAGS_SCHEMA_COLLECTION

DEFINITIONS_RESOLVER = RefResolver.from_schema(DEFINITIONS_SCHEMA)


def get_schema_for_tag(tag_name):
    """
    Get the schema associated with a tag (identified by it's name).

    :param tag_name: Name of the tag
    :type tag_name: str
    :return:
    """
    return TAGS_SCHEMA_COLLECTION[tag_name]
