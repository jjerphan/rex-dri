import json
import os

from backend_app.models.tag import Tag
from base_app.models import User
from .loadGeneric import LoadGeneric


class LoadTags(LoadGeneric):
    """
    Class to load the tags in the app.
    """

    def __init__(self, admin: User):
        self.admin = admin

    def load(self):
        tmp = os.path.join(os.path.realpath(__file__), "../../assets/tags.json")
        tags_path = os.path.abspath(tmp)
        with open(tags_path) as f:
            tags = json.load(f)
            for tag_name in tags:
                t = Tag(name=tag_name)
                t.save()
                self.add_info_and_save(t, self.admin)
