import csv
from os.path import join, abspath

from backend_app.load_data.utils import ASSETS_PATH
from backend_app.models.language import Language

from .loadGeneric import LoadGeneric


class LoadLanguages(LoadGeneric):
    """
        Load languages in the app
    """

    def load(self):
        languages_file_loc = abspath(join(ASSETS_PATH, "languages.csv"))

        with open(languages_file_loc) as csvfile:
            reader = csv.reader(csvfile, quotechar='"')
            next(reader)
            for r in reader:
                Language.objects.create(code_iso=r[0], name=r[1])
