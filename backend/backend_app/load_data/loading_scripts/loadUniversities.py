from os.path import abspath, join

from backend_app.load_data.utils import ASSETS_PATH, csv_2_dict_list
from backend_app.models.campus import Campus
from backend_app.models.city import City
from backend_app.models.country import Country
from backend_app.models.university import University
from base_app.models import User
from .loadGeneric import LoadGeneric


class LoadUniversities(LoadGeneric):
    """
    Load the universities in the app
    """

    def __init__(self, admin: User):
        self.admin = admin

    @staticmethod
    def get_destination_data():
        destinations_path = abspath(join(ASSETS_PATH, "destinations_extracted.csv"))
        return csv_2_dict_list(destinations_path)

    def load(self):
        for row in self.get_destination_data():
            lat = round(float(row["lat"]), 6)
            lon = round(float(row["lon"]), 6)

            country = Country.objects.get(pk=row["country"])
            city = City(name=row["city"], country=country)
            city.save()
            self.add_info_and_save(city, self.admin)

            univ = University.objects.update_or_create(
                utc_id=row["utc_id"],
                defaults={
                    "name": row["university"],
                    "acronym": row["acronym"],
                    "website": row["website"],
                    "logo": row["logo"],
                },
            )[0]
            self.add_info_and_save(univ, self.admin)

            main_campus = Campus(
                is_main_campus=True,
                name="Campus - " + univ.name,
                city=city,
                university=univ,
                lat=lat,
                lon=lon,
            )
            main_campus.save()
            self.add_info_and_save(main_campus, self.admin)
