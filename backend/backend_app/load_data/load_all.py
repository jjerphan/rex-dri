import reversion

from backend_app.load_data.loading_scripts.loadAdminUser import LoadAdminUser
from backend_app.load_data.loading_scripts.loadCountries import LoadCountries
from backend_app.load_data.loading_scripts.loadCurrencies import LoadCurrencies
from backend_app.load_data.loading_scripts.loadGroups import LoadGroups
from backend_app.load_data.loading_scripts.loadLanguages import LoadLanguages
from backend_app.load_data.loading_scripts.loadTags import LoadTags
from backend_app.load_data.loading_scripts.loadUniversities import LoadUniversities
from backend_app.load_data.loading_scripts.loadUniversityEx import LoadUniversityEx
from backend_app.load_data.loading_scripts.loadRecommendationLists import (
    LoadRecommendationLists,
)


def load_all():
    """
    Function to load all the initial data in the app
    """

    with reversion.create_revision():
        LoadGroups()
        admin = LoadAdminUser().get()
        LoadCurrencies(admin).load()
        LoadCountries(admin).load()
        LoadUniversities(admin).load()
        LoadTags(admin).load()
        LoadLanguages().load()
        LoadUniversityEx(admin).load()
        LoadRecommendationLists(admin).load()
