import logging
import os

import requests
from django.core.management.base import BaseCommand

from backend_app.models.currency import Currency
from external_data.models import ExternalDataUpdateInfo

FIXER_API_TOKEN = os.environ["FIXER_API_TOKEN"].strip()
logger = logging.getLogger("django")


class Command(BaseCommand):
    help = "Command to handle updating remote data"

    def add_arguments(self, parser):
        subparsers = parser.add_subparsers(
            title="subcommands", dest="subcommand", required=False
        )
        subparsers.add_parser("all", help="(default) Update all external data")
        subparsers.add_parser("currencies", help="Update currencies from fixer")

    def handle(self, *args, **options):
        if "subcommand" in options.keys():
            subcommand = options["subcommand"]
            if subcommand == "all" or subcommand is None:
                self.update_all()
            elif options["subcommand"] == "currencies":
                self.update_currencies()
        else:
            self.update_all()

    @staticmethod
    def update_all():
        logger.info("Updating all external data")
        Command.update_currencies()

    @staticmethod
    def update_currencies():
        logger.info("Updating currencies")
        response = requests.get(
            "http://data.fixer.io/api/latest?access_key={}".format(FIXER_API_TOKEN)
        )

        data = response.json()

        if data["success"]:
            for (code, rate) in response.json()["rates"].items():
                Currency.objects.update_or_create(
                    code=code, defaults={"one_EUR_in_this_currency": rate}
                )
            ExternalDataUpdateInfo.objects.create(source="fixer")
            logger.info("Currency update was successful")

        else:
            logger.error(
                "Updating currency information from fixer failed, see response from the API below."
            )
            logger.error(response.json())
