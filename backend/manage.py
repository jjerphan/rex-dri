#!/usr/bin/env python3
import os
import sys
from os.path import join

from dotenv import load_dotenv
from base_app.settings.dir_locations import ENVS_FILE_DIR

if __name__ == "__main__":
    # We make sure to use override = False to not override variables in the env.
    load_dotenv(join(ENVS_FILE_DIR, "db.env"), override=False)
    load_dotenv(join(ENVS_FILE_DIR, "django.env"), override=False)

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "base_app.settings.main")
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)
