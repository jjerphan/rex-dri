from django.test import TestCase
from rest_framework.test import APIClient

from base_app.models import User


class HasAccessTestCase(TestCase):
    """
    Test to check that validation of the CGU and RGPD is Required
    """

    @classmethod
    def setUpTestData(cls):
        password = "123456"

        cls.has_validated_user = User.objects.create_user(
            username="yolo", email="osef@master.fr", password=password
        )
        cls.has_validated_user.has_validated_cgu_rgpd = True
        cls.has_validated_user.save()

        cls.has_not_validated_user = User.objects.create_user(
            username="yolo2", email="osef@master.fr", password=password
        )
        cls.has_not_validated_user.has_validated_cgu_rgpd = False
        cls.has_not_validated_user.save()

        cls.has_validated_client = APIClient()
        cls.has_validated_client.login(
            username=cls.has_validated_user.username, password=password
        )

        cls.has_not_validated_client = APIClient()
        cls.has_not_validated_client.login(
            username=cls.has_not_validated_user.username, password=password
        )

    def test_accepted_has_access(self):
        response = self.has_validated_client.get("/api/")
        self.assertEqual(response.status_code, 200)

    def test_not_accepted_refused(self):
        response = self.has_not_validated_client.get("/api/")
        self.assertEqual(response.status_code, 302)  # We have a redirect
        self.assertEqual(response.url, "/cgu-rgpd/?next=/api/")
