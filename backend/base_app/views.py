import logging

from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.shortcuts import render
from webpack_loader.utils import get_files

from backend_app.utils import clean_route
from backend_app.viewsets import ALL_VIEWSETS
from base_app.forms import UserForm
from base_app.models import User

logger = logging.getLogger("django")


def get_bundle_loc(name):
    return "/".join(get_files(name)[0]["url"].split("/")[:-1]) + "/"


def index(request):
    """
    View to to display the index app that contains the JS / CSS
    The "template" displayed is in ./templates/index.html
    """

    # We give the user object so that we can access its id in JS
    # and fetch userData
    user = request.user

    # small hack to get the correct location of the frontend bundled files
    front_bundle_loc = get_bundle_loc("main")

    # We also retrieve the list of all routes endpoints
    endpoints = list(map(lambda v: clean_route(v.end_point_route), ALL_VIEWSETS))
    return render(
        request,
        "index.html",
        dict(user=user, endpoints=endpoints, front_bundle_loc=front_bundle_loc),
    )


def rgpd_raw(request):
    """
    Render the view that displays only the RGPD conditions
    """
    return render(request, "rgpd_raw.html")


def cgu_rgpd(request):
    """
    Render the view that handles user accepting CGU and RGPD conditions
    """
    if request.method == "POST":
        user = User.objects.get(pk=request.user.pk)
        form = UserForm(request.POST, instance=user)
        if form.is_valid():
            user = form.save(commit=True)
            user.save()
    else:
        user = User.objects.get(pk=request.user.pk)
        form = UserForm(instance=user)

    if "next" in request.GET and user.has_validated_cgu_rgpd:
        # if the user has just validated everything, we redirect him to the location he requested
        return HttpResponseRedirect(request.GET["next"])
    else:
        return render(request, "cgu_rgpd.html", dict(user=user, form=form))


def banned(request):
    user = request.user
    if user.is_banned:
        return render(request, "banned.html", dict(user=user))
    else:
        return HttpResponseNotFound()


def media_files_view(request, path):
    """
    Media files are served by nginx only if the user is connected.
    The authentication checked is performed through the middleware
    so here we only need to return a dumb request with the right headers
    that will be read by nginx.
    """
    response = HttpResponse()
    del response["Content-Type"]
    response["X-Accel-Redirect"] = "/protected-assets/media/" + path
    return response
