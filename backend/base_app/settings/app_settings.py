# Is moderation activated for models marked with 1 as a moderation level
MODERATION_ACTIVATED = True
# What is the default moderation level, see doc
# https://rex-dri.gitlab.utc.fr/rex-dri/documentation/#/Application/Backend/moderation_and_versioning?id=moderation
DEFAULT_MODEL_MODERATION_LEVEL = 2

# File extensions recognized as being pictures
ALLOWED_PHOTOS_EXTENSION = ["jpg", "jpeg", "png", "svg"]

# Configuration of the CAS server
CAS_SERVER_URL = "https://cas.utc.fr/cas/"
# We want to map the attribute returned by the cas to the user model attribute
CAS_APPLY_ATTRIBUTES_TO_USER = True
# Mapping of the attribute
CAS_RENAME_ATTRIBUTES = {"mail": "email", "givenName": "first_name", "sn": "last_name"}
