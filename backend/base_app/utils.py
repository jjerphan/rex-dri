def show_toolbar(request) -> bool:
    """
    Function to tell when to show the django debug toolbar.
    It was having an issue inside docker.

    The django debug toolbar is displayed only when in /api/...
    """
    if request.is_ajax():
        return False

    url = request.get_full_path()
    return "/api/" in url or "__debug__" in url
    return True
