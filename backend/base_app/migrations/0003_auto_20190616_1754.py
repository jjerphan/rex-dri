# Generated by Django 2.1.7 on 2019-06-16 15:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("base_app", "0002_auto_20190531_2007")]

    operations = [
        migrations.RenameField(
            model_name="user",
            old_name="has_validated_cgu",
            new_name="has_validated_cgu_rgpd",
        ),
        migrations.RemoveField(model_name="user", name="has_validated_rgpd"),
        migrations.AddField(
            model_name="user",
            name="delete_next_time",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="user",
            name="is_deleted",
            field=models.BooleanField(default=False),
        ),
    ]
