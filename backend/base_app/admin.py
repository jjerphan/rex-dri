from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm
from rest_framework.authtoken.admin import TokenAdmin

from .models import User


# Handling of the registration of the custom User model to make sure
# we can see all the fields.
# taken from: https://stackoverflow.com/a/15013810


class CustomUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User


class CustomUserAdmin(UserAdmin):
    form = CustomUserChangeForm

    fieldsets = UserAdmin.fieldsets + (
        (
            None,
            {"fields": ("allow_sharing_personal_info", "secondary_email", "pseudo")},
        ),
    )


admin.site.register(User, CustomUserAdmin)

# Pour la génération de token dans l'administration du site.
TokenAdmin.raw_id_fields = ("user",)
