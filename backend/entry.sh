#!/bin/bash

sh `dirname $0`/init_logs.sh

# Script to make sure that assets from the frontend have been generated at least
# once so that the backend container doesn't crash on the first launch.

while [ ! -f `dirname $0`/../frontend/webpack-stats.json ]; do
    echo "Waiting for frontend static files to be initialized..."
    sleep 2
done

while ! grep -q '"status":"done"' `dirname $0`/../frontend/webpack-stats.json; do
    echo "Waiting for frontend static files to be initialized..."
    sleep 2
done

echo "Frontend staticfiles are ready (NB: not necessarly up-to-date)."

./manage.py collectstatic --noinput
