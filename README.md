# Rex-DRI

*A revolution is on its way... :airplane: (it's still a work in progress).*

## Documentation

**The project documentation is available [here](https://rex-dri.gitlab.utc.fr/rex-dri/documentation/).**


## Notes

**Collaborators and PR are highly welcomed!**

Feel free to ask questions about the project using the issue system.

