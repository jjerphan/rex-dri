How to deploy
==============

## Step-by-step

Deploying the app is fairly simple: all you need is `docker` and `docker-compose` installed.

If you don't know how to install them, you should look in [this section](GettingStarted/set-up.md) of the documentation.

- Once this is done clone the repository of the project.
- Then, run `make setup` (this will generate the required `.env` files) and set their correct values in both `server/envs` directory and `.env` (at the root of the repo -- for the map to work appropriately). Take care especially with the files `django.env` and `external_data.env`.
- Once this is done, you can simply run `make prod` and then `make init_dev_data` (TODO create one for prod) to get going fast.

`make prod` will start all the services described in `docker-compose.prod.yml` (in the `server` directory).


## Production architecture

Bellow is a quick nasty schema of the deploy architecture:

![deploy architecture](./deploy_architecture.svg)

The important stuff to remember:

- All clients/browsers request comes in on port `80` where `nginx` is listening.
- Depending on the requested URI, `nginx` will:
  - Transfer the request to the backend (through a unix socket stored in docker volume),
  - Serve static files directly.
  - (media files are served by `nginx` after authentification)
- The backend runs on top of `uWSGI`.
- The database and the backend are on a private network.
- All logs are handled through docker volumes and a dedicated service that rotate the logs each day and keep 30 rotations (/days) of logs.

All request coming in `nginx` are logged. backend server errors are logged. If a frontend crash occurres a logging request is sent to the backend that will log it in a dedicated file.

