Moderation & Versioning in the app
=================================

## Moderation

### Application level

!> TODO

### Model level

TODO clean

Model `moderation_level` can take the following values:

* `0`: moderation will never be applied,
* `1`: moderation will be on if the global settings for moderation is turned on,
* `2`: (default for security reasons) moderation will always be on no matter what.

*NB: staff members, dri members and moderators won't be subject to (model-level) moderation.*

*NB: moderation can be moreover enforced at the object level. But that's for [another documentation part](Application/Backend/moderation_and_versioning.md).*


### Object/instance level

!> TODO


## Versioning

### General idea

!> TODO
