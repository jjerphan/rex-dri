Tags
=====

To simplify the general `backend` architecture and allow for some generality, a *tagging* system has been put in place.

A *Tag* is kind of Django model that contains the definition of its field (and the associated constraints) in its `config` attribute.
All information provided by the user is then stored in `JSON` format.

At this point, all `City`, `University`, `Campus` and `Country` models can be associated with any tag through `XXXXXTaggedItem` class.

A `tag` is configured with a `JSON` that looks like this:

```json
{
    "name of the field":{
        "type": "????",
        "required": bool,
        "validators" : {
            "name_validator_1" : ...
        }
}
```

At this point, `type` and `validators` may be:
- 'text' :
  - `max_length` to constrain the length of the text.
- 'url' :
  - `extension` : The constrain the extension (right `.` part) of the url.
- `list` : TODO
