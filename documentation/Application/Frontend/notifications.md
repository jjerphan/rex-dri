Notifications in the app
======

The visual notifications in the app are based on the [`Snackbars`](https://material-ui.com/demos/snackbars/) component of our frontend framework `material-ui`.

To ease the use of notifications throughout the app, we use the [`notistack`](https://github.com/iamhosseindhv/notistack) module.


## Don't miss notistack doc

The very good documentation is available [here](https://iamhosseindhv.com/notistack).


## How to use it

### 1. The standard way\*

!> \* Only when it's used in a *class-based* component that won't be extended.

You simply need to wrap your component in the `withSnackbar` function provided by `notistack` when exporting it.

```js
import React, { Component } from "react";
import { withSnackbar } from "notistack";

/**
 * Never forget to comment your code by the way
 *
 * @class ADumbComponent
 * @extends {Component}
 */
class ADumbComponent extends Component {
  // ....
}

export default withSnackbar(ADumbComponent);
```

By doing so, two `props` functions will be added to your component: `enqueueSnackbar` (to dispatch a notification) and `closeSnackbar` (to close a specific notification, see [doc](https://iamhosseindhv.com/notistack)).

Then in your component you can simply call `enqueueSnackbar`:

```js
this.props.enqueueSnackbar(
  "My awesome message",
  {
    variant: "success", // or "error", "warning" or "info"
    autoHideDuration: 5000 // duration of the notification
    // And other parameters you can find in the doc.
  }
)
```

### 2. The \* workaround

When the condition for 1 is not met, you can use the `Notifier` component available in `frontend/src/components/common/Notifier.js`. This component will display a notification as soon as it is added to the DOM. You should pass the parameters in its `props`, as follow:

```jsx
<Notifier
  message={"My awesome error message."}
  options={{ variant: "error" }}
/>
```
