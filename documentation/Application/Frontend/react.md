Quick intro to React
===============

?> :information_desk_person: `React` is a JavaScript library to build user interface and it was introduced by Facebook.


## General idea

When doing web development, you should be familiar with the *DOM* ([Document Object Model](https://fr.wikipedia.org/wiki/Document_Object_Model)) which is basically a tree translation of the HTML file you send to the browser for rendering.

React is built on a *virtual DOM* (you can explore it with the [React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi) extension for chromium based browsers). The elements of this *virtual DOM* are no longer HTML tags; they are real *living* objects called **components** that can be modified et that can perform multiple actions. Every time an update to is performed, the DOM in your browser is smartly updated by react based on its own internal *virtual DOM*.

Components being elements of tree like structure, they must have a parent (or be the root node) and they can have as many *children* components as they want.


## TODO more


