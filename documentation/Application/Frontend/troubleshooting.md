Troubleshooting
===============

## General theme / app bar color is messed up

?> :information_desk_person: Check that none of your imports from `material-ui` ends with `/index` (webstorm adds them sometimes and it seems to break some stuff along the way).

