Loading initial/example data into the app
=====

To load some basic data in the app, you can use the python classes: `loadCountries`, `loadUniversities`, etc. available in `backend/backend_app/load_data/loading_scripts`.

You can load everything in one command:

```bash
make init_dev_data
```

or by connecting to the `Django` shell:

```bash
make django_shell
```

And by running:

```python
from backend_app.load_data.load_all import load_all
load_all()
```

*NB: Those scripts are tested so they should work.*


By doing so, an admin user is created according to the environment variables `DJANGO_ADMIN_USERNAME` and `DJANGO_ADMIN_PASSWORD` that are set (in our case) in the `docker-compose` file.
