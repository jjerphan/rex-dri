Contributors
=========

?> Thank you to all of those who contributed to this project :thumbsup:

- Solène Aboud
- Ségolène Brisemeur
- Florent Chehab
- Alexandre Lanceart

*Find all the people who contributed [here](https://gitlab.utc.fr/rex-dri/rex-dri/graphs/master).*

