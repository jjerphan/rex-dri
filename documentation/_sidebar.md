<!-- docs/_sidebar.md -->

- Getting started

  * [Introduction](GettingStarted/introduction.md)
  * [Set-up](GettingStarted/set-up.md)
  * [Initializing data](GettingStarted/init_data.md)
  * [IDE Setup](GettingStarted/IDE_setup.md)


- Application documentation

  - Backend

    * [Architecture](Application/Backend/architecture.md)
    * [Models, Serializers and ViewSets](Application/Backend/models_serializers_viewsets.md)
    * [Config files](Application/Backend/config_files.md)
    * [Moderation & versioning](Application/Backend/moderation_and_versioning.md)
    * [Data validation](Application/Backend/data_validation.md)
    * [Tags](Application/Backend/tags.md)
    * [API](Application/Backend/API.md)
    * [External data](Application/Backend/external_data.md)
    * [Tests](Application/Backend/tests.md)
    * [Optimization](Application/Backend/optimization.md)

  - Frontend

    * [React basics](Application/Frontend/react.md)
    * [Use of Redux](Application/Frontend/redux.md)
    * [Tests](Application/Frontend/tests.md)
    * [Notifications](Application/Frontend/notifications.md)
    * [Map](Application/Frontend/map.md)
    * [Troubleshooting](Application/Frontend/troubleshooting.md)

  - [**Deploy**](Application/deploy.md)


- Comments about technologies used

  * [Use of `Docker`](Technologies/docker.md)
  * [Debugger](Technologies/debugging.md)


- Other

  * [About this documentation](Other/this_doc.md)
  * [Contributions](Other/contributions.md)
  * [Credits](Other/credits.md)
