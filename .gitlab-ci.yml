# In CI we use the latest docker images.

stages:
  - check
  - test
  - lint
  - svg-gen-docu # required to be done before documentation and in separate stages
  - documentation


.only-default: &only-default
  only:
    - master
    - merge_requests


check_back:
  <<: *only-default
  stage: check
  image: registry.gitlab.utc.fr/rex-dri/rex-dri/backend:v0.2.1
  before_script:
    - sh ./backend/init_logs.sh
    - make setup
  script:
    - cd backend && ./manage.py check
    - cd ../documentation && make extract_django  # Try to generate .dot files for the system architecture
  artifacts:
    paths:
      - documentation/generated/
    expire_in: 1 hour
  tags:
    - docker

check_front:
  <<: *only-default
  stage: check
  image: registry.gitlab.utc.fr/rex-dri/rex-dri/frontend:v0.5.0
  before_script:
    - cd frontend && cp -R /usr/src/deps/node_modules .
  script:
    - npm run build
  artifacts:
    paths:
      - frontend/webpack-stats.json
    expire_in: 1 hour
  tags:
    - docker

test_back:
  <<: *only-default
  stage: test
  image: registry.gitlab.utc.fr/rex-dri/rex-dri/backend:v0.2.1
  variables:
    POSTGRES_DB: postgres
    POSTGRES_USER: postgres
    POSTGRES_PASSWORD: postgres
    POSTGRES_HOST: postgres
    POSTGRES_PORT: 5432  # We absolutely need this one since a gitlab runner will inject a similar variable; will cause tests to fail.
  services:
    - postgres:10.5
  before_script:
    - sh ./backend/init_logs.sh
    - make setup
  script:
    - cd backend
    - pytest base_app/ backend_app/ --cov --cov-config .coveragerc --cov-report term --cov-report html
  artifacts:
    paths:
      - backend/htmlcov/
    expire_in: 1 hour
  tags:
    - docker

test_frontend:
  <<: *only-default
  stage: test
  image: registry.gitlab.utc.fr/rex-dri/rex-dri/frontend:v0.5.0
  before_script:
    - cd frontend && cp -R /usr/src/deps/node_modules .
  script:
    - npm run test
  tags:
    - docker

flake8:
  <<: *only-default
  stage: lint
  image: registry.gitlab.utc.fr/rex-dri/rex-dri/backend:v0.2.1
  script:
    - cd backend && flake8
  tags:
    - docker

eslint:
  <<: *only-default
  stage: lint
  image: registry.gitlab.utc.fr/rex-dri/rex-dri/frontend:v0.5.0
  before_script:
    - cd frontend && cp -R /usr/src/deps/node_modules .
  script:
    - npm run lint
  tags:
    - docker

generate_UML_svg:
  <<: *only-default
  stage: svg-gen-docu
  image: floawfloaw/plantuml
  script: cd documentation && make convert_to_svg
  dependencies:
    - check_back
  artifacts:
    paths:
      - documentation/generated/
    expire_in: 1 hour
  tags:
    - docker

pages:
  stage: documentation
  image: floawfloaw/plantuml
  dependencies:
    - test_back
    - generate_UML_svg
  script:
    - mkdir .public
    - mv backend/htmlcov/ .public/coverage
    - mv documentation/ .public/documentation
    - mv .public public
  artifacts:
    paths:
      - public
    expire_in: 1 month
  only:
    - master
  tags:
    - docker
