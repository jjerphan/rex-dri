module.exports = {
  "env": {
    "browser": true,
    "es6": true,
    "jest/globals": true
  },
  "extends": [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:import/errors",
    "plugin:import/warnings"
  ],
  "parser": "babel-eslint",
  "parserOptions": {
    "ecmaVersion": 2018,
    "sourceType": "module",
    "ecmaFeatures": {
      "jsx": true
    }
  },
  "plugins": [
    "react",
    "jest",
    "import",
  ],
  "rules": {
    "indent": [
      "error",
      2,
      { "SwitchCase": 1, "ignoredNodes": [ "JSXAttribute", "JSXSpreadAttribute", ] }
    ],
    "linebreak-style": [
      "error",
      "unix"
    ],
    "quotes": [
      "error",
      "double"
    ],
    "semi": [
      "error",
      "always"
    ],
    "react/no-unescaped-entities": "off", // that one doesn't improve code readability
    "react/jsx-indent-props": [2, "first"],
    "react/jsx-indent": [2, 2],
    "react/prop-types": "error",
    "react/no-deprecated": "error",
    "no-warning-comments": [
      "error",
      {
        "terms": ["todo", "fixme", "any other term"],
        "location": "anywhere"
      }
    ],
    "no-var": "error",
  }
};
