/**
 * Converts a date object into a date string "yyyy-mm-dd"
 *
 * @export
 * @param {Date} date
 * @returns
 */
export default function dateToDateStr(date) {
  if (!date) {
    return null;
  }

  let yyyy = date.getFullYear(),
    mm = date.getMonth() + 1,
    dd = date.getDate();

  // add zero to the left if necessary
  mm = mm < 10 ? "0" + mm : mm;
  dd = dd < 10 ? "0" + dd : dd;

  return `${yyyy}-${mm}-${dd}`;
}
