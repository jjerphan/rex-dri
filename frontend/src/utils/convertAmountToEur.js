import { getLatestReadDataFromStore } from "../redux/api/utils";

// For optimization
let allCurrencies = undefined;

/**
 * Function for converting money amounts to euros.
 *
 * @export
 * @param {number} amount
 * @param {string} currencyStr
 * @returns {number} or null if no matching currency could be found
 */
export default function convertAmountToEur(amount, currencyStr) {
  if (currencyStr === "EUR") {
    return amount;
  }

  // a bit of optimization, will be useful if this function is used a lot
  if (typeof allCurrencies === "undefined") {
    allCurrencies = new Map();
    const currencies = getLatestReadDataFromStore("currenciesAll");
    if (currencies.length === 0) {
      // To use this function, you need to have currencies loaded in the store. Make sure the currencies are loaded in the backend.
      // Also, make sure not use this function before we have the currencies loaded :)
      throw new Error("Currencies in the store are empty see code for more info.");
    }
    currencies.forEach(c => allCurrencies.set(c.id, c.one_EUR_in_this_currency));
  }

  if (allCurrencies.has(currencyStr)) {
    return Math.trunc(amount / allCurrencies.get(currencyStr));
  } else {
    return null;
  }
}
