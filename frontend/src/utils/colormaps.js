// Python script used to generate the values
/*
import matplotlib as cm
import matplotlib.pyplot as plt

N = 20

ALL = [
 'viridis', 'plasma', 'inferno', 'magma', 'cividis', 'Greys', 'Purples', 'Blues', 'Greens', 'Oranges', 'Reds',
            'YlOrBr', 'YlOrRd', 'OrRd', 'PuRd', 'RdPu', 'BuPu',
            'GnBu', 'PuBu', 'YlGnBu', 'PuBuGn', 'BuGn', 'YlGn', 'binary', 'gist_yarg', 'gist_gray', 'gray', 'bone', 'pink',
            'spring', 'summer', 'autumn', 'winter', 'cool', 'Wistia',
            'hot', 'afmhot', 'gist_heat', 'copper',
            'PiYG', 'PRGn', 'BrBG', 'PuOr', 'RdGy', 'RdBu',
            'RdYlBu', 'RdYlGn', 'Spectral', 'coolwarm', 'bwr', 'seismic',
            'twilight', 'twilight_shifted', 'hsv',
            'flag', 'prism', 'ocean', 'gist_earth', 'terrain', 'gist_stern',
            'gnuplot', 'gnuplot2', 'CMRmap', 'cubehelix', 'brg',
            'gist_rainbow', 'rainbow', 'jet', 'nipy_spectral', 'gist_ncar'
]


OUT = {}
for name in ALL:
    cmap = plt.get_cmap("RdYlGn", N)
    out = []
    for i in range(cmap.N):
        rgb = cmap(i)[:3] # will return rgba, we take only first 3 so we get rgb
        out.append(cm.colors.rgb2hex(rgb))
    print("export const {} = [{}];".format(name, ", ".join(['"{}"'.format(col) for col in out])))
*/
/**
 * @param {array.<string>} colorMap
 * @param {number} angle
 * @returns {string}
 */
export function getGradient(colorMap, angle = 90){
  return `linear-gradient(${angle}deg, ${colorMap.join(", ")})`;
}

export const viridis = ["#440154", "#481467", "#482576", "#453781", "#404688", "#39558c", "#33638d", "#2d718e", "#287d8e", "#238a8d", "#1f968b", "#20a386", "#29af7f", "#3dbc74", "#56c667", "#75d054", "#95d840", "#bade28", "#dde318", "#fde725"];
export const plasma = ["#0d0887", "#2c0594", "#43039e", "#5901a5", "#6e00a8", "#8305a7", "#9511a1", "#a72197", "#b6308b", "#c5407e", "#d14e72", "#dd5e66", "#e76e5b", "#f07f4f", "#f79044", "#fca338", "#feb72d", "#fccd25", "#f7e225", "#f0f921"];
export const inferno = ["#000004", "#08051d", "#180c3c", "#2f0a5b", "#450a69", "#5c126e", "#71196e", "#87216b", "#9b2964", "#b1325a", "#c43c4e", "#d74b3f", "#e55c30", "#f1711f", "#f8870e", "#fca108", "#fbba1f", "#f6d543", "#f1ed71", "#fcffa4"];
export const magma = ["#000004", "#07061c", "#150e38", "#29115a", "#3f0f72", "#56147d", "#6a1c81", "#802582", "#942c80", "#ab337c", "#c03a76", "#d6456c", "#e85362", "#f4695c", "#fa815f", "#fd9b6b", "#feb47b", "#fecd90", "#fde5a7", "#fcfdbf"];
export const cividis = ["#00224e", "#002c64", "#0c3470", "#273e6e", "#38476c", "#46516c", "#535a6d", "#5f636f", "#6b6d72", "#777776", "#828079", "#908b78", "#9c9576", "#aaa073", "#b8aa6e", "#c6b667", "#d4c15f", "#e4ce53", "#f2da44", "#fee838"];
export const Greys = ["#ffffff", "#f9f9f9", "#f2f2f2", "#eaeaea", "#e0e0e0", "#d6d6d6", "#cacaca", "#bebebe", "#afafaf", "#9e9e9e", "#8f8f8f", "#808080", "#717171", "#636363", "#555555", "#444444", "#313131", "#1f1f1f", "#101010", "#000000"];
export const Purples = ["#fcfbfd", "#f7f5fa", "#f1eff6", "#e9e8f2", "#e1e0ee", "#d7d7e9", "#cacbe3", "#bebfdd", "#b1b0d5", "#a4a1cc", "#9894c5", "#8b88bf", "#7f7bb9", "#7668af", "#6c56a5", "#63449d", "#5a3294", "#51218c", "#481085", "#3f007d"];
export const Blues = ["#f7fbff", "#ecf4fc", "#e2eef8", "#d8e7f5", "#cee0f2", "#c2d9ee", "#b1d2e8", "#a0cbe2", "#8bc0dd", "#76b4d8", "#62a8d3", "#519ccc", "#4090c5", "#3282be", "#2474b7", "#1967ad", "#0f59a3", "#084c94", "#083e80", "#08306b"];
export const Greens = ["#f7fcf5", "#eff9ec", "#e8f6e3", "#ddf2d8", "#d0edca", "#c3e7bc", "#b3e1ad", "#a3da9d", "#90d18d", "#7dc87e", "#69bf71", "#54b466", "#3fa95c", "#339c52", "#268e48", "#18823d", "#097533", "#006729", "#005522", "#00441b"];
export const Oranges = ["#fff5eb", "#ffefdf", "#fee8d3", "#fee0c2", "#fdd7b0", "#fdcc9c", "#fdbe85", "#fdb06e", "#fda25a", "#fd9446", "#fa8533", "#f57622", "#f06712", "#e6590a", "#dc4b03", "#c94202", "#b33b02", "#a03403", "#8f2d04", "#7f2704"];
export const Reds = ["#fff5f0", "#ffece3", "#fee3d7", "#fdd6c5", "#fdc7b0", "#fcb79c", "#fca588", "#fc9474", "#fc8363", "#fb7252", "#f86044", "#f34c37", "#ed392b", "#de2a25", "#cf1c1f", "#bf151a", "#af1117", "#9b0d14", "#810610", "#67000d"];
export const YlOrBr = ["#ffffe5", "#fffcd4", "#fff8c2", "#fff2b1", "#fee99f", "#fee08a", "#fed36e", "#fec652", "#feb441", "#fea231", "#fa9025", "#f37f1c", "#ea6e13", "#dd5f0b", "#cf5004", "#bc4403", "#a63a03", "#913204", "#7b2b05", "#662506"];
export const YlOrRd = ["#ffffcc", "#fff7b9", "#fff0a7", "#ffe895", "#fedf83", "#fed572", "#fec460", "#feb44e", "#fea446", "#fd953f", "#fd8038", "#fc6531", "#fb4b29", "#f03523", "#e61f1d", "#d7121f", "#c70723", "#b30026", "#9a0026", "#800026"];
export const OrRd = ["#fff7ec", "#fff1dd", "#feeace", "#fee3bd", "#fddaab", "#fdd19b", "#fdc790", "#fdbc85", "#fdaa74", "#fc9762", "#f98555", "#f4744e", "#ee6246", "#e44c35", "#da3623", "#cc2115", "#bc0d08", "#ab0000", "#950000", "#7f0000"];
export const PuRd = ["#f7f4f9", "#f0ecf5", "#eae4f1", "#e2d6e9", "#dac6e1", "#d3b5d8", "#cea6d0", "#ca96c8", "#d183bf", "#da6fb5", "#e158a8", "#e43f98", "#e62887", "#db1e71", "#d1145b", "#bd0c50", "#a60548", "#90003d", "#7c002e", "#67001f"];
export const RdPu = ["#fff7f3", "#feedea", "#fde4e0", "#fdd9d5", "#fccec9", "#fcc1bf", "#fbb1ba", "#faa1b6", "#f98bae", "#f874a5", "#f25d9f", "#e7479b", "#db3196", "#c71c8b", "#b30681", "#9e017c", "#880179", "#720175", "#5e006f", "#49006a"];
export const BuPu = ["#f7fcfd", "#edf5f9", "#e4eff5", "#d7e5f0", "#c9dbea", "#bcd1e5", "#aec7e0", "#a0bddb", "#97aed3", "#909eca", "#8c8dc2", "#8c7bb9", "#8c69b0", "#8a57a8", "#88459f", "#863193", "#831c85", "#790d74", "#630660", "#4d004b"];
export const GnBu = ["#f7fcf0", "#edf8e7", "#e4f4de", "#dbf1d5", "#d2eecc", "#c8eac3", "#b9e4bd", "#aadeb6", "#97d7bb", "#84d0c1", "#72c7c7", "#5fbccd", "#4cb1d2", "#3da1c9", "#2f90c0", "#2081b8", "#1171b1", "#0862a5", "#085193", "#084081"];
export const PuBu = ["#fff7fb", "#f7f0f7", "#efeaf3", "#e5e1ef", "#d9d8ea", "#cccfe5", "#bac6e0", "#a8bedc", "#94b6d7", "#7fadd2", "#67a4cc", "#4d99c6", "#338ebf", "#1f81b8", "#0a73b2", "#0569a5", "#046096", "#045585", "#03466e", "#023858"];
export const YlGnBu = ["#ffffd9", "#f7fcc8", "#f0f9b7", "#e3f4b2", "#d3eeb3", "#bfe6b5", "#a1dab8", "#83cebb", "#68c5be", "#4ebbc2", "#39aec3", "#2a9fc1", "#1d8ebf", "#1f79b5", "#2163ab", "#2351a2", "#243f99", "#20308b", "#142771", "#081d58"];
export const PuBuGn = ["#fff7fb", "#f7eef6", "#efe5f2", "#e5deed", "#d9d6e9", "#cccfe5", "#bac6e0", "#a8bedc", "#8fb6d7", "#74add2", "#5da4cc", "#4899c6", "#338fbd", "#1d89a6", "#078390", "#027a7b", "#017266", "#016653", "#015645", "#014636"];
export const BuGn = ["#f7fcfd", "#eff9fb", "#e8f6fa", "#def3f4", "#d4efec", "#c7eae3", "#b1e1d7", "#9cd9cb", "#86d0bb", "#71c7ac", "#5ebe9a", "#4fb587", "#3fac73", "#339d5f", "#268f4a", "#18823d", "#097533", "#006729", "#005522", "#00441b"];
export const YlGn = ["#ffffe5", "#fcfed2", "#f8fcc0", "#eff9b3", "#e2f4aa", "#d4eea1", "#c2e698", "#afde8f", "#99d586", "#83cb7d", "#6cc073", "#55b567", "#3fa95c", "#339951", "#268846", "#187b3f", "#096f3a", "#006235", "#00542f", "#004529"];
export const binary = ["#ffffff", "#f2f2f2", "#e4e4e4", "#d7d7d7", "#c9c9c9", "#bcbcbc", "#aeaeae", "#a1a1a1", "#949494", "#868686", "#797979", "#6b6b6b", "#5e5e5e", "#515151", "#434343", "#363636", "#282828", "#1b1b1b", "#0d0d0d", "#000000"];
export const gist_yarg = ["#ffffff", "#f2f2f2", "#e4e4e4", "#d7d7d7", "#c9c9c9", "#bcbcbc", "#aeaeae", "#a1a1a1", "#949494", "#868686", "#797979", "#6b6b6b", "#5e5e5e", "#515151", "#434343", "#363636", "#282828", "#1b1b1b", "#0d0d0d", "#000000"];
export const gist_gray = ["#000000", "#0d0d0d", "#1b1b1b", "#282828", "#363636", "#434343", "#515151", "#5e5e5e", "#6b6b6b", "#797979", "#868686", "#949494", "#a1a1a1", "#aeaeae", "#bcbcbc", "#c9c9c9", "#d7d7d7", "#e4e4e4", "#f2f2f2", "#ffffff"];
export const gray = ["#000000", "#0d0d0d", "#1b1b1b", "#282828", "#363636", "#434343", "#515151", "#5e5e5e", "#6b6b6b", "#797979", "#868686", "#949494", "#a1a1a1", "#aeaeae", "#bcbcbc", "#c9c9c9", "#d7d7d7", "#e4e4e4", "#f2f2f2", "#ffffff"];
export const bone = ["#000000", "#0c0c10", "#171721", "#232331", "#2f2f41", "#3b3b52", "#464662", "#525272", "#5e637e", "#6a738a", "#758395", "#8193a1", "#8da3ad", "#99b3b9", "#a4c4c4", "#b6d0d0", "#c8dcdc", "#dae8e8", "#edf3f3", "#ffffff"];
export const pink = ["#1e0000", "#4e3030", "#6b4444", "#815353", "#946060", "#a56b6b", "#b47575", "#c27f7e", "#c89287", "#cda38f", "#d3b397", "#d8c19e", "#ddcea5", "#e3dbac", "#e8e6b3", "#ececc3", "#f1f1d3", "#f6f6e3", "#fafaf1", "#ffffff"];
export const spring = ["#ff00ff", "#ff0df2", "#ff1be4", "#ff28d7", "#ff36c9", "#ff43bc", "#ff51ae", "#ff5ea1", "#ff6b94", "#ff7986", "#ff8679", "#ff946b", "#ffa15e", "#ffae51", "#ffbc43", "#ffc936", "#ffd728", "#ffe41b", "#fff20d", "#ffff00"];
export const summer = ["#008066", "#0d8666", "#1b8d66", "#289466", "#369a66", "#43a166", "#51a866", "#5eae66", "#6bb566", "#79bc66", "#86c366", "#94c966", "#a1d066", "#aed766", "#bcdd66", "#c9e466", "#d7eb66", "#e4f266", "#f2f866", "#ffff66"];
export const autumn = ["#ff0000", "#ff0d00", "#ff1b00", "#ff2800", "#ff3600", "#ff4300", "#ff5100", "#ff5e00", "#ff6b00", "#ff7900", "#ff8600", "#ff9400", "#ffa100", "#ffae00", "#ffbc00", "#ffc900", "#ffd700", "#ffe400", "#fff200", "#ffff00"];
export const winter = ["#0000ff", "#000df8", "#001bf2", "#0028eb", "#0036e4", "#0043dd", "#0051d7", "#005ed0", "#006bc9", "#0079c3", "#0086bc", "#0094b5", "#00a1ae", "#00aea8", "#00bca1", "#00c99a", "#00d794", "#00e48d", "#00f286", "#00ff80"];
export const cool = ["#00ffff", "#0df2ff", "#1be4ff", "#28d7ff", "#36c9ff", "#43bcff", "#51aeff", "#5ea1ff", "#6b94ff", "#7986ff", "#8679ff", "#946bff", "#a15eff", "#ae51ff", "#bc43ff", "#c936ff", "#d728ff", "#e41bff", "#f20dff", "#ff00ff"];
export const Wistia = ["#e4ff7a", "#eafa66", "#eff552", "#f5f03d", "#fbec29", "#ffe619", "#ffdd13", "#ffd40e", "#ffcb08", "#ffc203", "#ffba00", "#ffb400", "#ffae00", "#ffa800", "#ffa200", "#ff9b00", "#fe9400", "#fd8d00", "#fd8600", "#fc7f00"];
export const hot = ["#0b0000", "#2e0000", "#510000", "#740000", "#980000", "#bb0000", "#de0000", "#ff0200", "#ff2500", "#ff4900", "#ff6c00", "#ff8f00", "#ffb200", "#ffd600", "#fff900", "#ffff2c", "#ffff60", "#ffff95", "#ffffca", "#ffffff"];
export const afmhot = ["#000000", "#1b0000", "#360000", "#510000", "#6b0000", "#860700", "#a12200", "#bc3c00", "#d75700", "#f27200", "#ff8d0d", "#ffa828", "#ffc343", "#ffdd5e", "#fff879", "#ffff94", "#ffffae", "#ffffc9", "#ffffe4", "#ffffff"];
export const gist_heat = ["#000000", "#140000", "#280000", "#3c0000", "#510000", "#650000", "#790000", "#8d0000", "#a10000", "#b50000", "#c90d00", "#dd2800", "#f24300", "#ff5e00", "#ff7900", "#ff9428", "#ffae5e", "#ffc994", "#ffe4c9", "#ffffff"];
export const copper = ["#000000", "#110a07", "#21150d", "#321f14", "#422a1b", "#533421", "#633f28", "#74492f", "#855435", "#955e3c", "#a66943", "#b67349", "#c77e50", "#d88857", "#e8935d", "#f99d64", "#ffa86b", "#ffb272", "#ffbd78", "#ffc77f"];
export const PiYG = ["#8e0152", "#ab0f69", "#c62080", "#d35099", "#e07eb3", "#ea9fca", "#f3bddd", "#f9d3e8", "#fce5f1", "#f9f1f5", "#f3f6ed", "#eaf5d8", "#d7efb9", "#bfe492", "#a3d36d", "#85c048", "#6aaa34", "#509423", "#3b7c1d", "#276419"];
export const PRGn = ["#40004b", "#5c1668", "#782e85", "#8a539a", "#9d76af", "#b391c2", "#c8acd3", "#dbc5e0", "#eadbeb", "#f3eef3", "#eff5ee", "#dff1db", "#c9e9c3", "#aedea8", "#8aca89", "#62b368", "#3f974f", "#1e7b39", "#0e5f2a", "#00441b"];
export const BrBG = ["#543005", "#714108", "#8f540c", "#aa6d1e", "#c28835", "#d3aa60", "#e3c888", "#efdcad", "#f6ebce", "#f5f2e8", "#e9f2f1", "#d1ece8", "#b1e1da", "#8bd2c7", "#64b9af", "#3d9d94", "#1f827a", "#046961", "#015248", "#003c30"];
export const PuOr = ["#7f3b08", "#9a4a07", "#b55a07", "#cd700e", "#e3881c", "#f2a446", "#fdbe70", "#fed39c", "#fde5c4", "#f9f1e6", "#efeff4", "#dfe0ee", "#cccbe3", "#b8b2d6", "#a096c4", "#8579b0", "#6d539d", "#562b8a", "#42156b", "#2d004b"];
export const RdGy = ["#67001f", "#8e0d25", "#b41c2d", "#c7423f", "#d96753", "#e98c6e", "#f5ae8d", "#facab1", "#fde3d3", "#fef6f0", "#f7f7f7", "#e7e7e7", "#d4d4d4", "#c0c0c0", "#a7a7a7", "#8c8c8c", "#6f6f6f", "#505050", "#353535", "#1a1a1a"];
export const RdBu = ["#67001f", "#8e0d25", "#b41c2d", "#c7423f", "#d96753", "#e98c6e", "#f5ae8d", "#facab1", "#fce1d1", "#f9f0ea", "#edf2f5", "#d9e9f1", "#bddbea", "#9ccae1", "#75b3d4", "#4b98c6", "#3580b9", "#2368ad", "#144c88", "#053061"];
export const RdYlBu = ["#a50026", "#bf1927", "#d93328", "#e85337", "#f57446", "#fa9656", "#fdb668", "#fed081", "#fee79a", "#fff7b3", "#f7fcce", "#e7f6ec", "#cfebf3", "#b3ddeb", "#97c9e0", "#7ab2d4", "#6095c5", "#4778b6", "#3c57a5", "#313695"];
export const RdYlGn = ["#a50026", "#bf1927", "#d93328", "#e85337", "#f57446", "#fa9656", "#fdb668", "#fed07e", "#fee796", "#fff7b1", "#f5fbb1", "#e1f296", "#c9e881", "#aedc6f", "#8ecf67", "#6dc064", "#46ad5b", "#1e9a51", "#0e8144", "#006837"];
export const Spectral = ["#9e0142", "#bb2149", "#d7404e", "#e75948", "#f57446", "#fa9656", "#fdb668", "#fed07e", "#fee796", "#fff7b1", "#f8fcb5", "#ebf7a0", "#d3ed9c", "#b4e1a2", "#92d3a4", "#6dc5a5", "#50aaaf", "#358bbc", "#476db0", "#5e4fa2"];
export const coolwarm = ["#3b4cc0", "#4a63d4", "#5b7ae5", "#6d8ff1", "#7fa2fa", "#92b4fe", "#a4c2fe", "#b6cefa", "#c7d7f1", "#d6dce4", "#e4d9d3", "#eecfbf", "#f5c2aa", "#f7b295", "#f59f80", "#ef896c", "#e67158", "#d85646", "#c73735", "#b40426"];
export const bwr = ["#0000ff", "#1b1bff", "#3636ff", "#5151ff", "#6b6bff", "#8686ff", "#a1a1ff", "#bcbcff", "#d7d7ff", "#f2f2ff", "#fff2f2", "#ffd7d7", "#ffbcbc", "#ffa1a1", "#ff8686", "#ff6b6b", "#ff5151", "#ff3636", "#ff1b1b", "#ff0000"];
export const seismic = ["#00004c", "#000072", "#000098", "#0000bd", "#0000e3", "#0d0dff", "#4343ff", "#7979ff", "#aeaeff", "#e4e4ff", "#ffe4e4", "#ffaeae", "#ff7979", "#ff4343", "#ff0d0d", "#eb0000", "#d00000", "#b50000", "#9a0000", "#800000"];
export const twilight = ["#e2d9e2", "#cbd2d7", "#a3beca", "#80a5c3", "#6a8bbf", "#606eb8", "#5e4eab", "#5b2f94", "#4c176b", "#371143", "#381139", "#561546", "#792050", "#963350", "#ad4e51", "#bd6e5a", "#c78f72", "#cfaf99", "#dbcbc6", "#e2d9e2"];
export const twilight_shifted = ["#301437", "#411254", "#552081", "#5d3ea1", "#5f5fb3", "#647dbc", "#7499c1", "#8fb1c6", "#b6c8cf", "#d9d8de", "#dfd5d8", "#d4beaf", "#cb9f83", "#c37f64", "#b65e54", "#a34150", "#882850", "#67194c", "#44123f", "#2f1436"];
export const hsv = ["#ff0000", "#ff4f00", "#ff9f00", "#ffee00", "#c1ff00", "#72ff00", "#22ff00", "#00ff2d", "#00ff7c", "#00ffcb", "#00e3ff", "#0094ff", "#0045ff", "#0a00ff", "#5a00ff", "#a900ff", "#f700fe", "#ff00b6", "#ff0067", "#ff0018"];
export const flag = ["#ff0000", "#490000", "#000058", "#0415ff", "#c5eaff", "#ffc997", "#ef0000", "#2c0000", "#000078", "#1e3fff", "#e1f7ff", "#ffad78", "#d30000", "#100000", "#000097", "#3a66ff", "#fbfeff", "#ff8b58", "#b60000", "#000000"];
export const prism = ["#ff0000", "#00ab57", "#ff3600", "#0036e3", "#ffab00", "#2400ff", "#ffff00", "#8d00e3", "#8dff00", "#ff0057", "#24db00", "#ff0000", "#0072a5", "#ff7200", "#0000ff", "#ffdb00", "#5400ff", "#c9ff00", "#c900a5", "#54ff00"];
export const ocean = ["#008000", "#006b0d", "#00571b", "#004328", "#002f36", "#001b43", "#000751", "#000d5e", "#00226b", "#003679", "#004a86", "#005e94", "#0072a1", "#0d86ae", "#369abc", "#5eaec9", "#86c3d7", "#aed7e4", "#d7ebf2", "#ffffff"];
export const gist_earth = ["#000000", "#090f75", "#122e77", "#1b4a7a", "#24637c", "#2d787f", "#348575", "#3a8d65", "#419454", "#4e9c47", "#6ba44f", "#85aa54", "#9baf59", "#b1b55d", "#bbae61", "#c1a368", "#d0ad89", "#dfbcaa", "#eed7d3", "#fdfbfb"];
export const terrain = ["#333399", "#2157bd", "#0f7be1", "#009df3", "#00b8a2", "#0dcf69", "#43d973", "#79e47e", "#aeef89", "#e4fa94", "#f2ee92", "#d7cb83", "#bca975", "#a18766", "#866458", "#94766f", "#ae9893", "#c9bab7", "#e4dddb", "#ffffff"];
export const gist_stern = ["#000000", "#f50d1b", "#bf1b36", "#7c2851", "#39366b", "#434386", "#5151a1", "#5e5ebc", "#6b6bd7", "#7979f2", "#8686e2", "#9494a9", "#a1a170", "#aeae37", "#bcbc02", "#c9c934", "#d7d767", "#e4e49a", "#f2f2cc", "#ffffff"];
export const gnuplot = ["#000000", "#3b0053", "#53009d", "#6501d5", "#7502f7", "#8305fe", "#8f08ea", "#9b0dbc", "#a51379", "#b01b2a", "#b92500", "#c23100", "#cb4000", "#d35200", "#db6600", "#e37d00", "#ea9800", "#f1b700", "#f8d900", "#ffff00"];
export const gnuplot2 = ["#000000", "#000036", "#00006b", "#0000a1", "#0000d7", "#0a00ff", "#3400ff", "#5e00ff", "#8801fe", "#b21be4", "#dc36c9", "#ff51ae", "#ff6c93", "#ff8778", "#ffa25d", "#ffbc43", "#ffd728", "#fff20d", "#ffff57", "#ffffff"];
export const CMRmap = ["#000000", "#101036", "#20206b", "#302690", "#4026ab", "#5528b9", "#752d9e", "#953283", "#bf385f", "#ea3d39", "#fa4d1e", "#ef680e", "#e68301", "#e69e0c", "#e6b917", "#e6cb3a", "#e6db65", "#eaea94", "#f4f4c9", "#ffffff"];
export const cubehelix = ["#000000", "#130918", "#1a1732", "#192a47", "#15414e", "#17584a", "#246b3d", "#3f7632", "#647a30", "#8d7a3c", "#b17959", "#ca7b81", "#d485ac", "#d296d1", "#c9ade9", "#c2c5f3", "#c3dbf2", "#d0ecef", "#e6f7f1", "#ffffff"];
export const brg = ["#0000ff", "#1b00e4", "#3600c9", "#5100ae", "#6b0094", "#860079", "#a1005e", "#bc0043", "#d70028", "#f2000d", "#f20d00", "#d72800", "#bc4300", "#a15e00", "#867900", "#6b9400", "#51ae00", "#36c900", "#1be400", "#00ff00"];
export const gist_rainbow = ["#ff0029", "#ff1f00", "#ff6800", "#ffb000", "#fff900", "#bdff00", "#74ff00", "#2cff00", "#00ff1d", "#00ff65", "#00ffad", "#00fff5", "#00c0ff", "#0077ff", "#002eff", "#1b00ff", "#6400ff", "#ad00ff", "#f600ff", "#ff00bf"];
export const rainbow = ["#8000ff", "#652afe", "#4a53fc", "#2f79f7", "#149df1", "#07bcea", "#22d5e0", "#3cead5", "#57f7c9", "#72febc", "#8dfead", "#a8f79d", "#c3ea8b", "#ddd579", "#f8bc66", "#ff9d53", "#ff793f", "#ff532a", "#ff2a15", "#ff0000"];
export const jet = ["#000080", "#0000bd", "#0000fa", "#0022ff", "#0057ff", "#008dff", "#00c3ff", "#0ff8e8", "#3affbc", "#66ff91", "#91ff66", "#bcff3a", "#e8ff0f", "#ffd500", "#ffa400", "#ff7200", "#ff4000", "#fa0e00", "#bd0000", "#800000"];
export const nipy_spectral = ["#000000", "#780089", "#7a009b", "#0000b2", "#0019dd", "#0080dd", "#009ecd", "#00aa9d", "#00a34f", "#00a900", "#00cd00", "#00f100", "#76ff00", "#def300", "#fbd500", "#ffa400", "#ff1800", "#e10000", "#cd0000", "#cccccc"];
export const gist_ncar = ["#000080", "#005c0d", "#0001ec", "#00baff", "#00f9f4", "#00fa9b", "#0aff0f", "#5fce00", "#7efa05", "#baff3b", "#f7fc07", "#ffdb00", "#ffba0e", "#ff4c01", "#ff0a00", "#ff00ec", "#a72cff", "#e77bef", "#f4baf6", "#fef8fe"];