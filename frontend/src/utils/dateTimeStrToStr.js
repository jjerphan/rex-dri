
/**
 * Function to extract date time information from a string of format
 * "yyyy-mm-dd hh:mm:ss"
 *
 * @export
 * @param {string} dateTime
 * @returns {object}
 */
export default function dateTimeStrToStr(dateTime) {
  const reg = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/,
    res = reg.exec(dateTime),
    yyyy = res[1],
    mm = res[2],
    dd = res[3],
    hh = res[4],
    min = res[5],
    sec = res[6];

  return {
    date: `${dd}/${mm}/${yyyy}`,
    time: `${hh}h${min}`,
    fullTime: `${hh}h${min}min${sec}s`
  };
}
