/**
 * Function to get a regex object for money parsing
 *
 * @returns
 */
function getMoneyRegex() {
  // regex lookbehind is not supported in Firefox at this time
  // can't use it to detect code markdown: /(?<!`):(\d*[.,]?\d*)(\w{3}):/g;
  return /:(\d*[.,]?\d*)(\w{3}):/g;
}


/**
 * Parses a string to determine if there are some currency in it.
 *
 * For example, the string: "Hi, I earn :10.15CHF:" will be converted to:
 * [{ isMoney: false, text: 'Hi, I earn ' },
 * { isMoney: true, amount: '10.15', currency: 'CHF' }]
 *
 * In the string: amount can be an int, a float with ',' or '.' as separator
 *  And the currency can be in mixed case, but will always be return in uppercase.
 *
 * @export
 * @param {string} str
 * @returns {Array}
 */
export default function parseMoney(str) {
  if (str === "") {
    return [];
  }

  // reusable function
  const getOutputText = (str) => ({ isMoney: false, text: str });

  if (!getMoneyRegex().test(str)) {
    // if the string doesn't contain anything interesting
    return [getOutputText(str)];
  } else {

    let matches = [],
      match,
      moneyRegEx = getMoneyRegex();

    while ((match = moneyRegEx.exec(str)) !== null) {
      const matchStartIndex = match.index, // index of the starting ':'
        matchLastIndex = moneyRegEx.lastIndex - 1, // index of the ending ':'
        amount = parseFloat(match["1"].replace(",", ".")), // fix numbers with "," as decimal separators
        currency = match["2"].toUpperCase(); // make sure the currency is uppercase

      // regex lookbehind is not supported in Firefox at this time
      if (matchStartIndex !== 0 && str[matchStartIndex - 1] === "`") {
        // ignore it's for code
      } else {
        matches.push({ matchStartIndex, matchLastIndex, amount, currency });
      }
    }

    let res = [], lastIndex = 0;

    matches.forEach((el) => {
      if (lastIndex !== el.matchStartIndex) {
        // we need to add a classic string that was before the currency marker
        res.push(getOutputText(str.substring(lastIndex, el.matchStartIndex)));
      }
      // We add the element corresponding to money mount
      res.push({ isMoney: true, amount: el.amount, currency: el.currency });
      lastIndex = el.matchLastIndex + 1;
    });

    // we need to add the eventual trailing text:
    if (lastIndex !== str.length) {
      res.push(getOutputText(str.substring(lastIndex)));
    }

    return res;
  }

}

