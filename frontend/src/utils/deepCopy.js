/**
 * Quick & dirty way to deep copy an object
 * @param obj
 * @returns {object}
 */
export const deepCopy = obj => JSON.parse(JSON.stringify(obj));