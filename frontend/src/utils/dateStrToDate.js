/**
 * Converts a date of format "yyyy-mm-dd" to a js dates
 * mm is expected to start at 1 (and is corrected)
 *
 * @export
 * @param {string} dateTimeStr
 * @returns
 */
export default function dateStrToDate(dateTimeStr) {
  if (!dateTimeStr) {
    return null;
  }
  const reg = /(\d{4})-(\d{2})-(\d{2})/,
    res = reg.exec(dateTimeStr),
    yyyy = res[1],
    mm = res[2],
    dd = res[3];

  let d = new Date();
  d.setFullYear(yyyy, mm - 1, dd);
  return d;
}
