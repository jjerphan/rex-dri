import { getLatestReadDataFromStore } from "../redux/api/utils";

// For optimization
let currenciesSymbols = undefined;

/**
 * Function to get the symbol associated with a currency.
 * If no symbol is found, currencyStr is returned itsself.
 *
 * @export
 * @param {string} currencyStr
 * @returns
 */
export default function getCurrencySymbol(currencyStr) {
  if (currencyStr === "EUR") {
    return "€";
  }

  // a bit of optimization, will be useful if this function is used a lot
  if (typeof currenciesSymbols === "undefined") {
    currenciesSymbols = new Map();
    const currencies = getLatestReadDataFromStore("currenciesAll");
    if (currencies.length === 0) {
      // To use this function, you need to have currencies loaded in the store. Make sure the currencies are loaded in the backend.
      // Also, make sure not use this function before we have the currencies loaded :)
      throw new Error("Currencies in the store are empty see code for more info.");
    }
    currenciesSymbols = new Map();
    currencies
      .filter(c => c.symbol !== "") // We keep only the currencies with a symbol
      .forEach(c => currenciesSymbols.set(c.id, c.symbol));
  }

  if (currenciesSymbols.has(currencyStr)) {
    return currenciesSymbols.get(currencyStr);
  } else {
    return " " + currencyStr;
  }
}
