/**
 * Function that converts an array of instances of a model to a map.
 *
 * @export
 * @param {Array} arr
 * @param {string} [attrToUseAsKey="id"]
 * @returns {Map}
 */
export default function arrayOfInstancesToMap(arr, attrToUseAsKey = "id") {
  let out = new Map;
  arr.forEach(instance => out.set(instance[attrToUseAsKey], instance));
  return out;
}
