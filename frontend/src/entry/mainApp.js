import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {BrowserRouter as Router} from "react-router-dom";
import {SnackbarProvider} from "notistack"; // provider to easily handle notifications across the app
import Button from "@material-ui/core/Button";

import store from "../redux/store";
import App from "../components/app/App";
import ThemeProvider from "../components/common/theme/ThemeProvider";
import CssBaseline from "@material-ui/core/CssBaseline";

const MainReactEntry = () => (
  <Provider store={store}>
    {/* <React.StrictMode> */}
    <ThemeProvider>
      <Router>
        <SnackbarProvider
          maxSnack={3}
          anchorOrigin={{
            vertical: "top",
            horizontal: "right",
          }}
          action={[
            <Button key={0} color="secondary" size="small">
              {"Fermer"}
            </Button>
          ]}
        >
          <>
            <CssBaseline/>
            <App/>
          </>
        </SnackbarProvider>
      </Router>
    </ThemeProvider>
    {/* </React.StrictMode> */}
  </Provider>
);

const wrapper = document.getElementById("app");

wrapper ? ReactDOM.render(<MainReactEntry/>, wrapper) : null;

// eslint-disable-next-line no-undef
if (module.hot) {
  // eslint-disable-next-line no-undef
  module.hot.accept();
}
