import React from "react";
import ReactDOM from "react-dom";
import OfflineThemeProvider from "../components/common/theme/OfflineThemeProvider";
import CssBaseline from "@material-ui/core/CssBaseline";
import Logo from "../components/app/Logo";
import BaseTemplate from "../components/app/BaseTemplate";
import FormRgpdCgu from "../components/pages/FormRgpdCgu";

function MainReactEntry() {

  const toolbarContent = (
    <div style={{flex: 1}}>
      <Logo/>
    </div>
  );

  return (
    <OfflineThemeProvider>
      <>
        <CssBaseline/>
        <main>
          <BaseTemplate toolbarContent={toolbarContent}>
            <FormRgpdCgu/>
          </BaseTemplate>
        </main>
      </>
    </OfflineThemeProvider>
  );
}

const wrapper = document.getElementById("app");

wrapper ? ReactDOM.render(<MainReactEntry/>, wrapper) : null;

// eslint-disable-next-line no-undef
if (module.hot) {
  // eslint-disable-next-line no-undef
  module.hot.accept();
}
