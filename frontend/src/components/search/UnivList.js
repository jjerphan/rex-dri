import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import MobileStepper from "@material-ui/core/MobileStepper";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import SwipeableViews from "react-swipeable-views";
import range from "../../utils/range";
import {APP_ROUTES} from "../../config/appRoutes";
import isEqual from "lodash/isEqual";
import CustomNavLink from "../common/CustomNavLink";

/**
 * Component to provide a nice list of universities that can be swiped
 *
 * @class UnivList
 * @extends {React.Component}
 */
class UnivList extends React.Component {
  state = {
    activeStep: 0,
  };

  componentDidUpdate(prevProps) {
    // If the list is not the same, then make sure to "reset" and move to the first page
    if (!isEqual(prevProps.universitiesToList, this.props.universitiesToList)) {
      this.handleStepChange(0);
    }
  }


  handleNext() {
    this.setState(prevState => ({
      activeStep: prevState.activeStep + 1,
    }));
  }

  handleBack() {
    this.setState(prevState => ({
      activeStep: prevState.activeStep - 1,
    }));
  }

  handleStepChange(activeStep) {
    this.setState({activeStep});
  }

  render() {
    const {classes, theme, itemsPerPage, universitiesToList} = this.props,
      {activeStep} = this.state,
      numberOfItems = universitiesToList.length,
      maxSteps = Math.ceil(numberOfItems / itemsPerPage);

    // Prevent bug
    if (!numberOfItems) {
      return <></>;
    }

    function getIndexesOnPage(page) {
      const firstIdxOnPage = page * itemsPerPage,
        lastIdxOnPage = Math.min((page + 1) * itemsPerPage, numberOfItems);
      return range(lastIdxOnPage - firstIdxOnPage).map(el => el + firstIdxOnPage);
    }

    return (
      <div className={classes.root}>
        <SwipeableViews
          axis={theme.direction === "rtl" ? "x-reverse" : "x"}
          index={this.state.activeStep}
          onChangeIndex={(step) => this.handleStepChange(step)}
          enableMouseEvents
        >

          {range(maxSteps).map(page => (
            <div key={page}>
              <List component="nav">
                <Divider/>
                {getIndexesOnPage(page).map(
                  univIdx => (
                    <CustomNavLink to={APP_ROUTES.forUniversity(universitiesToList[univIdx].id)} key={univIdx}>
                      <ListItem button divider={true} key={univIdx}>
                        <ListItemText primary={universitiesToList[univIdx].name}/>
                      </ListItem>
                    </CustomNavLink>
                  )
                )}
              </List>
            </div>
          ))}
        </SwipeableViews>


        <MobileStepper
          steps={maxSteps}
          position="static"
          activeStep={activeStep}
          className={classes.mobileStepper}
          nextButton={
            <Button color="secondary" size="small" onClick={() => this.handleNext()}
                    disabled={activeStep >= maxSteps - 1}>
              Suivant
              {theme.direction === "rtl" ? <KeyboardArrowLeft/> : <KeyboardArrowRight/>}
            </Button>
          }
          backButton={
            <Button color="secondary" size="small" onClick={() => this.handleBack()} disabled={activeStep === 0}>
              {theme.direction === "rtl" ? <KeyboardArrowRight/> : <KeyboardArrowLeft/>}
              Précédent
            </Button>
          }
        />

      </div>
    );
  }
}

UnivList.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  universitiesToList: PropTypes.array.isRequired,
  itemsPerPage: PropTypes.number.isRequired,
};

UnivList.defaultProps = {
  itemsPerPage: 10
};

const styles = theme => ({
  root: {
    maxWidth: 650,
    marginLeft: "auto",
    marginRight: "auto",
    flexGrow: 1,
  },
  header: {
    display: "flex",
    alignItems: "center",
    height: 50,
    paddingLeft: theme.spacing(4),
    marginBottom: 20,
    backgroundColor: theme.palette.background.default,
  },
});

export default withStyles(styles, {withTheme: true})(UnivList);
