import React from "react";
import PropTypes from "prop-types";

import TextField from "@material-ui/core/TextField";

import CustomComponentForAPI from "../common/CustomComponentForAPI";
import {connect} from "react-redux";
import fuzzysort from "fuzzysort";
import UnivList from "./UnivList";
import withStyles from "@material-ui/core/styles/withStyles";
import getActions from "../../redux/api/getActions";


/**
 * Component to search through the universities
 *
 * @class Search
 * @extends {CustomComponentForAPI}
 * @extends React.Component
 *
 */
class Search extends CustomComponentForAPI {

  state = {
    input: "",
  };


  /**
   * Get the university suggestions for the `input` string.
   *
   * @param {string} input
   * @memberof Search
   */
  getSuggestions() {
    const filteredUniversities = this.props.selectedUniversities,
      universities = this.getLatestReadData("universities"),
      possibleUniversities = filteredUniversities.length === 0 ?
        universities
        :
        universities.filter(univ => filteredUniversities.includes(univ.id)),
      filter = fuzzysort.go(
        this.state.input,
        possibleUniversities,
        {keys: ["name", "acronym"]}
      );

    let suggestions = filter.map(item => item.obj);
    if (suggestions.length === 0) {
      suggestions = possibleUniversities;
    }
    return suggestions;
  }

  customRender() {
    const suggestions = this.getSuggestions();
    const {classes} = this.props;

    return (
      <>
        <TextField
          id="full-width"
          label=""
          InputLabelProps={{
            shrink: true,
          }}
          placeholder="Nom ou acronyme..."
          fullWidth
          margin="normal"
          InputProps={{classes: {input: classes.inputCentered}}}
          onChange={(e) => this.setState({input: e.target.value})}
        />
        <UnivList universitiesToList={suggestions}/>
      </>
    );
  }
}

Search.propTypes = {
  classes: PropTypes.object.isRequired,
  selectedUniversities: PropTypes.array.isRequired
};


const mapStateToProps = (state) => {
  return {
    universities: state.api.universitiesAll,
    mainCampuses: state.api.mainCampusesAll,
    cities: state.api.citiesAll,
    countries: state.api.countriesAll,
    selectedUniversities: state.app.selectedUniversities
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    api: {
      universities: () => dispatch(getActions("universities").readAll()),
      mainCampuses: () => dispatch(getActions("mainCampuses").readAll()),
      cities: () => dispatch(getActions("cities").readAll()),
      countries: () => dispatch(getActions("countries").readAll())
    },
  };
};

const styles = {
  inputCentered: {
    textAlign: "center"
  },
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Search));
