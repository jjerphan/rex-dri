import React from "react";
import PropTypes from "prop-types";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import BaseMarkdown from "../common/markdown/BaseMarkdown";
import Button from "@material-ui/core/Button";
import getActions from "../../redux/api/getActions";
import {compose} from "recompose";
import {connect} from "react-redux";
import {RequestParams} from "../../redux/api/RequestParams";


const deleteMessage = `
# Attention

* Cette action entrainera la suppression de toute vos données sous licence « REX-DRI Private » 
(login UTC, adresses mails, pseudos, listes privée, réglages personnels, etc.),
* Afin de satisfaire les exigences de la licence « REX-DRI BY », votre nom et prénom seront conservés,
* Pour toute demande supplémentaire, merci de contacter le SIMDE.
* La suppression de votre compte sera effective **sous 48h**.

*La suppression d'un compte consiste donc au vidage des informations personnelles associées.*

# Rappel

* Cette action sera de toute manière automatiquement réalisée passer 5 ans d'inactivité de votre compte.

*Si vous souhaitez définitivement supprimer votre compte, cliquez sur le bouton ci-dessous.*
`;


function DeleteAccount(props) {
  const {deleteProgrammed} = props;

  if (deleteProgrammed) {
    return (
      <>
        <Button fullWidth
                color={"primary"}
                variant={"contained"}
                onClick={() => props.undoDeleteAccount()}>
          La suppression de votre compte interviendra sous 48h. Pour annuler le processus, cliquez sur ce bouton.
        </Button>
      </>
    );

  } else {
    return (
      <>
        <ExpansionPanel>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
            <Typography variant={"caption"}>Supprimer mon compte</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails style={{display: "block"}}>
            <BaseMarkdown source={deleteMessage} headingOffset={3}/>
            <Button fullWidth variant={"outlined"} onClick={() => props.deleteAccount()}>
              Je demande la suppression de mon compte
            </Button>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </>
    );
  }
}

DeleteAccount.propTypes = {
  deleteProgrammed: PropTypes.bool.isRequired,
  deleteAccount: PropTypes.func.isRequired,
  undoDeleteAccount: PropTypes.func.isRequired,
};


const mapDispatchToProps = (dispatch) => {
  return {
    deleteAccount: () => {
      const params = RequestParams.Builder
        .withOnSuccessCallback(() => dispatch(getActions("users").invalidateOne()))
        .build();

      return dispatch(getActions("emptyUserAccount").create(params));
    },
    undoDeleteAccount: () => {
      const params = RequestParams.Builder
        .withOnSuccessCallback(() => dispatch(getActions("users").invalidateOne()))
        .withId("oserf")
        .build();
      return dispatch(getActions("emptyUserAccount").delete(params));
    }
  };
};

export default compose(
  connect(() => ({}), mapDispatchToProps),
)(DeleteAccount);

