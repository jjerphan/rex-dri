import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import compose from "recompose/compose";
import {connect} from "react-redux";

import {withSnackbar} from "notistack";
import Form from "../form/Form";
import editorStyle from "../editor/editorStyle";
import TextField from "../form/fields/TextField";
import Editor from "../editor/Editor";
import getMapStateToPropsForEditor from "../editor/getMapStateToPropsForEditor";
import getMapDispatchToPropsForEditor from "../editor/getMapDispatchToPropsForEditor";
import BooleanField from "../form/fields/BooleanField";
import {getLatestReadDataFromStore} from "../../redux/api/utils";
import FormLevelError from "../form/FormLevelError";


const styles = theme => ({
  ...editorStyle(theme)
});


class UserInfoForm extends Form {

  /**
   * @override
   */
  formLevelErrors = [
    new FormLevelError(
      ["allow_sharing_personal_info"],
      (sharingAllowed) => {
        const userData = getLatestReadDataFromStore("userDataOne"),
          {owner_level: userLevel} = userData;
        return userLevel > 0 && !sharingAllowed;
      },
      "Les modérateurs, membres de la DRI ainsi que les administrateurs du site sont forcés d'avoir un profil publique.")
  ];

  render() {
    return (
      <>
        <BooleanField label={"Autorisation de partage"}
                      {...this.getReferenceAndValue("allow_sharing_personal_info")}
                      required={true}
                      labelIfTrue="(Le partage est activé)"
                      labelIfFalse="(Le partage est désactivé)"/>

        <TextField label={"Pseudo"}
                   maxLength={12}
                   {...this.getReferenceAndValue("pseudo")}
                   required={true}/>

        <TextField label={"Adresse email secondaire"}
                   maxLength={300}
                   {...this.getReferenceAndValue("secondary_email")}
                   required={false}/>
      </>
    );
  }
}


class UserInfoEditor extends Editor {
  renderForm() {
    return <UserInfoForm
      modelData={this.props.rawModelData}
      ref={this.formRef}
    />;
  }
}


export default compose(
  withSnackbar,
  withStyles(styles, {withTheme: true}),
  connect(
    getMapStateToPropsForEditor("users"),
    getMapDispatchToPropsForEditor("users")
  )
)(UserInfoEditor);
