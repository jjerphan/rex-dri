import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import SendIcon from "@material-ui/icons/Send";
import CancelIcon from "@material-ui/icons/Cancel";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import CustomComponentForAPI from "../common/CustomComponentForAPI";
import getActions from "../../redux/api/getActions";
import {connect} from "react-redux";
import SameLine from "../common/SameLine";
import {compose} from "recompose";
import UserInfoEditor from "./UserInfoEditor";
import CreateIcon from "@material-ui/icons/Create";
import {classNames} from "../../utils/classNames";
import {RequestParams} from "../../redux/api/RequestParams";
import DeleteAccount from "./DeleteAccount";

function TypographyWithIcon(props) {
  return (
    <SameLine>
      <Typography variant={props.variant}
                  className={props.typographyClass}>
        {props.text}
      </Typography>
      {props.public ? <CheckCircleIcon color="primary"/> : <CancelIcon color="disabled"/>}
    </SameLine>
  );
}

TypographyWithIcon.defaultProps = {
  typographyClass: "",
};

TypographyWithIcon.propTypes = {
  public: PropTypes.bool.isRequired,
  variant: PropTypes.string.isRequired,
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired,
  typographyClass: PropTypes.string.isRequired,
};


/**
 * Component used to display the user information
 *
 * @class UserInfo
 * @extends {CustomComponentForAPI}
 */
class UserInfo extends CustomComponentForAPI {
  /**
   * @override
   * @type {boolean}
   */
  enableSmartDataRefreshOnComponentDidUpdate = true;

  apiParams = {
    "user": ({props}) =>
      RequestParams.Builder
        .withId(props.userId)
        .build(),
  };

  state = {editorOpen: false};

  openEditorPanel() {
    this.setState({editorOpen: true});
  }

  closeEditorPanel() {
    this.setState({editorOpen: false});
  }

  customRender() {
    const {classes} = this.props,
      userModelData = this.getLatestReadData("user"),
      {
        allow_sharing_personal_info: userSharesInfo,
        email,
        first_name: firstName,
        last_name: lastName,
        pseudo,
        secondary_email: secondaryEmail,
        username: login,
        id,
        delete_next_time: deleteProgrammed,
      } = userModelData,
      // eslint-disable-next-line no-undef
      isOwner = parseInt(__AppUserId) === parseInt(id),
      displayData = isOwner || userSharesInfo;

    return (
      <div className={classes.root}>
        <div className={classNames(classes.wrapper, classes.centered)}>
          <TypographyWithIcon public={userSharesInfo}
                              variant="h3"
                              text={displayData && firstName && lastName ? `${firstName} ${lastName.toUpperCase()}` : "Prénom et Nom"}/>

          <TypographyWithIcon public={userSharesInfo}
                              variant="h5"
                              text={displayData ? login : "Login UTC"}
                              typographyClass={classes.login}/>

          <Typography variant="caption">Les prénom, nom et login sont fournis par le CAS de l'UTC.</Typography>


          <div className={classes.spacer}/>


          <TypographyWithIcon public={true}
                              variant="h6"
                              text="Pseudo"/>

          <Typography variant="body1"><em>{pseudo}</em></Typography>


          <div className={classes.spacer}/>


          <TypographyWithIcon public={userSharesInfo}
                              variant="h6"
                              text="Adresse mail UTC"/>
          {
            displayData && email ?
              <>
                <Button
                  variant="outlined"
                  color="secondary"
                  className={classes.button}
                  href={`mailto:${email}`}
                >
                  {email} <SendIcon className={classes.rightIcon}/>
                </Button>
                <Typography variant="caption">
                  Cette adresse mail a été fournie par le CAS de l'UTC lors de la connexion de la personne.
                  Si la personne n"est plus à l"UTC, il se peut que cette adresse ne soit plus d'actualité.
                </Typography>
              </>
              :
              <Typography variant="caption">
                Aucune adresse mail rattachée à ce compte (compte créé hors CAS).
              </Typography>
          }


          <div className={classes.spacer}/>


          <TypographyWithIcon public={userSharesInfo}
                              variant={"h6"}
                              text={"Autre adresse de contact"}/>
          {
            displayData && secondaryEmail !== null && secondaryEmail !== "" ?
              <>
                <Button variant="outlined"
                        color="secondary"
                        className={classes.button}
                        href={`mailto:${secondaryEmail}`}>
                  {secondaryEmail} <SendIcon className={classes.rightIcon}/>
                </Button>
                <Typography variant="caption">
                  Cette adresse mail a été mise à disposition par la personne comme autre point de contact.
                </Typography>
              </>
              :
              <Typography variant="caption">
                Aucune addresse email secondaire n'est disponible.
              </Typography>
          }


          <div className={classes.spacer}/>

          {
            isOwner ?
              <>
                {
                  !deleteProgrammed ?
                    <>
                      <div style={{width: "100%"}}>
                        <Button variant={"contained"}
                                color={"primary"}
                                fullWidth
                                onClick={() => this.openEditorPanel()}>
                          Éditer<CreateIcon className={classes.rightIcon}/>
                        </Button>
                      </div>
                      <UserInfoEditor open={this.state.editorOpen}
                                      closeEditorPanel={() => this.closeEditorPanel()}
                                      rawModelData={userModelData}/>
                    </> : <></>
                }
                <div className={classes.deleteContainer}>
                  <DeleteAccount deleteProgrammed={deleteProgrammed}/>
                </div>
              </>
              :
              <></>
          }

        </div>
      </div>
    );
  }
}

UserInfo.propTypes = {
  userId: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
};


const mapStateToProps = (state) => {
  return {
    user: state.api.usersOne,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    api: {
      user: (params) => dispatch(getActions("users").readOne(params))
    },
  };
};

const styles = theme => ({
  root: {
    width: "100%",
  },
  wrapper: {
    maxWidth: "800px",
  },
  button: {
    margin: theme.spacing(1),
  },
  rightIcon: {
    marginLeft: theme.spacing(1),
  },
  login: {
    fontFamily: "monospace",
  },
  deleteContainer: {
    margin: theme.spacing(2)
  },
  spacer: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1),
  },
  centered: {
    margin: "0 auto",
    display: "block",
  }
});


export default compose(
  withStyles(styles, {withTheme: true}),
  connect(mapStateToProps, mapDispatchToProps),
)(UserInfo);
