import { getLatestRead } from "../../redux/api/utils";


/**
 * Function to create the mapStateToProps function for editor in a "generic way"
 *
 * @export
 * @param {string} elKey
 * @returns
 */
export default function getMapStateToPropsForEditor(elKey) {
  const propName = `${elKey}One`;

  return (state) => {
    let lastUpdateTimeInModel = null;

    // We make sure to get the latest data
    const latestRead = getLatestRead(state.api[propName]);
    if (latestRead.readAt !== 0) {
      lastUpdateTimeInModel = latestRead.data.updated_on;
    }

    // simple way to retreive the save error
    const updateFailed = state.api[propName].updateFailed,
      createFailed = state.api[propName].createFailed;

    let savingHasError = updateFailed;
    if (createFailed.failed) {
      savingHasError = createFailed;
    }

    return {
      savingHasError,
      lastUpdateTimeInModel
    };
  };
}
