import getActions from "../../redux/api/getActions";
import { openFullScreenDialog, closeFullScreenDialog } from "../../redux/actions/fullScreenDialog";
import {RequestParams} from "../../redux/api/RequestParams";

/**
 * Function to create the mapDispatchToProps function for editor in a "generic way"
 *
 * @export
 * @param {string} name
 * @returns
 */
export default function getMapDispatchToPropsForEditor(name) {
  return (dispatch) => {
    let lastSave = "update";
    return {
      // eslint-disable-next-line no-unused-vars
      saveData: (data, onSuccessCallback = new Function()) => {
        if ("id" in data) { // it's an update
          lastSave = "update";
          const params = RequestParams.Builder
            .withId(data.id)
            .withData(data)
            .withOnSuccessCallback(onSuccessCallback)
            .build();
          dispatch(getActions(name).update(params));
        } else { // it's a create
          lastSave = "create";
          const params = RequestParams.Builder
            .withData(data)
            .withOnSuccessCallback(onSuccessCallback)
            .build();
          dispatch(getActions(name).create(params));
        }
      },
      openFullScreenDialog: (innerNodes) => dispatch(openFullScreenDialog(innerNodes)),
      closeFullScreenDialog: () => dispatch(closeFullScreenDialog()),
      clearSaveError: () => {
        // we need to clear the correct error
        // depending on if it was a save or a create
        switch (lastSave) {
          case "update":
            dispatch(getActions(name).clearUpdateFailed());
            break;

          case "create":
            dispatch(getActions(name).clearCreateFailed());
            break;
        }
      },
    };
  };
}
