export default function editorStyle(theme) {
  return {
    appBar: {
      position: "sticky",
    },
    flex: {
      flex: 1,
    },
    paper: {
      padding: theme.spacing(2),
      margin: theme.spacing(2),
    }
  };
}
