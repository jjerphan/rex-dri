import React, {Component} from "react";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import Alert from "../common/Alert";
// Form is imported only for type hints
// eslint-disable-next-line no-unused-vars
import Form from "../form/Form";


/**
 * Class to handle editions of models on the frontend. It should be extended, eg:
 *
 * class BlablaEditor extends Editor {
 *   renderForm() {
 *     return <blablaForm
 *       modelData={this.props.rawModelData}
 *       ref={this.formRef}
 *     />;
 *   }
 * }
 *
 * @class Editor
 * @extends {Component}
 */
class Editor extends Component {

  // handling of the alert
  state = {
    alert: {
      open: false,
    },
  };

  // Store the "saving" notification so that we can delete it when done saving.
  savingNotification = null;


  // reference to the form that the editor should contain
  formRef = React.createRef();


  /**
   * Extra attributes that should be retrieved in rawModelData when parsing
   * I.e. the fields that are not present in the form but still need to be there
   *
   * @type {Array.<string>}
   */
  extraFieldMappings = [];

  /**
   * Creates an instance of Editor. and subscribes to the module wrapper so that it can access its functions.
   *
   * @param {object} props
   */
  constructor(props) {
    super(props);
    // editors can be used outside of a module wrapper.
    if ("subscribeToModuleWrapper" in props) {
      props.subscribeToModuleWrapper(this);
    }
  }

  /**
   * Function that extracts the modelData from the raw one.
   *
   * Basically we extract all field that can be edited in the form associated with the editor.
   * Plut the id.
   *
   * @returns
   */
  parseRawModelData() {
    let out = {};
    this.getForm()
      .getFields()
      .forEach(({fieldMapping}) => out[fieldMapping] = this.props.rawModelData[fieldMapping]);

    this.extraFieldMappings.forEach(fieldMapping => out[fieldMapping] = this.props.rawModelData[fieldMapping]);

    if ("id" in this.props.rawModelData) {
      const {id} = this.props.rawModelData;
      out.id = id;
    }

    return out;
  }

  /**
   * Returns the form instance associated with the editor
   *
   * @returns {Form}
   */
  getForm() {
    return this.formRef.current;
  }

  /////////
  // Shortcut functions to those of the form instance
  getFormError() {
    return this.getForm().getError();
  }

  getFormData() {
    return this.getForm().getDataFromFields();
  }

  formHasChanges() {
    return this.getForm().hasChanges();
  }

  // End of shortcut functions
  ////////////////////////


  /**
   * Function to handle save editor events, eg when clicking on the save button
   * This function is not trivial and checks are performed.
   *
   */
  handleSaveEditorRequest() {
    const getFormError = this.getFormError();
    if (!getFormError.status) { // no error, we can save if necessary

      if (this.formHasChanges()) {
        // Copy the model data and copy above the data from the form
        // So that we don't forget anything.
        this.performSave(
          Object.assign({}, this.parseRawModelData(this.props.rawModelData), this.getFormData())
        );
      } else {
        this.notifyNoChangesDetected();
        this.closeEditor(false);
      }

    } else {
      this.notifyFormHasErrors();
    }
  }


  /**
   * Function to save the `data` to the server.
   *
   * @param {Object} data
   */
  performSave(data) {
    this.notifyIsSaving();
    this.props.saveData(
      data,
      (newData) => this.handleSaveRequestWasSuccessful(newData)
    );
  }

  /**
   * Function that is called as soon as we now that the save request to the server was successful
   *
   * This enables not to use weird save detection method.
   *
   * @param {object} newData object returned by the server
   */
  handleSaveRequestWasSuccessful(newData) {
    // We check if data was moderated
    let message,
      lastUpdateTimeInModel = this.props.lastUpdateTimeInModel; // at this point it will be still the previously stored data
    const newUpdateTimeInModel = newData.updated_on;

    // Some models are not subject to moderation so they wan't have update_time
    // And the value will be undefined.
    if ( (typeof lastUpdateTimeInModel !== "undefined") && lastUpdateTimeInModel == newUpdateTimeInModel) {
      message = "Les données ont été enregistrées et sont en attentes de modération.";
    } else {
      lastUpdateTimeInModel = newUpdateTimeInModel;
      message = "Les données ont été enregistrées avec succès !";
    }

    this.notifySaveSuccessful(message);
    this.closeEditor(true);
  }


  /**
   * Function to handle close editor request from the user.
   * It checks if there is data to save or not.
   *
   */
  handleCloseEditorRequest() {
    if (this.formHasChanges()) {
      this.alertChangesNotSaved();
    } else {
      this.closeEditor(false);
    }
  }

  /**
   * Effectively close the editor window and notify if there was something new that was saved
   *
   * @param {Boolean} somethingWasSaved
   */
  closeEditor(somethingWasSaved) {
    this.props.closeFullScreenDialog();
    this.props.closeEditorPanel(somethingWasSaved);
  }


  /**
   * This function is extended to handle all the logic such as
   * - opening the editor
   * - Detecting when there was a successful save
   * - etc.
   *
   */
  componentDidUpdate() {

    // Open the editor if needed
    // don't check the if prevProps was false before, we need to rerender it anyway so that we can
    // display the alert.
    if (this.props.open) {
      this.props.openFullScreenDialog(this.renderEditor());
    }

    // we make to notify if saving to the server has errors
    const {savingHasError} = this.props;
    if (savingHasError.failed && !this.state.alert.open) {
      this.alertSaveFailed(JSON.stringify(savingHasError.error, null, 2));
    }
  }


  /**
   * Function to render the forms elements.
   * Basically something that extends the `Form` component
   *
   * @returns
   */
  renderForm() {
    throw new Error("The renderForm function must be extended in sub classes");
  }


  /**
   * Main rendering function. Its return value is passed to the
   * fullscreen app dialog through redux.
   *
   * @returns
   */
  renderEditor() {
    const {classes} = this.props;
    return (
      <>
        <Alert
          {...this.state.alert}
          handleClose={() => this.handleCloseAlertRequest()}
        />
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton color="inherit" onClick={() => this.handleCloseEditorRequest()} aria-label="Close">
              <CloseIcon/>
            </IconButton>
            <Typography variant="h6" color="inherit" className={classes.flex}>
              Mode édition
            </Typography>
            <Button color="inherit" onClick={() => this.handleSaveEditorRequest()}>
              Enregistrer
            </Button>
          </Toolbar>
        </AppBar>
        <Paper className={classes.paper}>
          {this.renderForm()}
        </Paper>
      </>
    );
  }

  render() {
    return <></>;
  }


  ///////////////////
  // Notification and alert related functions

  // Alerts related
  alertSaveFailed(error) {
    this.removeSavingNotification();
    this.setState({
      alert: {
        open: true,
        info: true,
        title: "L'enregistrement sur le serveur a échoué.",
        description: "Vous pourrez réessayer après avoir fermer cette alerte. Si l'erreur persiste, vérifier votre connexion internet ou contacter les administrateurs du site.\n\n" + error,
        infoText: "J'ai compris",
        handleResponse: () => {
          this.props.clearSaveError();
          this.setState({alert: {open: false}});
        }
      }
    });
  }

  alertChangesNotSaved() {
    this.setState({
      alert: {
        open: true,
        info: false,
        title: "Modifications non enregistrées !",
        description: "Vous avez des modifications qui n'ont pas été sauvegardées. Voulez-vous les enregistrer ?",
        agreeText: "Oui, je les enregistre",
        disagreeText: "Non",
        handleResponse: (agree) => {
          if (agree) {
            this.handleSaveEditorRequest();
          } else {
            this.closeEditor();
          }
        },
      }
    });
  }

  handleCloseAlertRequest() {
    this.setState({
      alert: {open: false}
    });
  }


  // Notifications related

  notifyNoChangesDetected() {
    this.props.enqueueSnackbar("Aucun changement n'a été repéré.", {variant: "info"});
  }

  notifyFormHasErrors() {
    this.props.enqueueSnackbar("Le formulaire semble incohérent, merci de vérifier son contenu.", {variant: "error"});
  }

  notifyIsSaving() {
    this.savingNotification = this.props.enqueueSnackbar("Enregistrement en cours", {
      variant: "info",
      persist: true,
      action: null
    });
  }

  notifySaveSuccessful(message) {
    setTimeout(() => this.removeSavingNotification(), 1500); // add a little delay for smoothing
    this.props.enqueueSnackbar(message, {variant: "success", autoHideDuration: 5000});
  }

  removeSavingNotification() {
    if (this.savingNotification) {
      this.props.closeSnackbar(this.savingNotification);
      this.savingNotification = null;
    }
  }

}


Editor.propTypes = {
  classes: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired, // should the editor be opened
  subscribeToModuleWrapper: PropTypes.func,
  rawModelData: PropTypes.object.isRequired,
  closeEditorPanel: PropTypes.func.isRequired,
  // props added in subclasses but are absolutely required to handle redux
  savingHasError: PropTypes.object.isRequired,
  clearSaveError: PropTypes.func.isRequired,
  lastUpdateTimeInModel: PropTypes.string,
  openFullScreenDialog: PropTypes.func.isRequired,
  closeFullScreenDialog: PropTypes.func.isRequired,
  saveData: PropTypes.func.isRequired,
  // Notifications related
  enqueueSnackbar: PropTypes.func.isRequired,
  closeSnackbar: PropTypes.func.isRequired,
};

Editor.defaultProps = {
  open: false,
  // eslint-disable-next-line no-console
  closeEditorPanel: () => console.error("Dev forgot something...")
};

export default Editor;
