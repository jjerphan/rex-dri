/**
 * Class to handle easily form level errors
 */
export default class FormLevelError {

  /**
   * @param {Array.<string>} fields Array of the field concerned by the error.
   * @param {function(**): boolean} check Function that performs the check of the fields.  The values of the `fields` are passed as parameters and the function is expected to return a boolean.
   * @param {string} message Message for the error in case the check doesn't pass.
   */
  constructor(fields, check, message) {
    this.fields = fields;
    this.check = check;
    this.message = message;
  }

  /**
   * Do we currently have an error ?
   * @param {object} formData data from the form
   * @returns {boolean}
   */
  hasError(formData) {
    return this.check(...this.fields.map(fieldMapping => formData[fieldMapping]));
  }

  /**
   * Get the message associated with the error
   * @returns {string}
   */
  getMessage() {
    return this.message;
  }

  /**
   * Return an array of string corresponding to the field name.
   * @returns {Array<string>}
   */
  getFields() {
    return this.fields;
  }
}
