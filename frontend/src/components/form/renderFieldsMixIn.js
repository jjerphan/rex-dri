/**
 * This file contains extra functions that are directly added the the `Form` class.
 * They are store in another file to be clearer.
 *
 * It basically contains shortcuts to rendering fields.
 *
 * They should be understand as if they were written inside the `Form` class.
 */

import React from "react";
import Typography from "@material-ui/core/Typography";
import getObjModerationLevel from "../../utils/getObjModerationLevels";
import SelectField from "./fields/SelectField";
import UsefulLinksField from "./fields/UsefulLinksField";
import MarkdownField from "./fields/MarkdownField";
import TextField from "./fields/TextField";
import MultiSelectField from "./fields/MultiSelectField";
import NumberField from "./fields/NumberField";

import {getLatestReadDataFromStore} from "../../redux/api/utils";

export default {
  /**
   * For field mixins that handle custom props, we need to make use of getReferenceAndValue too
   *
   * @param {object} props
   * @returns
   */
  customizeProps(props) {
    return Object.assign(props, {...this.getReferenceAndValue(props.fieldMapping)});
  },

  renderObjModerationLevelField() {
    // hack to access directly the store and get the value we need.
    const userData = getLatestReadDataFromStore("userDataOne"),
      possibleObjModeration = getObjModerationLevel(userData.owner_level, true);
    if (possibleObjModeration.length > 1) {
      return (
        <>
          <Typography variant='caption'>Niveau de modération supplémentaire souhaité</Typography>
          <SelectField label={"Niveau de modération pour ce module"}
                       {...this.getReferenceAndValue("obj_moderation_level")}
                       required={true}
                       options={possibleObjModeration}/>
        </>
      );
    } else {
      return (
        <Typography variant='caption'>
          Votre statut ne vous permet pas modifier le niveau local de modération pour ce module.
        </Typography>
      );
    }
  },

  renderImportanceLevelField() {
    const options = [
      {"label": "Normal", "value": "-"},
      {"label": "Important", "value": "+"},
      {"label": "Très important", "value": "++"},
    ];

    return (
      <>
        <Typography variant='caption'>Qualification de l'importance de l'information présentée</Typography>
        <SelectField label={"Niveau d'importance"}
                     {...this.getReferenceAndValue("importance_level")}
                     required={true}
                     options={options}/>
      </>
    );
  },

  renderUsefulLinksField() {
    return (
      <UsefulLinksField label={"Lien(s) utile(s) (ex : vers ces informations)"}
                        {...this.getReferenceAndValue("useful_links")} />
    );
  },

  renderMarkdownField(props) {
    return (
      <MarkdownField
        {...this.customizeProps(props)}
      />
    );
  },

  renderCommentField() {
    return this.renderMarkdownField({
      label: "Commentaire associé à ces informations",
      maxLength: 500,
      fieldMapping: "comment",
    });
  },

  renderTextField(props) {
    return (
      <TextField
        {...this.customizeProps(props)}
      />
    );
  },

  renderTitleField() {

    return this.renderTextField({
      label: "Titre",
      required: true,
      fieldMapping: "title",
    });
  },

  renderUniversitiesField() {
    const {outsideData} = this.props,
      universities = outsideData.universities.map(
        (univ) => {
          return {label: univ.name, value: univ.id, disabled: false};
        }
      );

    return (
      <MultiSelectField label={"Universités concernées"}
                        {...this.getReferenceAndValue("universities")}
                        required={true}
                        options={universities}/>
    );
  },

  renderCountriesField() {
    const {outsideData} = this.props,
      countries = outsideData.countries.map(
        (country) => {
          return {label: country.name, value: country.id, disabled: false};
        }
      );

    return (
      <MultiSelectField label={"Pays concernés"}
                        {...this.getReferenceAndValue("countries")}
                        required={true}
                        options={countries}/>
    );
  },

  renderSelectField(props) {
    return (
      <SelectField
        {...this.customizeProps(props)}
      />
    );
  },

  renderCurrencyField(props) {
    const {outsideData} = this.props;
    const currencies = outsideData.currencies.map(
      c => ({label: c.name ? c.name : c.code, value: c.code, disabled: false})
    );

    return this.renderSelectField({
      ...props,
      options: currencies,
    });
  },

  renderNumberField(props) {
    return (
      <NumberField
        {...this.customizeProps(props)}
      />
    );
  },

};
