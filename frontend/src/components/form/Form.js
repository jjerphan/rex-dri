import {Component} from "react";
import PropTypes from "prop-types";

import isEqual from "lodash/isEqual";
import renderFieldsMixIn from "./renderFieldsMixIn";
import CustomError from "../common/CustomError";

/**
 * Function to build a chached version of the fields mapping to cascade update.
 *
 * @param {Array.<FormLevelError>} formLevelErrors
 * @returns {ReactMapboxGl<string, Set>}
 */
function buildFieldUpdateCache(formLevelErrors) {
  let out = new Map();
  formLevelErrors.map(e => e.getFields())
    .forEach(fieldList =>
      fieldList.forEach(field => {
        if (!out.has(field)) {
          out.set(field, new Set());
        }

        fieldList
          .filter(linkedField => linkedField !== field)
          .forEach(linkedField => {
            out.set(field, out.get(field).add(linkedField));
          });
      }));

  return out;
}


/**
 * React component that should contain `Field` instances.
 *
 * It has a ton of custom behavior implemented in it. `Fields` and `Form`
 * are depending on each other with some custom subscribing behavior.
 *
 * @class Form
 * @extends {React.Component}
 */
class Form extends Component {

  /**
   *
   *
   * handling of the interconnection between the fields and the form
   *
   *
   */


  /**
   * YOU SHOULDN'T OVERRIDE THIS
   *
   * We store it as an object to make sure we don't have issues
   * with resetting it for some reason
   * @type {object.<string, Field>}
   */
  fields = {};

  /**
   * Array containing the possible form level errors
   * @abstract
   * @type {Array.<FormLevelError>}
   */
  formLevelErrors = [];

  /**
   * YOU SHOULDN'T OVERRIDE THIS
   *
   * This method MUST be used on all field inside a `Form` instance.
   *
   * Function that returns the value corresponding to `fieldMapping` and
   * a reference to the Form so that the fields can subscribe.
   *
   * @param {string} fieldMapping
   * @param {function} [convertValue=v => v] Method applied to the value from the modelData to get "the value"
   */
  getReferenceAndValue(fieldMapping, convertValue = v => v) {
    // using react ref was to complicated with ref forwarding not working with withStyles.
    return {value: convertValue(this.props.modelData[fieldMapping]), form: this, fieldMapping};
  }


  /**
   *
   *
   *
   * General Utils
   *
   *
   *
   */

  /**
   * YOU SHOULDN'T OVERRIDE THIS
   *
   * Function to be used in fields so that they subscribe to a form
   *
   * Using ref to field is not working with withStyles ref forwarding issues.
   *
   * @param {string} fieldMapping
   * @param {Field} field
   */
  fieldSubscribe(fieldMapping, field) {
    this.fields[fieldMapping] = field;
  }

  /**
   * YOU SHOULDN'T OVERRIDE THIS
   *
   * Function that returns the fields contained in the form
   * as an array of {fieldMapping: string, field: Field}
   *
   * Works only if the `getReferenceAndValue` was used on the Field props and the field subscribed.
   *
   * @returns {{fieldMapping: string, field: Field}[]}
   */
  getFields() {
    return Object.keys(this.fields)
      .map(fieldMapping =>
        ({fieldMapping, field: this.fields[fieldMapping]})
      );
  }

  /**
   * YOU SHOULDN'T OVERRIDE THIS
   *
   * Function to look if there has been changes compared to the data
   * this is already saved.
   *
   * @returns {Boolean}
   */
  hasChanges() {
    const formData = this.getDataFromFields(),
      modelData = this.props.modelData;

    return Object.keys(formData).some(fieldMapping => {
      const cmp1 = formData[fieldMapping],
        cmp2 = modelData[fieldMapping];

      // we need to compare objects (ie JSON objects) differently
      if (typeof cmp1 === "object") {
        return !isEqual(cmp1, cmp2);
      } else {
        return cmp1 !== cmp2;
      }
    });
  }


  /**
   *
   *
   *
   *
   * Error handling
   *
   *
   *
   */

  /**
   * YOU SHOULDN'T OVERRIDE THIS
   *
   * Returns an object containing with {fieldMapping: valueInField}
   *
   * @returns {object.<string, any>}
   */
  getDataFromFields() {
    return this.getFields()
      .reduce(
        (acc, {field, fieldMapping}) => {
          acc[fieldMapping] = field.getValue();
          return acc;
        },
        Object());
  }

  /**
   * YOU SHOULDN'T OVERRIDE THIS
   *
   * Returns the error object corresponding to the errors **from the form** for a field.
   *
   * @param {string} fieldMapping
   * @returns {CustomError}
   */
  getErrorForField(fieldMapping) {
    const formData = this.getDataFromFields();
    const errorMessages = this.formLevelErrors
      .filter(formLevelError => formLevelError.getFields().includes(fieldMapping))
      .filter(formLevelError => formLevelError.hasError(formData))
      .map(formLevelError => formLevelError.getMessage());
    return new CustomError(errorMessages);
  }


  /**
   * YOU SHOULDN'T OVERRIDE THIS
   *
   * Function to call once a field has been updated, to trigger a rerendering of all the fields
   * linked by a form level error handling.
   * @param {string} fieldMapping
   */
  fieldUpdated(fieldMapping) {
    const fieldsToUpdate = this.getFieldsToUpdate();
    if (fieldsToUpdate.has(fieldMapping)) {
      fieldsToUpdate
        .get(fieldMapping)
        .forEach(field => this.fields[field].forceUpdate());
    }
  }


  /**
   * YOU SHOULDN'T OVERRIDE THIS
   *
   * Helper function to cache the map containin the mapping between a field(name) and the set of fields
   * that should be updated when the previous one is updated.
   * @returns {ReactMapboxGl<string, Set>}
   */
  getFieldsToUpdate() {
    if (!this.fieldsToUpdate) {
      this.fieldsToUpdate = buildFieldUpdateCache(this.formLevelErrors);
    }
    return this.fieldsToUpdate;
  }


  /**
   * YOU SHOULDN'T OVERRIDE THIS
   * Function to build all the errors from the fields of the form.
   *
   * @returns {CustomError}
   */
  getFieldsErrors() {
    return CustomError.superCombine(
      this.getFields()
        .map(({field}) => field.getError())
    );
  }

  /**
   * YOU SHOULDN'T OVERRIDE THIS
   * Function to know if the form has errors either at the field level
   * Or when running checks that combine fields
   *
   * @returns {CustomError}
   */
  getError() {
    return this.getFieldsErrors()
      .combine(this.hasFormLevelErrors());
  }


  /**
   * YOU SHOULDN'T OVERRIDE THIS
   * Function to check for errors in the form
   *
   * @returns {CustomError}
   */
  hasFormLevelErrors() {
    const formData = this.getDataFromFields();
    const errors = this.formLevelErrors
      .filter(formLevelError => formLevelError.hasError(formData))
      .map(formLevelError => formLevelError.getMessage());
    return new CustomError(errors);
  }

}

// Copy all the custom already ready render fields mix in
Object.assign(Form.prototype, renderFieldsMixIn);


Form.propTypes = {
  modelData: PropTypes.object.isRequired,
  outsideData: PropTypes.object,
};

export default Form;
