import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import Form from "../Form";
import FieldWrapper from "./FieldWrapper";
// eslint-disable-next-line no-unused-vars
import CustomError from "../../common/CustomError";

/**
 * Class that handle fields logic
 *
 * @abstract
 * @class Field
 * @extends {PureComponent}
 */
class Field extends PureComponent {

  // Attribute that will be used to replace null value
  defaultNullValue = undefined;

  constructor(props) {
    super(props);

    // make sure to subscribe to the form ! IMPORTANT
    props.form.fieldSubscribe(props.fieldMapping, this);

    this.state = {
      value: props.value,
    };
  }

  /**
   * Sets the new state. Before it's done it replaces the value by null if appropriate.
   *
   * @override
   * @param {object} newState
   */
  setState(newState) {
    let state = Object.assign({}, newState);
    if (typeof this.defaultNullValue !== "undefined") {
      Object.assign(state, {value: newState.value === this.defaultNullValue ? null : newState.value});
    }
    const {form, fieldMapping} = this.props;
    super.setState(newState, () => {
      form.fieldUpdated(fieldMapping);
    });

  }

  /**
   * What is error state of the field.
   *
   * @virtual
   * @returns {CustomError}
   */
  getError() {
    throw Error("This methods has to be override in sub classes");
  }

  /**
   * YOU SHOULDN'T OVERRIDE THIS
   *
   * Return the errors from the field and the errors from the form corresponding to the field.
   * @returns {CustomError}
   */
  getAllErrors() {
    const {form, fieldMapping} = this.props;
    return this.getError().combine(form.getErrorForField(fieldMapping));
  }


  /**
   * function to get serialize the value from the field, to get it ready to send to server
   * You might need to override this for weird formats such as dates.
   *
   * @returns {string|object}
   */
  serializeFromField() {
    return this.state.value;
  }

  /**
   * Returns the serialized value of the field
   *
   * @returns
   */
  getValue() {
    return this.serializeFromField();
  }

  /**
   * Function that should render the field itself
   *
   * MUST BE OVERRIDEN
   *
   * @virtual
   */
  renderField() {
    throw new Error("This method should be override in subclass of Field");
  }

  /**
   * Default react render function
   *
   * @returns
   */
  render() {
    return (
      <FieldWrapper
        required={this.props.required}
        errorObj={this.getAllErrors()}
        label={this.props.label}
      >
        {this.renderField()}
      </FieldWrapper>
    );
  }

}

Field.defaultProps = {
  required: false,
  label: "mon label",
};

Field.propTypes = {
  required: PropTypes.bool.isRequired, // is the field required ?
  label: PropTypes.string, // text to go along the field
  value: PropTypes.isRequired, // value of the field
  form: PropTypes.instanceOf(Form).isRequired, // (reference of to the) form containing the field
  fieldMapping: PropTypes.string.isRequired, // name of the field in the data
};

export default Field;
