
import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";
import compose from "recompose/compose";

import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";

import DeleteIcon from "@material-ui/icons/Delete";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import Field from "./Field";
import Typography from "@material-ui/core/Typography";

import isUrl from "../../../utils/isUrl";
import CustomError from "../../common/CustomError";



/**
 * Form field to render the useFullLinks field that is present in many models.
 *
 * Has to props parameters: `urlMaxLength` and `descriptionMaxLength`
 *
 * @class UsefulLinksField
 * @extends {Field}
 */
class UsefulLinksField extends Field {

  /**
   * @override
   * @returns
   * @memberof UsefulLinksField
   */
  getError() {
    const usefulLinks = this.state.value;
    let messages = Array();

    if (this.props.required && usefulLinks.length == 0) {
      messages.push("Au moins un lien doit être présent");
    }

    const hasEmpty = usefulLinks.some(el =>
      (el.url != "" && el.description == "")
      || (el.url == "" && el.description != "")
    );
    if (hasEmpty) { messages.push("Les deux champs url et description doivent être saisis."); }

    const hasInvalidUrl = usefulLinks.some(el => !isUrl(el.url));
    if (hasInvalidUrl) { messages.push("Un url n'est pas reconnu dans le formulaire."); }

    const hasTooLongUrlOrDesc = usefulLinks.some(el =>
      (el.url && el.url.length > this.props.urlMaxLength)
      || (el.description && el.description.length > this.props.descriptionMaxLength)
    );
    if (hasTooLongUrlOrDesc) { messages.push("Un url ou une description est trop longue."); }

    return new CustomError(messages);
  }

  /**
   * @override
   * @returns
   * @memberof UsefulLinksField
   */
  serializeFromField() {
    const usefulLinks = this.state.value;
    return usefulLinks.filter(el => !(el.url === "" && el.description === ""));
  }

  /**
   * Simple wrapper for cleaner syntax
   *
   * @param {Array[Object]} newUsefulLinks
   * @memberof UsefulLinksField
   */
  updateUsefulLinks(newUsefulLinks) {
    this.setState({
      value: newUsefulLinks,
    });
  }

  /**
   * Checks that bot url and description are set for all used
   * @returns
   * @memberof UsefulLinksField
   */
  areAllUsefulLinksUsed() {
    return this.state.value
      .every(el => el.url != "" && el.description != "");
  }



  /**
   *
   *
   * User interaction handlers
   *
   *
   *
   */


  handleUsefulLinkUrlChange(idx, evt) {
    const newUsefulLinks = this.state.value.map((usefulLink, indexOfChange) => {
      if (idx !== indexOfChange) { return usefulLink; }
      return { ...usefulLink, url: evt.target.value };
    });

    this.updateUsefulLinks(newUsefulLinks);
  }

  handleUsefulLinkDescriptionChange(idx, evt) {
    const newUsefulLinks = this.state.value.map((usefulLink, indexOfChange) => {
      if (idx !== indexOfChange) { return usefulLink; }
      return { ...usefulLink, description: evt.target.value };
    });

    this.updateUsefulLinks(newUsefulLinks);
  }


  handleAddUsefulLink() {
    this.updateUsefulLinks(this.state.value.concat([{ url: "", description: "" }]));
  }

  handleSwapUsefulLink(idx, swapIndexDiff) {
    const { value } = this.state;
    let newUsefulLinks = value.slice();

    const b = value[idx + swapIndexDiff];
    newUsefulLinks[idx + swapIndexDiff] = newUsefulLinks[idx];
    newUsefulLinks[idx] = b;
    this.updateUsefulLinks(newUsefulLinks);
  }

  handleRemoveUsefulLink(idx) {
    this.updateUsefulLinks(this.state.value.filter((s, idxToRemove) => idx !== idxToRemove));
  }


  /**
   *
   *
   * Enf of User interaction handlers
   *
   *
   *
   */

  /**
   * @override
   * @returns
   * @memberof UsefulLinksField
   */
  renderField() {
    const nbOfUsefulLinks = this.state.value.length;
    return (
      <>
        <Typography variant='caption'>Tous les champs doivent être remplis et les URLs doivent commencer par 'http' ou 'https' ou 'ftp'.</Typography>
        <Grid
          container
          spacing={2}
          direction="column"
          justify="flex-start"
          alignItems="flex-start"
        >
          {this.state.value.map((usefulLink, idx) => (
            <Grid item key={idx}>
              <Grid
                container
                spacing={1}
                alignItems="center"
              >

                <Grid item>
                  <TextField
                    placeholder={"URL"}
                    value={usefulLink.url}
                    onChange={(e) => this.handleUsefulLinkUrlChange(idx, e)}
                  />
                </Grid>

                <Grid item>
                  <TextField
                    placeholder={"Description"}
                    value={usefulLink.description}
                    onChange={(e) => this.handleUsefulLinkDescriptionChange(idx, e)}
                  />
                </Grid>

                <Grid item>

                  <IconButton
                    onClick={() => this.handleSwapUsefulLink(idx, -1)}
                    disabled={idx == 0}
                    color="secondary"
                  >
                    <KeyboardArrowUpIcon />
                  </IconButton>
                  <IconButton
                    onClick={() => this.handleSwapUsefulLink(idx, 1)}
                    disabled={idx == nbOfUsefulLinks - 1}
                    color="secondary"
                  >
                    <KeyboardArrowDownIcon />
                  </IconButton>
                  <IconButton
                    onClick={() => this.handleRemoveUsefulLink(idx)}
                    color='secondary'
                  >
                    <DeleteIcon />
                  </IconButton>

                </Grid>
              </Grid>

            </Grid>
          ))}
          <Grid item>
            <Button
              disabled={!this.areAllUsefulLinksUsed()}
              size="small"
              variant="outlined"
              color="secondary"
              onClick={() => this.handleAddUsefulLink()}
            >
              Ajouter un lien
            </Button>
          </Grid>
        </Grid>
      </>
    );
  }
}


UsefulLinksField.defaultProps = {
  value: Array(),
  urlMaxLength: 300,
  descriptionMaxLength: 50,
};

UsefulLinksField.propTypes = {
  value: PropTypes.arrayOf(PropTypes.shape({
    url: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
  })),
  urlMaxLength: PropTypes.number.isRequired,
  descriptionMaxLength: PropTypes.number.isRequired
};


const styles = {
  container: {
    display: "flex",
    flexWrap: "wrap",
  },
  usefulLinkContainer: {
    display: "flex",
    flexWrap: "wrap",
  }
};

export default compose(
  withStyles(styles, { withTheme: true }),
)(UsefulLinksField);
