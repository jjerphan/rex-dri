import React from "react";
import PropTypes from "prop-types";

import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

import Field from "./Field";
import CustomError from "../../common/CustomError";

import withStyles from "@material-ui/core/styles/withStyles";
import DownshiftMultiple from "../../common/DownshiftMultiple";
import uuid from "uuid/v4";


/**
 * Form field for select (not multiple, use the other one for such thing).
 *
 * @class SelectField
 * @extends {Field}
 */
class SelectField extends Field {
  defaultNullValue = uuid();

  /**
   * @override
   * @returns
   */
  getError() {
    const {value} = this.state;
    let messages = Array();

    if (this.props.required && (value === null)) {
      messages.push("Ce champ est requis.");
    }
    return new CustomError(messages);
  }

  handleChangeValue(value) {
    this.setState({value});
  }

  /**
   * @override
   * @returns
   */
  renderField() {
    const valFromState = this.state.value,
      value = valFromState === null ? this.defaultNullValue : valFromState,
      {options} = this.props;

    if (options.length < 10) {
      return (
        <Select
          value={value}
          onChange={(e) => this.handleChangeValue(e.target.value)}
        >
          {
            options.map((el, idx) => (
              <MenuItem key={idx} disabled={false || el.disabled} value={el.value}>{el.label}</MenuItem>
            ))
          }
        </Select>
      );
    } else {
      return (
        <DownshiftMultiple value={[value]}
                           multiple={false}
                           options={options}
                           fieldPlaceholder={this.props.label}
                           onChange={(value) => this.handleChangeValue(value)}/>
      );
    }

  }
}


SelectField.defaultProps = {
  value: null,
};

SelectField.propTypes = {
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  options: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([PropTypes.number.isRequired, PropTypes.string.isRequired]),
    disabled: PropTypes.bool
  })),
};

const styles = theme => ({
  secondary: {
    color: theme.palette.text.secondary,
  }
});

export default withStyles(styles, {withTheme: true})(SelectField);
