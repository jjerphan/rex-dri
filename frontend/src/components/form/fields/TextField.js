import React from "react";
import PropTypes from "prop-types";

import MuiTextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";

import Field from "./Field";
import isUrl from "../../../utils/isUrl";
import stringHasExtension from "../../../utils/stringHasExtension";
import CustomError from "../../common/CustomError";
import truncateString from "../../../utils/truncateString";


/**
 * Form field for a simple textField (with no markdown)
 *
 *
 * @class TextField
 * @extends {Field}
 */
class TextField extends Field {
  defaultNullValue = "";

  /**
   * @override
   * @returns
   * @memberof TextField
   */
  getError() {
    const {value} = this.state;
    let messages = Array();
    if (this.props.required && value == "") {
      messages.push("Ce champ est requis mais il est vide.");
    }
    if (this.props.maxLength && value !== null && value.length > this.props.maxLength) {
      messages.push("L'URL est trop long.");
    }
    if (this.props.isUrl && value != "" && !isUrl(value)) {
      messages.push("L'URL entrer n'est pas reconnu.");
    }
    if (this.props.isUrl && value != "" && this.props.urlExtensions.length > 0) {
      if (!stringHasExtension(value, this.props.urlExtensions)) {
        messages.push("Extension de l'URL non conforme");
      }
    }
    return new CustomError(messages);
  }


  handleChangeValue(val) {
    const {maxLength} = this.props,
      value = truncateString(val, maxLength);

    this.setState({value});
  }


  /**
   * @override
   * @returns
   * @memberof TextField
   */
  renderField() {
    const valFromState = this.state.value,
      value = valFromState === null ? this.defaultNullValue : valFromState;

    return (
      <>
        {
          this.props.isUrl ?
            <>
              <Typography variant='caption'>Un URL est attendu ici (Le protocole - http/https/ftp - est requis
                !)</Typography>
              {
                this.props.urlExtensions.length > 0 ?
                  <Typography variant='caption'>L'url doit terminer par l'une des extensions suivantes (majuscule ou
                    miniscule) : {JSON.stringify(this.props.urlExtensions)}</Typography>
                  :
                  <></>
              }
            </>
            :
            <></>
        }
        {
          this.props.maxLength ?
            <Typography variant='caption'>Nombre de caractères : {value.length}/{this.props.maxLength}</Typography>
            :
            <></>
        }
        <MuiTextField
          placeholder={"Le champ est vide"}
          fullWidth={true}
          multiline={false}
          value={value}
          onChange={(e) => this.handleChangeValue(e.target.value)}
        />
      </>
    );
  }
}


TextField.defaultProps = {
  value: "",
  maxLength: 0,
  isUrl: false,
  urlExtensions: [],
};

TextField.propTypes = {
  value: PropTypes.string,
  maxLength: PropTypes.number,
  isUrl: PropTypes.bool.isRequired,
  urlExtensions: PropTypes.arrayOf(PropTypes.string.isRequired)
};

export default TextField;
