import React from "react";
import PropTypes from "prop-types";

import MenuItem from "@material-ui/core/MenuItem";
import ListItemText from "@material-ui/core/ListItemText";
import Select from "@material-ui/core/Select";
import Checkbox from "@material-ui/core/Checkbox";

import Field from "./Field";
import CustomError from "../../common/CustomError";
import DownshiftMultiple from "../../common/DownshiftMultiple";


/**
 * Form field for multi selects.
 *
 *
 * @class MultiSelectField
 * @extends {Field}
 */
class MultiSelectField extends Field {

  constructor(props) {
    super(props);
    this.optionsByValue = {};
    props.options.map((opt) => this.optionsByValue[opt.value] = opt.label);
  }

  /**
   * @override
   * @returns
   * @memberof MultiSelectField
   */
  getError() {
    const {value} = this.state;
    let messages = Array();
    if (this.props.required && value.length === 0) {
      messages.push("Ce champ est requis.");
    }
    return new CustomError(messages);
  }

  handleChangeValue(value) {
    this.setState({value});
  }

  /**
   * @override
   * @returns
   * @memberof MultiSelectField
   */
  renderField() {
    const {value} = this.state,
      {options} = this.props;

    // for performance considerations
    if (options.length < 10) {
      return (
        <Select
          value={value}
          onChange={(e) => this.handleChangeValue(e.target.value)}
          multiple
          renderValue={selected => selected.map((el) => this.optionsByValue[el]).join(", ")}
        >
          {
            options.map((el) => (
              <MenuItem disabled={false || el.disabled} key={el.value} value={el.value}>
                <Checkbox checked={value.indexOf(el.value) > -1}/>
                <ListItemText primary={el.label}/>
              </MenuItem>
            ))
          }
        </Select>
      );
    } else {
      return (
        <DownshiftMultiple multiple={true}
                           value={value}
                           options={options}
                           fieldPlaceholder={this.props.label}
                           onChange={(selection) => this.handleChangeValue(selection)}/>
      );
    }

  }
}


MultiSelectField.defaultProps = {
  value: Array(),
};

MultiSelectField.propTypes = {
  value: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
  options: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([PropTypes.number.isRequired, PropTypes.string.isRequired]),
    disabled: PropTypes.bool
  })),
};


export default MultiSelectField;
