import React from "react";
import PropTypes from "prop-types";

import Field from "./Field";
import CustomError from "../../common/CustomError";
import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";


/**
 * Form field for a simple boolean switch field
 *
 *
 * @class BooleanField
 * @extends {Field}
 */
class BooleanField extends Field {

  /**
   * @override
   * @returns
   * @memberof BooleanField
   */
  getError() {
    // There are no possible errors, but we need to redefine this function
    return new CustomError([]);
  }


  handleChangeValue(value) {
    this.setState({value});
  }


  /**
   * @override
   * @returns
   * @memberof BooleanField
   */
  renderField() {
    const checked = this.state.value,
      {labelIfTrue, labelIfFalse} = this.props;

    return (
      <FormControlLabel
        control={
          <Switch
            checked={checked}
            onChange={(event) => this.handleChangeValue(event.target.checked)}
            color="primary"
          />
        } label={checked ? labelIfTrue : labelIfFalse}/>

    );
  }
}


BooleanField.defaultProps = {
  value: false,
  labelIfTrue: "",
  labelIfFalse: "",
};

BooleanField.propTypes = {
  value: PropTypes.bool.isRequired,
  labelIfTrue: PropTypes.string,
  labelIfFalse: PropTypes.string,
};

export default BooleanField;
