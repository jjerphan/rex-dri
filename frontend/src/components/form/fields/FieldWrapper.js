import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import withStyles from "@material-ui/core/styles/withStyles";
import compose from "recompose/compose";

import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import CustomError from "../../common/CustomError";


/**
 * Wrapper for fields in forms to handle labels and visual feedback
 *
 * @class FieldWrapper
 * @extends {PureComponent}
 */
class FieldWrapper extends PureComponent {
  render() {
    const { classes, required, errorObj, label, children } = this.props;

    return (
      <FormControl className={classes.formElement} required={required} error={errorObj.status} fullWidth={true}>
        <FormLabel>{label}</FormLabel>
        {
          errorObj.messages.map((error, idx) => (
            <FormHelperText key={idx}>{error}</FormHelperText>
          ))
        }
        {children}
      </FormControl>
    );
  }
}

FieldWrapper.defaultProps = {
  label: "LABEL",
  required: false,
  errorObj: new CustomError(),
};

FieldWrapper.propTypes = {
  label: PropTypes.string.isRequired,
  required: PropTypes.bool.isRequired,
  errorObj: PropTypes.instanceOf(CustomError).isRequired, // Error state of the field
  children: PropTypes.node.isRequired,
  classes: PropTypes.object.isRequired,
};


const styles = theme => ({
  formElement: {
    marginBottom: theme.spacing(3)
  }
});


export default compose(
  withStyles(styles, { withTheme: true })
)(FieldWrapper);
