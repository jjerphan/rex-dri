
import React from "react";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";

import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";

import Markdown from "../../common/markdown/Markdown";
import Field from "./Field";

import TextLink from "../../common/TextLink";
import CustomError from "../../common/CustomError";


/**
 * Form field for markdown elements.
 *
 * A `maxLength` can be set to limit the length of the text.
 *
 * @class MarkdownField
 * @extends {Field}
 */
class MarkdownField extends Field {
  defaultNullValue = "";

  /**
   * One error detection for this field
   *
   * @override
   * @returns {CustomError}
   * @memberof MarkdownField
   */
  getError() {
    const { value } = this.state;
    let messages = Array();
    if (this.props.required && value == "") {
      messages.push("Ce champ est requis mais il est vide.");
    }
    if (this.props.maxLength && value.length > this.props.maxLength) {
      messages.push("Le texte est trop long.");
    }

    return new CustomError(messages);
  }

  /**
   * @memberof MarkdownField
   */
  handleChangeValue = (val) => {
    let value = val;
    const { maxLength } = this.props;

    if (maxLength && value.length > maxLength + 1) {
      value = value.substring(0, maxLength + 1);
    }
    this.setState({ value: value });
  }


  /**
   * @override
   * @returns
   * @memberof MarkdownField
   */
  renderField() {

    const valFromState = this.state.value,
      value = valFromState === null ? this.defaultNullValue : valFromState;

    return (
      <Grid
        container
        spacing={2}
      >
        <Grid item xs={12} md={12} lg={6}>
          <Typography variant='caption'>
            <em>Saisie du texte
                (ce champ supporte en grande partie la syntaxe <TextLink href="https://www.markdownguide.org/basic-syntax">Markdown</TextLink>)
            </em>
          </Typography>
          {
            this.props.maxLength ?
              <Typography variant='caption'>Nombre de caractères : {value.length}/{this.props.maxLength}</Typography>
              :
              <></>
          }
          <TextField
            placeholder={"Le champ est vide"}
            fullWidth={true}
            multiline={true}
            value={value}
            onChange={(e) => this.handleChangeValue(e.target.value)}
          />
        </Grid>


        <Grid item xs={12} md={12} lg={6}>
          <Typography variant='caption'><em>Prévisualisation du texte</em></Typography>
          {
            value == "" ?
              (<Typography variant='caption'>Le champ est vide</Typography>)
              :
              (<Markdown source={value} />)
          }

        </Grid>

      </Grid>
    );
  }
}


MarkdownField.defaultProps = {
  value: "",
  maxLength: 0,
};

MarkdownField.propTypes = {
  value: PropTypes.string,
  maxLength: PropTypes.number
};


export default MarkdownField;
