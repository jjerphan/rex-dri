import React from "react";
import PropTypes from "prop-types";

import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";

import Field from "./Field";
import CustomError from "../../common/CustomError";


/**
 * Form field for numbers.
 * You can specify a `minValue` and a `maxValue` through the props.
 *
 * @class NumberField
 * @extends {Field}
 */
class NumberField extends Field {
  defaultNullValue = "";

  /**
   * @override
   * @returns {CustomError}
   * @memberof NumberField
   */
  getError() {
    const { value } = this.state;
    let messages = Array();
    if (this.props.required && value == "") {
      messages.push("Ce champ est requis mais il est vide.");
    }
    if (this.props.maxValue && value > this.props.maxValue) {
      messages.push("Le nombre inscrit est trop grand.");
    }
    if (this.props.minValue && value < this.props.minValue) {
      messages.push("Le nombre inscrit est trop petit.");
    }
    return new CustomError(messages);
  }

  handleChangeValue(val) {
    const regex = /[^\d.]*/gi,
      value = val.replace(regex, "");
    this.setState({ value });
  }

  /**
   * Render information about the constraints applied to the field.
   *
   * @returns
   * @memberof NumberField
   */
  renderInfo() {
    const { minValue, maxValue } = this.props;
    if (maxValue !== null && minValue !== null) {
      return <Typography variant='caption'>Un nombre compris entre {minValue} et {maxValue} est attendu.</Typography>;
    } else if (minValue !== null) {
      return <Typography variant='caption'>Un nombre supérieur ou égale à {minValue} est attendu.</Typography>;
    } else if (maxValue !== null) {
      return <Typography variant='caption'>Un nombre inférieur ou égale à {maxValue} est attendu.</Typography>;
    } else {
      return <></>;
    }
  }

  /**
   * @override
   * @returns
   * @memberof NumberField
   */
  renderField() {
    const valFromState = this.state.value,
      value = valFromState === null ? this.defaultNullValue : valFromState;

    return (
      <>
        {this.renderInfo()}
        <TextField
          placeholder={"Le champ est vide"}
          fullWidth={true}
          multiline={false}
          value={value}
          onChange={(e) => this.handleChangeValue(e.target.value)}
        />
      </>
    );
  }
}


NumberField.defaultProps = {
  value: "",
  maxValue: null,
  minValue: null,
};

NumberField.propTypes = {
  value: PropTypes.string,
  maxValue: PropTypes.number,
  minValue: PropTypes.number,
};


export default NumberField;
