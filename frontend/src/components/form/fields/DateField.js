import React from "react";
import PropTypes from "prop-types";

import {DatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";

import frLocale from "date-fns/locale/fr";
import format from "date-fns/format";

import KeyboardArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRightIcon from "@material-ui/icons/KeyboardArrowRight";
import dateToDateStr from "../../../utils/dateToDateStr";

import Field from "./Field";
import CustomError from "../../common/CustomError";

/**
 * Class to customize the header of the date selection box
 *
 * @class LocalizedUtils
 * @extends {DateFnsUtils}
 */
class LocalizedUtils extends DateFnsUtils {
  getDatePickerHeaderText(date) {
    return format(date, "d MMM yyyy", {locale: this.locale});
  }
}

/**
 * Form field for dates
 *
 * @class DateField
 * @extends {Field}
 */
class DateField extends Field {

  /**
   * @override
   * @returns
   * @memberof DateField
   */
  serializeFromField() {
    return dateToDateStr(this.state.value);
  }

  /**
   * One error detection for this field
   * @override
   * @returns {CustomError}
   * @memberof MarkdownField
   */
  getError() {
    const date = this.state.value;
    let messages = Array();
    if (this.props.required && !date) {
      messages.push("Date requise !");
    }

    return new CustomError(messages);
  }

  /**
   * @param {Date} date
   * @memberof DateField
   */
  handleDateChange(date) {
    this.setState({
      value: date,
    });
  }

  /**
   * @override
   * @memberof DateField
   */
  renderField() {
    return (
      <MuiPickersUtilsProvider utils={LocalizedUtils} locale={frLocale}>
        <DatePicker
          clearable
          format="d MMM yyyy"
          value={this.state.value}
          onChange={(d) => this.handleDateChange(d)}
          clearLabel="vider"
          cancelLabel="annuler"
          leftArrowIcon={<KeyboardArrowLeftIcon/>}
          rightArrowIcon={<KeyboardArrowRightIcon/>}
        />
      </MuiPickersUtilsProvider>
    );
  }
}

DateField.defaultProps = {
  value: null,
};

DateField.propTypes = {
  value: PropTypes.instanceOf(Date),
};


export default DateField;
