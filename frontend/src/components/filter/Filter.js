import React from "react";
import PropTypes from "prop-types";
import DownshiftMultiple from "../common/DownshiftMultiple";

import CustomComponentForAPI from "../common/CustomComponentForAPI";
import {connect} from "react-redux";
import {saveSelectedUniversities} from "../../redux/actions/filter";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import InfoIcon from "@material-ui/icons/InfoOutlined";

import getActions from "../../redux/api/getActions";
import {compose} from "recompose";
import uuid from "uuid/v4";


/**
 * Implementation of a filter component
 *
 * @class Filter
 * @extends {CustomComponentForAPI}
 * @extends React.Component
 */
class Filter extends CustomComponentForAPI {

  /**
   * Static variables to share behaviors between instances
   */
  static DOWNSHIFT_ID = uuid();
  static isOpened = false;
  static hasSelection = false;

  /**
   * Function to get the list of countries where there are universities.
   *
   * @returns {Array} of the countries instances
   * @memberof Filter
   */
  getCountriesWhereThereAreUniversities() {
    const {mainCampuses, cities, countries} = this.getLatestReadDataFor(["mainCampuses", "cities", "countries"]);

    let citiesMap = new Map(),
      countriesMap = new Map();

    cities.forEach(el => citiesMap.set(el.id, el));
    countries.forEach(el => countriesMap.set(el.id, el));

    let res = new Map();

    mainCampuses.forEach(el => {
      const cityId = el.city,
        city = citiesMap.get(cityId),
        countryId = city.country,
        country = countriesMap.get(countryId);

      res.set(countryId, country);
    });
    return [...res.values()];
  }


  updateSelectedUniversities(selection) {
    const mainCampuses = this.getLatestReadData("mainCampuses");

    let selectedUniversities = [];
    mainCampuses.forEach(campus => {
      const campusFull = this.joinCampus(campus);
      if (selection.includes(campusFull.country.iso_alpha2_code)) {
        selectedUniversities.push(campusFull.university.id);
      }
    });
    this.props.saveSelection(selectedUniversities);
    Filter.hasSelection = selectedUniversities.length !== 0;
    this.forceUpdate();
  }

  customRender() {
    const options = this.getCountriesWhereThereAreUniversities()
      .map(c => ({value: c.iso_alpha2_code, label: c.name}));
    const {classes} = this.props;

    return (
      <ExpansionPanel expanded={Filter.isOpen} onChange={() => {
        Filter.isOpen = !Filter.isOpen;
        this.forceUpdate();
      }}>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
          <Typography className={classes.heading}>Appliquer des filtres</Typography>
          <div className={classes.infoFilter}>
            <InfoIcon color={Filter.hasSelection ? "primary" : "disabled"}/>
            <Typography className={classes.caption} color={Filter.hasSelection ? "primary" : "textSecondary"}>
              <em>{Filter.hasSelection ? "(Un filtre est actif)" : "(Aucun filtre est actif)"}</em>
            </Typography>
          </div>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <DownshiftMultiple
            fieldPlaceholder={"Filter par pays"}
            options={options}
            onChange={(selection) => this.updateSelectedUniversities(selection)}
            cacheId={Filter.DOWNSHIFT_ID}
          />
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}

Filter.propTypes = {
  classes: PropTypes.object.isRequired,
  saveSelection: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  return {
    universities: state.api.universitiesAll,
    mainCampuses: state.api.mainCampusesAll,
    cities: state.api.citiesAll,
    countries: state.api.countriesAll,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    api: {
      universities: () => dispatch(getActions("universities").readAll()),
      mainCampuses: () => dispatch(getActions("mainCampuses").readAll()),
      cities: () => dispatch(getActions("cities").readAll()),
      countries: () => dispatch(getActions("countries").readAll())
    },
    saveSelection: (selectedUniversities) => dispatch(saveSelectedUniversities(selectedUniversities)),
  };
};


const styles = theme => ({
  root: {
    width: "100%",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightMedium,
  },
  infoFilter: {
    marginLeft: theme.spacing(2),
    display: "inherit"
  }
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withStyles(styles)
)(Filter);
