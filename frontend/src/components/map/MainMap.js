import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import getActions from "../../redux/api/getActions";

import CustomComponentForAPI from "../common/CustomComponentForAPI";
import arrayOfInstancesToMap from "../../utils/arrayOfInstancesToMap";
import BaseMap from "./BaseMap";

import uuid from "uuid/v4";
import "./map.scss";

/**
 * Main map of the application (map tab)
 */
class MainMap extends CustomComponentForAPI {
  static id = uuid();

  customRender() {
    const {
      universities,
      mainCampuses,
      countries,
      cities
    } = this.getAllLatestReadData();


    // some conversions for optimization (faster search of element in a map)
    const universitiesMap = arrayOfInstancesToMap(universities),
      countriesMap = arrayOfInstancesToMap(countries),
      citiesMap = arrayOfInstancesToMap(cities);

    let mainCampusesSelection = [];
    const listUnivsel = this.props.selectedUniversities;

    //console.log(this.props.selectedUniversities);
    // Merge the data and add it to the selection

    mainCampuses.forEach(campus => {
      const univ = universitiesMap.get(campus.university);
      if ((listUnivsel.length > 0 && listUnivsel.indexOf(univ.id) > -1) || listUnivsel.length === 0) {

        if (campus && univ) {
          const city = citiesMap.get(campus.city),
            country = countriesMap.get(city.country);

          mainCampusesSelection.push({
            univName: univ.name,
            univLogoUrl: univ.logo,
            cityName: city.name,
            countryName: country.name,
            lat: parseFloat(campus.lat),
            lon: parseFloat(campus.lon),
            univId: univ.id
          });

        }
      }
    });

    // create all the markers
    return (
      <BaseMap id={MainMap.id} campuses={mainCampusesSelection}/>
    );
  }
}

MainMap.propTypes = {
  universities: PropTypes.object.isRequired,
  mainCampuses: PropTypes.object.isRequired,
  cities: PropTypes.object.isRequired,
  countries: PropTypes.object.isRequired,
  selectedUniversities: PropTypes.array.isRequired
};

const mapStateToProps = (state) => {
  return {
    universities: state.api.universitiesAll,
    mainCampuses: state.api.mainCampusesAll,
    cities: state.api.citiesAll,
    countries: state.api.countriesAll,
    selectedUniversities: state.app.selectedUniversities
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    api: {
      universities: () => dispatch(getActions("universities").readAll()),
      mainCampuses: () => dispatch(getActions("mainCampuses").readAll()),
      cities: () => dispatch(getActions("cities").readAll()),
      countries: () => dispatch(getActions("countries").readAll())
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainMap);
