import React, {Component} from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconAdd from "@material-ui/icons/Add";
import IconClose from "@material-ui/icons/Close";
import {Link} from "react-router-dom";

import MyCardMedia from "./MyCardMedia";
import {APP_ROUTES} from "../../config/appRoutes";


/**
 * Custom component to create the overlayed popup on the map with university info
 *
 * @class UnivMapPopup
 * @extends {Component}
 */
class UnivMapPopup extends Component {

  render() {
    const {classes, univLogoUrl, univName, cityName, countryName, univId, handleClose} = this.props,
      height = "50";// this.props.univLogoUrl !== "" ? "140" : "0";

    return (
      <Card className={classes.card}>
        <CardActionArea>
          <div style={{padding: "10px", minHeight: "70px"}}>
            <MyCardMedia className={classes.media}
                         height={height}
                         url={univLogoUrl}
                         title={univName} />
          </div>
          <Divider/>
          <CardContent>
            <Typography variant="subtitle1">
              {cityName}, {countryName}
            </Typography>
            <Divider/>
            <Typography gutterBottom variant="h5">
              {univName}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Link to={APP_ROUTES.forUniversity(univId)} className={classes.moreInfoLink}>
            <Button variant="contained" size="small" color="primary">
              < IconAdd/>
              En savoir plus
            </Button>
          </Link>
          <Button size="small" color="secondary" onClick={() => handleClose()}>
            < IconClose/>
            Fermer
          </Button>
        </CardActions>
      </Card>
    );
  }
}

UnivMapPopup.propTypes = {
  classes: PropTypes.object.isRequired,
  univLogoUrl: PropTypes.string.isRequired,
  univName: PropTypes.string.isRequired,
  cityName: PropTypes.string.isRequired,
  countryName: PropTypes.string.isRequired,
  univId: PropTypes.number.isRequired,
  handleClose: PropTypes.func.isRequired,
};

const styles = {
  card: {
    maxWidth: 345,
  },
  media: {
    objectFit: "cover",
  },
  hide: {
    display: "none",
  },
  moreInfoLink: {
    textDecoration: "none"
  }
};

export default withStyles(styles)(UnivMapPopup);
