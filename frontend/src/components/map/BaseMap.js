import React, {Component} from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import {compose} from "recompose";
import PropTypes from "prop-types";

import ReactMapboxGl, {Feature, Layer, Popup} from "react-mapbox-gl";
import UnivMapPopup from "./UnivMapPopup";


const Map = ReactMapboxGl({
  dragRotate: false,
  pitchWithRotate: false,
  maxZoom: 12,
});


/**
 * Custom react-wrapper on top of MapBoxGlJs to handle our maps in a generic manner.
 *
 * If an id is provided, the state of the map will be automatically saved and regenerated.
 */
class BaseMap extends Component {
  // Static variable to hold the map center in a generic way without redux
  static allMaps = {};

  // campusesMarkers = [];


  /**
   * @static
   * @private
   * @constant
   * @type {{center: number[], zoom: number[]}}
   */
  static DEFAULTS = {
    center: [53.94, 41.13],
    zoom: [0.86]
  };

  state = {
    popup: undefined
  };


  saveStatus(map) {
    if (typeof this.props.id !== "undefined") {
      const center = map.getCenter();
      BaseMap.allMaps[this.props.id] = {
        center: [center.lng, center.lat],
        zoom: [map.getZoom()],
      };
    }
  }

  openPopup(campusInfo) {
    this.setState({popup: campusInfo});
  }

  closePopup() {
    this.setState({popup: undefined});
  }

  toggleHover(map, cursor) {
    map.getCanvas().style.cursor = cursor;
  }

  render() {
    const {theme} = this.props,
      style = this.props.theme.palette.type === "light" ?
        "light"
        :
        "dark";

    let mapStatus = Object.assign({}, BaseMap.DEFAULTS);

    if (typeof this.props.id !== "undefined") {
      const previousData = BaseMap.allMaps[this.props.id];
      if (typeof previousData !== "undefined") {
        Object.assign(mapStatus, previousData);
      }
    }

    const {popup} = this.state,
      {campuses} = this.props;

    return (
      <Map style={`/map-server/styles/${style}/style.json`}
           containerStyle={{
             height: "60vh",
             width: "100%"
           }}
           zoom={mapStatus.zoom}
           center={mapStatus.center}
           onMoveEnd={(map) => this.saveStatus(map)}
      >
        {
          campuses ?
            <Layer
              type="circle"
              id="campuses"
              paint={{"circle-color": theme.palette.primary.main, "circle-opacity": 0.8, "circle-radius": 8}}>

              {
                campuses.map(
                  (campusInfo, key) =>
                    <Feature key={key}
                             coordinates={[campusInfo.lon, campusInfo.lat]}
                             onClick={() => this.openPopup(campusInfo)}
                             onMouseEnter={({map}) => this.toggleHover(map, "pointer")}
                             onMouseLeave={({map}) => this.toggleHover(map, "")}
                    />)
              }
            </Layer>
            :
            <></>
        }
        {popup && (
          <Popup key={popup.univId} coordinates={[popup.lon, popup.lat]}>
            <UnivMapPopup {...popup} handleClose={() => this.closePopup()}/>
          </Popup>
        )}
      </Map>
    );
  }
}


BaseMap.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  id: PropTypes.string,
  campuses: PropTypes.array,
};

// eslint-disable-next-line no-unused-vars
const styles = theme => ({});

export default compose(
  withStyles(styles, {withTheme: true}),
)(BaseMap);
