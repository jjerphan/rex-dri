import React, { Component } from "react";
import PropTypes from "prop-types";
import CardMedia from "@material-ui/core/CardMedia";

/**
 * Custom MUI Card Media Component that renders a single div if no URL is provided
 *
 * @class MyCardMedia
 * @extends {Component}
 */
class MyCardMedia extends Component {
  render() {
    const { title, height, url } = this.props;
    if (url == "") {
      return (<div />);
    }

    return (
      <CardMedia
        component="img"
        height={height}
        image={url}
        title={title}
        style={{objectFit: "contain"}}
      />
    );
  }
}

MyCardMedia.propTypes = {
  title: PropTypes.string.isRequired,
  height: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

export default MyCardMedia;
