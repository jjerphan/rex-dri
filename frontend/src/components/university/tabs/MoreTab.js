import React, { Component } from "react";

import ModuleGroupWrappers from "../modules/common/ModuleGroupWrapper";


/**
 * Tab on the university page containing "more" information
 *
 * @class MoreTab
 * @extends {Component}
 */
class MoreTab extends Component {

  render() {
    return (
      <>
        <ModuleGroupWrappers groupTitle={"Administratif"} />
        <ModuleGroupWrappers groupTitle={"Assurances"} />
        <ModuleGroupWrappers groupTitle={"Transport"} />
        <ModuleGroupWrappers groupTitle={"Culture"} />
        <ModuleGroupWrappers groupTitle={"Tourisme"} />
        <ModuleGroupWrappers groupTitle={"Photos"} />
        <ModuleGroupWrappers groupTitle={"Autres"} />
      </>
    );
  }
}

export default MoreTab;
