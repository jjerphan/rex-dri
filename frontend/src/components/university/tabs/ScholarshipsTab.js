import React, {Component} from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import compose from "recompose/compose";

import Grid from "@material-ui/core/Grid";

import UniversityScholarships from "../modules/UniversityScholarships";
import CountryScholarships from "../modules/CountryScholarships";


/**
 * Tab on the university page containing information related to scholarship
 *
 * @class ScholarshipsTab
 * @extends {Component}
 */
class ScholarshipsTab extends Component {

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container spacing={2} >

          <Grid item xs={12}>
            <UniversityScholarships/>
          </Grid>
          <Grid item xs={12}>
            <CountryScholarships/>
          </Grid>

        </Grid>

      </div>
    );
  }
}

ScholarshipsTab.propTypes = {
  classes: PropTypes.object.isRequired,
};

const styles = {
  root: {
    display: "flex",
    width: "90%",
    marginLeft: "auto",
    marginRight: "auto"
  },
};

export default compose(
  withStyles(styles),
)(ScholarshipsTab);
