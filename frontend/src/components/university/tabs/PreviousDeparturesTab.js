import React from "react";

import PreviousDeparture from "../modules/previousExchangeFeedback/PreviousDeparture";
import CustomComponentForAPI from "../../common/CustomComponentForAPI";
import PropTypes from "prop-types";
import getActions from "../../../redux/api/getActions";
import compose from "recompose/compose";
import {connect} from "react-redux";
import {withUnivInfo} from "../common/withUnivInfo";
import {RequestParams} from "../../../redux/api/RequestParams";


/**
 * Tab on the university page containing information related to previous departure
 *
 * @class PreviousDeparturesTab
 * @extends {Component}
 */
class PreviousDeparturesTab extends CustomComponentForAPI {
  apiParams = {
    exchanges: ({props}) => RequestParams.Builder.withQueryParam("university", props.univId).build(),
  };

  customRender() {
    return (
      <>
        {
          this.getLatestReadData("exchanges").map((rawModelData, idx) => (
            <PreviousDeparture key={idx} rawModelData={rawModelData}/>
          ))
        }
      </>
    );
  }

}

PreviousDeparturesTab.propTypes = {
  univId: PropTypes.string.isRequired,
  exchanges: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  return {
    exchanges: state.api.exchangeFeedbacksAll,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    api: {
      exchanges: (params) => dispatch(getActions("exchangeFeedbacks").readAll(params)),
    },
  };
};


export default compose(
  withUnivInfo(),
  connect(mapStateToProps, mapDispatchToProps),
)(PreviousDeparturesTab);

