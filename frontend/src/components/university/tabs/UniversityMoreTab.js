import React, { Component } from "react";
import ModuleGroupWrappers from "../modules/common/ModuleGroupWrapper";



/**
 * Tab containing specific comments about the university
 *
 * @class UniversityMoreTab
 * @extends {Component}
 */
class UniversityMoreTab extends Component {
  render() {
    return (
      <>
        <ModuleGroupWrappers groupTitle={"Partenariats spécifiques"} />
        <ModuleGroupWrappers groupTitle={"Commentaires partagés"} />
        <ModuleGroupWrappers groupTitle={"Autres"} />
      </>
    );
  }
}


export default UniversityMoreTab;
