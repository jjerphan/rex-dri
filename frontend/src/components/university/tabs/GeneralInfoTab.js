import React from "react";

import UniversityGeneral from "../modules/UniversityGeneral";
import UniversitySemestersDates from "../modules/UniversitySemestersDates";
import UniversityDri from "../modules/UniversityDri";
import CountryDri from "../modules/CountryDri";
import {makeStyles} from "@material-ui/styles";


const useStyles = makeStyles(theme => ({
  wideRoot: {
    display: "flex",
    width: "100%",
    marginLeft: "auto",
    marginRight: "auto",
    [theme.breakpoints.down("sm")]: {
      display: "none",
    }
  },
  wideLeftCol: {
    flexGrow: 8,
    marginRight: theme.spacing(2),
  },
  wideRightCol: {
    flexGrow: 6,
  },
  wideColItem: {
    marginBottom: theme.spacing(2),
  },
  smallRoot: {
    width: "100%",
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
  smallColItem: {
    marginBottom: theme.spacing(2),
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  spacer: {
    marginBottom: theme.spacing(2),
  },
}));

/**
 * Main tab of the university page
 */
function GeneralInfoTab() {
  const classes = useStyles();
  // We render both full screen and small screen layout to prevent components from unmounting.
  // And causing trouble elsewhere :)
  // the hiding is done with display none.
  return (
    <>
      <div className={classes.wideRoot}>
        <div className={classes.wideLeftCol}>
          {
            [UniversityGeneral, UniversityDri, CountryDri].map((Comp, key) =>
              <div key={key} className={classes.wideColItem}>
                <Comp/>
              </div>
            )
          }
        </div>
        <div className={classes.wideRightCol}>
          {
            [UniversitySemestersDates].map((Comp, key) =>
              <div key={key} className={classes.wideColItem}>
                <Comp/>
              </div>
            )
          }
        </div>
      </div>
      <div className={classes.smallRoot}>
        <div className={classes.spacer}/>
        {
          [UniversityGeneral, UniversityDri, CountryDri, UniversitySemestersDates].map((Comp, key) =>
            <div key={key} className={classes.smallColItem}>
              <Comp/>
            </div>
          )
        }
      </div>
    </>
  );
}

GeneralInfoTab.propTypes = {};


export default GeneralInfoTab;
