import React, { Component } from "react";
import ModuleGroupWrappers from "../modules/common/ModuleGroupWrapper";

/**
 * Tab on the university page related to Campuses and cities
 *
 * @class CampusesCitiesTab
 * @extends {Component}
 */
class CampusesCitiesTab extends Component {
  render() {
    return (
      <>
        <ModuleGroupWrappers groupTitle={"Logement"} />
        <ModuleGroupWrappers groupTitle={"Vie étudiante"} />
        <ModuleGroupWrappers groupTitle={"Vie quotidienne (et transport)"} />
        <ModuleGroupWrappers groupTitle={"Autre chose ?"} />
      </>
    );
  }
}

export default CampusesCitiesTab;
