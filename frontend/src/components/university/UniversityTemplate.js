import React, {Component} from "react";
import PropTypes from "prop-types";
import withWidth, {isWidthUp} from "@material-ui/core/withWidth";
import compose from "recompose/compose";

import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import StarsIcon from "@material-ui/icons/Stars";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import GpsFixedIcon from "@material-ui/icons/GpsFixed";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";
import HistoryIcon from "@material-ui/icons/History";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";

import CoverGallery from "./otherComponents/CoverGallery";

import GeneralInfoTab from "./tabs/GeneralInfoTab";
// import UniversityMoreTab from "./tabs/UniversityMoreTab";
// import CampusesCitiesTab from "./tabs/CampusesCitiesTab";
import PreviousDeparturesTab from "./tabs/PreviousDeparturesTab";
import ScholarshipsTab from "./tabs/ScholarshipsTab";
import {withRouter} from "react-router-dom";
import {PaddedPageDiv} from "../pages/shared";
import {withStyles} from "@material-ui/styles";
import {appBarHeight, siteMaxWidth} from "../../config/sharedStyles";

// import MoreTab from "./tabs/MoreTab";


/**
 * Component that handles all the elements of the university page
 *
 * @class UniversityTemplate
 * @extends {React.Component}
 */
class UniversityTemplate extends Component {
  state = {
    value: "general",
  };

  componentDidMount() {
    // Make sure to virtually redirect to "general" if no tab is previously selected.
    const {tabName} = this.props.match.params;

    if (tabName === "" || typeof tabName === "undefined") {
      this.handleChange(undefined, "general");
    } else {
      this.handleChange(undefined, tabName);
    }
  }

  handleChange(event, value) {
    this.props.history.push(value);
    this.setState({value});
  }

  render() {
    const {width, classes} = this.props,
      {value} = this.state;

    let scroll = true;
    if (isWidthUp("lg", width)) {
      scroll = false;
    }

    return (
      <>
        <CoverGallery/>
        <AppBar position="sticky" color="default" className={classes.tabBar}>
          <Tabs
            value={value}
            onChange={(event, value) => this.handleChange(event, value)}
            variant={scroll ? "scrollable" : "standard"}
            centered={!scroll}
            scrollButtons="on"
            indicatorColor="secondary"
            textColor="secondary"
          >
            <Tab label="Généralités" value="general" icon={<StarsIcon/>}/>
            <Tab label="Université+" value="more" icon={<GpsFixedIcon/>}/>
            <Tab label="Précédents départs" value="previous-exchanges" icon={<HistoryIcon/>}/>
            <Tab label="Bourses" value="scholarships" icon={<AttachMoneyIcon/>}/>
            <Tab label="Campus & ville(s)" value="campus-and-city" icon={<LocationOnIcon/>}/>
            <Tab label="Autres" value="other" icon={<MoreHorizIcon/>}/>
          </Tabs>
        </AppBar>
        <PaddedPageDiv>
          {value === "general" ? <GeneralInfoTab/> : <></>}
          {value === "more" ? <></> : <></>}
          {value === "previous-exchanges" ? <PreviousDeparturesTab/> : <></>}
          {value === "scholarships" ? <ScholarshipsTab/> : <></>}
          {value === "campus-and-city" ? <></> : <></>}
          {value === "other" ? <></> : <></>}
        </PaddedPageDiv>
      </>
    );
  }
}

UniversityTemplate.propTypes = {
  width: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
  match: PropTypes.shape({ // given by withRouter
    params: PropTypes.shape({
      univId: PropTypes.string.isRequired,
      tabName: PropTypes.string,
    }).isRequired,
  }).isRequired,
  history: PropTypes.object.isRequired, // given by withRouter
};


const styles = theme => ({
  tabBar: {
    minHeight: theme.spacing(9),
    [`@media (min-width:${siteMaxWidth()}px)`]:{
      top: appBarHeight(theme)
    }
  },
});

export default compose(
  withStyles(styles),
  withWidth({noSSR: true}),
  withRouter
)(UniversityTemplate);
