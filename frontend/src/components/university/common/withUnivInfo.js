import UnivContext from "./UnivContext";
import React from "react";
import {setDisplayName} from "recompose";

/**
 * HOC (higher order component) wrapper to provide information to components.
 *
 *
 * The component being wrapped must me in the same subtree as the UnivInfoProvider childrens
 *
 * @param {Array.<string>} propertiesToAdd List of the properties to add from UnivInfoProvider
 * @returns {function(*): function(*): *}
 */
export function withUnivInfo(propertiesToAdd = []) {

  function getPropertiesToAdd(infos) {
    let out = {};
    propertiesToAdd.forEach(p => out[p] = infos[p]);
    return out;
  }

  return Component => setDisplayName("UnivInfoConsumer")(
    // We need to forward the ref to make sure the styles are correctly applied.
    // eslint-disable-next-line react/display-name
    React.forwardRef((props, ref) => (
      <UnivContext.Consumer>
        {
          infos => (
            <Component {...props} univId={infos.univId} {...getPropertiesToAdd(infos)} ref={ref}/>
          )
        }
      </UnivContext.Consumer>
    )));
}

