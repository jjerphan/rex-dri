import React from "react";
import PropTypes from "prop-types";
import compose from "recompose/compose";
import { connect } from "react-redux";
import UnivContext from "../common/UnivContext";

import CustomComponentForAPI from "../../common/CustomComponentForAPI";

import getActions from "../../../redux/api/getActions";



/**
 * Univ info provider to pre-fetch some data and make it available down the tree very easily
 *
 * @class UnivInfoProvider
 * @extends {CustomComponentForAPI}
 * @extends React.Component
 */
class UnivInfoProvider extends CustomComponentForAPI {
  customRender() {
    const { univId } = this.props,
      { city, country } = this.getUnivCityAndCountry(univId),
      { universities, countries, currencies } = this.getLatestReadDataFor(["universities", "countries", "currencies"]),
      countryId = country.id,
      cityId = city.id;

    return (
      <UnivContext.Provider value={{ univId, countryId, cityId, city, country, universities, countries, currencies }}>
        {this.props.children}
      </UnivContext.Provider>
    );
  }
}

UnivInfoProvider.propTypes = {
  univId: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

const mapStateToProps = (state) => {
  return {
    countries: state.api.countriesAll,
    cities: state.api.citiesAll,
    currencies: state.api.currenciesAll,
    mainCampuses: state.api.mainCampusesAll,
    universities: state.api.universitiesAll,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    api: {
      universities: () => dispatch(getActions("universities").readAll()),
      mainCampuses: () => dispatch(getActions("mainCampuses").readAll()),
      cities: () => dispatch(getActions("cities").readAll()),
      countries: () => dispatch(getActions("countries").readAll()),
      currencies: () => dispatch(getActions("currencies").readAll()),
    },
  };
};


export default compose(
  connect(mapStateToProps, mapDispatchToProps)
)(UnivInfoProvider);
