import React, { Component } from "react";
import AwesomeSlider from "react-awesome-slider";
import styles from "./CoverGallery.scss";

/**
 * Component to display a cover image gallery with custom styling done in the ./CoverGallery.scss file
 *
 * @class CoverGallery
 * @extends {React.Component}
 */
class CoverGallery extends Component {
  render() {
    return (
      <AwesomeSlider cssModule={styles} >
        <div data-src="https://www.epflalumni.ch/wp-content/uploads/2015/02/Centresportif-770x399.jpg" />
        <div data-src="https://www.stcc.ch/wp-content/uploads/2017/11/epfl.jpg" />
      </AwesomeSlider>
    );
  }
}

export default CoverGallery;
