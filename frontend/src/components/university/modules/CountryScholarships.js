import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import compose from "recompose/compose";
import {connect} from "react-redux";


import Module from "./common/Module";

import ModuleWrapper from "./common/ModuleWrapper";
import ModuleGroupWrapper from "./common/ModuleGroupWrapper";

import Scholarship from "./common/Scholarship";

import CountryScholarshipEditor from "../editors/CountryScholarshipEditor";

import getActions from "../../../redux/api/getActions";
import {withUnivInfo} from "../common/withUnivInfo";
import {RequestParams} from "../../../redux/api/RequestParams";


// eslint-disable-next-line no-unused-vars
const styles = theme => ({});

function renderCore(rawModelData, classes, outsideData) {
  const {comment, frequency, currency, short_description} = rawModelData;
  const amountMin = rawModelData.amount_min,
    amountMax = rawModelData.amount_max,
    otherAdvantages = rawModelData.other_advantages;

  return (
    <Scholarship
      currency={currency}
      currencies={outsideData.currencies}
      frequency={frequency}
      shortDescription={short_description}
      amountMin={amountMin}
      amountMax={amountMax}
      otherAdvantages={otherAdvantages}
      comment={comment}
    />
  );
}


class CountryScholarships extends Module {
  apiParams = {
    countryScholarships: ({props}) => RequestParams.Builder.withQueryParam("countries", props.countryId).build(),
  };

  customRender() {
    const countryScholarshipsItems = this.getOnlyReadData("countryScholarships"),
      {countries, currencies, classes} = this.props,
      outsideData = {
        countries,
        currencies
      };

    return (
      <ModuleGroupWrapper
        groupTitle={`Bourses liées au pays (${this.props.country.name})`}
        endPoint={"countryScholarship"}
        editor={CountryScholarshipEditor}
        invalidateGroup={this.props.invalidateData}
        defaultModelData={{
          countries: [this.props.countryId],
          importance_level: "-",
          currency: "EUR",
          obj_moderation_level: 0
        }}
        propsForEditor={{
          outsideData: outsideData,
        }}
      >
        {
          countryScholarshipsItems.map((rawModelData, idx) => (
            <ModuleWrapper
              key={idx}
              buildTitle={(modelData) => modelData.title}
              rawModelData={rawModelData}
              editor={CountryScholarshipEditor}
              renderCore={renderCore}
              coreClasses={classes}
              outsideData={outsideData}
              moduleInGroupInfos={{isInGroup: true, invalidateGroup: () => this.props.invalidateData()}}
            />
          ))
        }
      </ModuleGroupWrapper>

    );
  }
}

CountryScholarships.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  countryId: PropTypes.string.isRequired,
  country: PropTypes.object.isRequired,
  countries: PropTypes.array.isRequired,
  currencies: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => {
  return {
    countryScholarships: state.api.countryScholarshipsAll,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    api: {
      countryScholarships: (params) => dispatch(getActions("countryScholarships").readAll(params)),
    },
    invalidateData: () => dispatch(getActions("countryScholarships").invalidateAll())
  };
};


export default compose(
  withUnivInfo(["countries", "currencies", "countryId", "country"]),
  withStyles(styles, {withTheme: true}),
  connect(mapStateToProps, mapDispatchToProps)
)(CountryScholarships);
