import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import compose from "recompose/compose";
import { connect } from "react-redux";

import Markdown from "../../common/markdown/Markdown";

import Module from "./common/Module";

import ModuleWrapper from "./common/ModuleWrapper";
import ModuleGroupWrapper from "./common/ModuleGroupWrapper";

import CountryDriEditor from "../editors/CountryDriEditor";
import getActions from "../../../redux/api/getActions";
import {withUnivInfo} from "../common/withUnivInfo";
import {RequestParams} from "../../../redux/api/RequestParams";


// eslint-disable-next-line no-unused-vars
const styles = theme => ({
});

// eslint-disable-next-line no-unused-vars
function renderCore(rawModelData, classes, outsideData) {
  const { comment } = rawModelData;
  return (
    <Markdown source={comment} />
  );
}

class CountryDri extends Module {
  apiParams = {
    countryDri: ({props}) => RequestParams.Builder.withQueryParam("countries", props.countryId).build(),
  };

  customRender() {
    const countryDriItems = this.getOnlyReadData("countryDri"),
      { countries, classes } = this.props,
      outsideData = { countries };

    return (
      <ModuleGroupWrapper
        groupTitle={"Informations émanant de la DRI liées au pays"}
        endPoint={"countryDri"}
        editor={CountryDriEditor}
        invalidateGroup={this.props.invalidateData}
        defaultModelData={{ countries: [this.props.countryId], importance_level: "-" }}
        propsForEditor={{
          outsideData: outsideData,
        }}
      >
        {
          countryDriItems.map((rawModelData, idx) => (
            <ModuleWrapper
              key={idx}
              buildTitle={(modelData) => modelData.title}
              rawModelData={rawModelData}
              editor={CountryDriEditor}
              renderCore={renderCore}
              coreClasses={classes}
              outsideData={outsideData}
              moduleInGroupInfos={{ isInGroup: true, invalidateGroup: () => this.props.invalidateData() }}
            />
          ))
        }
      </ModuleGroupWrapper >

    );
  }
}

CountryDri.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  countryId: PropTypes.string.isRequired,
  countries: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => {
  return {
    countryDri: state.api.countryDriAll,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    api: {
      countryDri: (params) => dispatch(getActions("countryDri").readAll(params)),
    },
    invalidateData: () => dispatch(getActions("countryDri").invalidateAll())
  };
};


export default compose(
  withUnivInfo(["countries", "countryId"]),
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(CountryDri);
