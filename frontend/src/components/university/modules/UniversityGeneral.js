import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import compose from "recompose/compose";
import { connect } from "react-redux";

import Typography from "@material-ui/core/Typography";

import ModuleWrapper from "./common/ModuleWrapper";
import TextLink from "../../common/TextLink";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import PhotoSizeSelectActualIcon from "@material-ui/icons/PhotoSizeSelectActual";

import Module from "./common/Module";

import UniversityGeneralEditor from "../editors/UniversityGeneralEditor";

import getActions from "../../../redux/api/getActions";
import {withUnivInfo} from "../common/withUnivInfo";
import {RequestParams} from "../../../redux/api/RequestParams";

// eslint-disable-next-line no-unused-vars
const styles = theme => ({
});

function renderCore(rawModelData, classes, outsideData) {
  const univInfos = rawModelData;
  const { name, acronym, logo, website } = univInfos;
  const { city, country } = outsideData;
  return (
    <Grid container spacing={2} direction='row'>
      <Grid item xs={4} style={{ display: "inline-flex", alignItems: "center" }}>
        {
          logo != "" ?
            <img style={{ width: "100%" }} src={logo} />
            :
            <PhotoSizeSelectActualIcon style={{ marginLeft: "auto", marginRight: "auto", width: "60%", height: "60%" }} />
        }

      </Grid>
      <Grid item xs>
        <Typography variant='h5'>{name}</Typography>
        <Typography variant='h6'>{acronym}</Typography>
        <Divider />
        <Typography variant='subtitle1'>{city}, {country}</Typography>
        <Typography variant='body2'> Site internet&nbsp;:{" "}
          {
            website != "" ?
              <TextLink href={website}>{website}</TextLink>
              :
              <em>Non connu.</em>
          }
        </Typography>
      </Grid>
    </Grid>
  );
}


class UniversityGeneral extends Module {
  apiParams = {
    university: ({props}) => RequestParams.Builder.withId(props.univId).build(),
  };

  customRender() {
    const univInfos = this.getLatestReadData("university");
    const { classes } = this.props;
    const outsideData = {
      city: this.props.city.name,
      country: this.props.country.name
    };

    return (
      <ModuleWrapper
        buildTitle={() => "Présentation"}
        rawModelData={univInfos}
        editor={UniversityGeneralEditor}
        renderCore={renderCore}
        coreClasses={classes}
        outsideData={outsideData}
      />
    );
  }
}

UniversityGeneral.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  univId: PropTypes.string.isRequired,
  city: PropTypes.object.isRequired,
  country: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  return {
    university: state.api.universitiesOne,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    api: {
      university: (params) => dispatch(getActions("universities").readOne(params)),
    },
    invalidateData: () => dispatch(getActions("universities").invalidateOne())
  };
};


export default compose(
  withUnivInfo(["city", "country"]),
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(UniversityGeneral);
