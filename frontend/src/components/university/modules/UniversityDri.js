import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import compose from "recompose/compose";
import {connect} from "react-redux";

import Markdown from "../../common/markdown/Markdown";

import Module from "./common/Module";

import ModuleWrapper from "./common/ModuleWrapper";
import ModuleGroupWrapper from "./common/ModuleGroupWrapper";

import UniversityDriEditor from "../editors/UniversityDriEditor";

import getActions from "../../../redux/api/getActions";
import {withUnivInfo} from "../common/withUnivInfo";
import {RequestParams} from "../../../redux/api/RequestParams";

// eslint-disable-next-line no-unused-vars
const styles = theme => ({
});

// eslint-disable-next-line no-unused-vars
function renderCore(rawModelData, classes, outsideData) {
  const { comment } = rawModelData;

  return (
    <Markdown source={comment} />
  );
}


class UniversityDri extends Module {
  apiParams = {
    universityDri: ({props}) => RequestParams.Builder.withQueryParam("universities", props.univId).build(),
  };

  customRender() {
    const { universities, classes } = this.props,
      outsideData = { universities },
      univDriItems = this.getLatestReadData("universityDri");

    return (
      <ModuleGroupWrapper
        groupTitle={"Informations émanant de la DRI liées à l'université"}
        endPoint={"universityDri"}
        editor={UniversityDriEditor}
        invalidateGroup={this.props.invalidateData}
        defaultModelData={{ universities: [this.props.univId], importance_level: "-" }}
        propsForEditor={{
          outsideData: outsideData,
        }}
      >
        {
          univDriItems.map(rawModelData => (
            <ModuleWrapper
              key={rawModelData.id} // use the id of the model to prevent useless unmount
              buildTitle={(modelData) => modelData.title}
              rawModelData={rawModelData}
              editor={UniversityDriEditor}
              renderCore={renderCore}
              coreClasses={classes}
              outsideData={outsideData}
              moduleInGroupInfos={{ isInGroup: true, invalidateGroup: () => this.props.invalidateData() }}
            />
          ))
        }
      </ModuleGroupWrapper >

    );
  }
}

UniversityDri.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  univId: PropTypes.string.isRequired,
  universities: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => {
  return {
    universityDri: state.api.universityDriAll,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    api: {
      universityDri: (params) => dispatch(getActions("universityDri").readAll(params)),
    },
    invalidateData: () => dispatch(getActions("universityDri").invalidateAll())
  };
};


export default compose(
  withUnivInfo(["universities"]),
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(UniversityDri);
