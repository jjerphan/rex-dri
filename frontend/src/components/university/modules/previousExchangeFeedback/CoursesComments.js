import React, {useState} from "react";
import {withStyles} from "@material-ui/core/styles";
import MuiExpansionPanel from "@material-ui/core/ExpansionPanel";
import MuiExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import MuiExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import PropTypes from "prop-types";
import Markdown from "../../../common/markdown/Markdown";

const ExpansionPanel = withStyles({
  root: {
    border: "1px solid rgba(0, 0, 0, .125)",
    boxShadow: "none",
    "&:not(:last-child)": {
      borderBottom: 0,
    },
    "&:before": {
      display: "none",
    },
    "&$expanded": {
      margin: "auto",
    },
  },
  expanded: {},
})(MuiExpansionPanel);

const ExpansionPanelSummary = withStyles({
  root: {
    backgroundColor: "rgba(0, 0, 0, .03)",
    borderBottom: "1px solid rgba(0, 0, 0, .125)",
    marginBottom: -1,
    minHeight: 56,
    "&$expanded": {
      minHeight: 56,
    },
  },
  content: {
    "&$expanded": {
      margin: "12px 0",
    },
  },
  expanded: {},
})(MuiExpansionPanelSummary);

const ExpansionPanelDetails = withStyles(theme => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiExpansionPanelDetails);


function CoursesComments(props) {
  const [expanded, setExpanded] = useState(-1),
    handleChange = panel => (event, newExpanded) => {
      setExpanded(newExpanded ? panel : false);
    },
    augmentedData = props.data.map(({code, comment}) => ({
      code,
      comment,
      disabled: comment === null || comment === ""
    }));

  return (
    <>
      <Typography variant={"h6"}>Commentaires sur les cours</Typography>
      {
        augmentedData.map(({code, comment, disabled}, key) => (
          <ExpansionPanel square expanded={expanded === key} key={key} onChange={handleChange(key)} disabled={disabled}>
            <ExpansionPanelSummary>
              <Typography style={{fontWeight: 500}}>{code}</Typography>
              {disabled ? <Typography variant={"caption"}>(aucun commentaire saisi)</Typography> : <></>}
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              {!disabled ? <Markdown source={comment}/> : <></>}
            </ExpansionPanelDetails>
          </ExpansionPanel>
        ))
      }
    </>
  );
}

CoursesComments.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    code: PropTypes.string.isRequired,
    comment: PropTypes.string,
  })).isRequired,
};


export default CoursesComments;