import React from "react";

import PropTypes from "prop-types";
import CoursesComments from "./CoursesComments";
import {makeStyles} from "@material-ui/styles";
import CoursesSummary from "./CoursesSummary";


const useStyles = makeStyles(theme => ({
  spacer: {
    marginTop: theme.spacing(3),
  },
}));


function CourseFeedback(props) {
  const {exchangeCourses} = props,
    coursesSummaryData = exchangeCourses.map(course => ({
      courseCode: course.code,
      nbCredit: course.nb_credit,
      languageCode: course.course_feedback.language,
      adequation: course.course_feedback.adequation,
      workingDose: course.course_feedback.working_dose,
      languageFollowingEase: course.course_feedback.language_following_ease
    })),
    coursesCommentsData = exchangeCourses.map(course => ({code: course.code, comment: course.course_feedback.comment})),
    classes = useStyles();

  return (
    <>
      <CoursesSummary data={coursesSummaryData}/>
      <div className={classes.spacer}/>
      <CoursesComments data={coursesCommentsData}/>
    </>
  );
}


CourseFeedback.propTypes = {
  exchangeCourses: PropTypes.array.isRequired,
};

export default CourseFeedback;