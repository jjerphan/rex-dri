import React from "react";
import Grid from "@material-ui/core/Grid";
import TruncatedMarkdown from "../../../common/markdown/TruncatedMarkdown";
import PropTypes from "prop-types";
import MetricFeedback from "../../../common/MetricFeedback";
import Paper from "@material-ui/core/Paper";
import Divider from "@material-ui/core/Divider";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import CourseFeedback from "./CourseFeedback";
import {makeStyles} from "@material-ui/styles";
import Chip from "@material-ui/core/Chip";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Typography from "@material-ui/core/Typography";
import useMediaQuery from "@material-ui/core/useMediaQuery";


const useStyle = makeStyles(theme => ({
  paper: {
    marginBottom: theme.spacing(2),
    padding: theme.spacing(2),
  },
  gradingDiv: {
    margin: `${theme.spacing(1)}px auto`,
  },
  chip: {
    marginRight: theme.spacing(1),
    fontWeight: theme.typography.fontWeightMedium
  },
  expansionPanel: {
    margin: `${theme.spacing(1)}px auto`,
  },
  spacer: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  leftCol: {
    width: 150,
  },
  leftColExtra: {
    width: 150 + 60,
  },
  feedbackHeaderCell: {
    [theme.breakpoints.up("xs")]: {
      margin: theme.spacing(0, 2),
    },
  },
  feedbackHeaderText: {
    textAlign: "center",
  }
}));

function GridColumn(props) {
  const classes = useStyle();

  return (
    <Grid container
          direction="column"
          justify="flex-start"
          alignItems="center">
      {
        props.children.map((c, key) => <Grid key={key} item className={classes.feedbackHeaderCell}>{c}</Grid>)
      }
    </Grid>
  );
}

GridColumn.propTypes = {
  children: PropTypes.array.isRequired,
};

function GridLine(props) {
  return (
    <Grid container
          direction="row"
          justify="space-around"
          alignItems="flex-end">
      {
        props.children.map((c, key) => <Grid key={key} item>{c}</Grid>)
      }
    </Grid>
  );
}

GridLine.propTypes = {
  children: PropTypes.array.isRequired,
};

function FeedbackHeader(props) {
  const classes = useStyle();

  return (
    <GridLine>
      <GridColumn>
        <Typography variant={"h6"} className={classes.feedbackHeaderText}>Niveau académique</Typography>
        <MetricFeedback min={-5} max={5} value={props.academicalLevel}/>
      </GridColumn>
      <GridColumn>
        <Typography variant={"h6"} className={classes.feedbackHeaderText}>Accueil des étudiants étrangers</Typography>
        <MetricFeedback min={-5} max={5} value={props.foreignStudentWelcome}/>
      </GridColumn>
      <GridColumn>
        <Typography variant={"h6"} className={classes.feedbackHeaderText}>Intérêt culturel</Typography>
        <MetricFeedback min={-5} max={5} value={props.culturalInterest}/>
      </GridColumn>
    </GridLine>
  );
}

FeedbackHeader.propTypes = {
  academicalLevel: PropTypes.number.isRequired,
  foreignStudentWelcome: PropTypes.number.isRequired,
  culturalInterest: PropTypes.number.isRequired,
};

function PreviousDeparture(props) {
  const {rawModelData} = props,
    classes = useStyle(),
    cleanSemester = `${rawModelData.exchange.semester.toUpperCase()} ${rawModelData.exchange.year}`;
  const isWide = useMediaQuery("(min-width:1100px)");


  function renderCore() {
    return (
      <>
        <FeedbackHeader academicalLevel={rawModelData.academical_level_appreciation}
                        foreignStudentWelcome={rawModelData.foreign_student_welcome}
                        culturalInterest={rawModelData.cultural_interest}/>

        <Divider/>

        <Typography variant={"h6"}>Avis général</Typography>
        <TruncatedMarkdown comment={rawModelData.general_comment}/>

        <ExpansionPanel className={classes.expansionPanel} elevation={4} expanded={true}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
            <Typography variant={"h6"}>Évaluation des cours suivis</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails style={{display: "block"}}>
            <CourseFeedback exchangeCourses={rawModelData.exchange.exchange_courses}/>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </>
    );
  }

  return (
    <Paper className={classes.paper}>
      <Chip label={cleanSemester} className={classes.chip} color="primary"/>
      <Chip label={rawModelData.exchange.student_major} className={classes.chip} color="secondary"/>
      <Typography display={"inline"}>Florent Chehab TODO</Typography>
      <Divider className={classes.spacer}/>
      <div style={{display: "flex"}}>
        <div className={isWide ? classes.leftColExtra : ""}/>
        <div style={{width: "100%"}}>
          {renderCore()}
        </div>
      </div>
    </Paper>
  );
}


PreviousDeparture.propTypes = {
  rawModelData: PropTypes.object.isRequired,
};


export default PreviousDeparture;
