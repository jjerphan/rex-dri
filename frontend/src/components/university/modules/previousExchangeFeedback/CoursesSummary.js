import React from "react";
import PropTypes from "prop-types";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import MetricFeedback from "../../../common/MetricFeedback";
import {makeStyles} from "@material-ui/styles";
import {getLatestReadDataFromStore} from "../../../../redux/api/utils";
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";

const useStyles = makeStyles(theme => ({
  table: {
    margin: theme.spacing(1),
  },
  smallCard: {
    marginBottom: theme.spacing(2),
  }
}));


class LanguagesHelper {
  static languageMap = new Map();

  static getLanguageName(code) {
    const languages = getLatestReadDataFromStore("languagesAll");
    if (!LanguagesHelper.languageMap.has(code)) {
      languages.forEach(({id, name}) => LanguagesHelper.languageMap.set(id, name));
    }
    return LanguagesHelper.languageMap.get(code);
  }
}

function CoursesSummary(props) {
  const classes = useStyles();
  const wide = false;

  if (wide) {
    return (
      <>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>Cours</TableCell>
              <TableCell>Crédits</TableCell>
              <TableCell>Langue</TableCell>
              <TableCell>Intérêt</TableCell>
              <TableCell>Dose de travail /10</TableCell>
              <TableCell>Facilité à suivre la langue /10</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              props.data.map(
                ({courseCode, nbCredit, languageCode, adequation, workingDose, languageFollowingEase}, idx) => (
                  <TableRow key={idx}>
                    <TableCell scope="course">{courseCode}</TableCell>
                    <TableCell>{nbCredit}</TableCell>
                    <TableCell>
                      {
                        languageCode ?
                          LanguagesHelper.getLanguageName(languageCode)
                          :
                          <em>Non spécifié</em>
                      }
                    </TableCell>
                    <TableCell><MetricFeedback min={-5} max={5} value={adequation}
                                               height={12}/></TableCell>
                    <TableCell>{workingDose}</TableCell>
                    <TableCell>{languageFollowingEase}</TableCell>
                  </TableRow>
                ))
            }
          </TableBody>
        </Table>
      </>
    );
  } else {
    return (
      <>
        {
          props.data.map(
            ({courseCode, nbCredit, languageCode, adequation, workingDose, languageFollowingEase}, key) => (
              <div key={key} className={classes.smallCard}>
                <Chip label={courseCode} color="default"/> <Typography
                variant={"caption"}>({nbCredit} ECTS)</Typography>
                <Typography>Cours enseigné en {LanguagesHelper.getLanguageName(languageCode)}.</Typography>
                <div>
                  <Typography>Intérêt</Typography><MetricFeedback min={-5} max={5} value={adequation}
                                                                  height={12} width={100}/>
                </div>
                {workingDose}
                {languageFollowingEase}
              </div>
            ))
        }
      </>);
  }

}

CoursesSummary.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    courseCode: PropTypes.string.isRequired,
    nbCredit: PropTypes.number.isRequired,
    languageCode: PropTypes.string,
    adequation: PropTypes.number,
    workingDose: PropTypes.number,
    languageFollowingEase: PropTypes.number,
  })).isRequired,
};


export default CoursesSummary;