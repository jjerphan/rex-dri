import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import compose from "recompose/compose";
import {connect} from "react-redux";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

import Markdown from "../../common/markdown/Markdown";

import CloudQueueIcon from "@material-ui/icons/CloudQueue";
import LocalFloristIcon from "@material-ui/icons/LocalFlorist";

import ModuleWrapper from "./common/ModuleWrapper";

import Module from "./common/Module";

import dateStrToStr from "../../../utils/dateStrToStr";

import UniversitySemestersDatesEditor from "../editors/UniversitySemestersDatesEditor";

import getActions from "../../../redux/api/getActions";
import {withUnivInfo} from "../common/withUnivInfo";
import {RequestParams} from "../../../redux/api/RequestParams";

const styles = theme => ({
  root: {
    width: "100%",
    overflowX: "auto",
  },
  tableCell: {
    padding: "2px",
  },
  content: {
    display: "flex",
    alignItems: "center",
  },
  icon: {
    paddingRight: theme.spacing(1)
  }
});

function convertDateStrToStr(date) {
  if (date) {
    return dateStrToStr(date);
  } else {
    return "Non connue.";
  }
}

function renderCore(rawModelData, classes) {
  let { autumn_begin, autumn_end, spring_begin, spring_end, comment } = rawModelData;

  autumn_begin = convertDateStrToStr(autumn_begin);
  autumn_end = convertDateStrToStr(autumn_end);
  spring_begin = convertDateStrToStr(spring_begin);
  spring_end = convertDateStrToStr(spring_end);

  return (
    <>
      <div className={classes.root}>
        <Table >
          <TableHead>
            <TableRow key={0}>
              <TableCell className={classes.tableCell} component="th" > Semestre </TableCell>
              <TableCell className={classes.tableCell} component="th" > Date de début </TableCell>
              <TableCell className={classes.tableCell} component="th" > Date de fin </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>

            <TableRow key={1}>
              <TableCell className={classes.tableCell} component="td">
                <div className={classes.content}>
                  <LocalFloristIcon color='disabled' className={classes.icon} />
                  <div>Printemps</div>
                </div>
              </TableCell>
              <TableCell className={classes.tableCell} component="td" >{spring_begin}</TableCell>
              <TableCell className={classes.tableCell} component="td" >{spring_end}</TableCell>
            </TableRow>

            <TableRow key={2}>
              <TableCell className={classes.tableCell} component="td">
                <div className={classes.content}>
                  <CloudQueueIcon color='disabled' className={classes.icon} />
                  <div>Automne</div>
                </div>
              </TableCell>
              <TableCell className={classes.tableCell} component="td" >{autumn_begin}</TableCell>
              <TableCell className={classes.tableCell} component="td" >{autumn_end}</TableCell>
            </TableRow>

          </TableBody>
        </Table>
      </div>
      <Markdown source={comment} />
    </>
  );
}


class UniversitySemestersDates extends Module {
  apiParams = {
    universitySemestersDates: ({props}) => RequestParams.Builder.withId(props.univId).build(),
  };

  customRender() {
    const semestersDates = this.getLatestReadData("universitySemestersDates");
    const { classes } = this.props;
    return (
      <ModuleWrapper
        buildTitle={() => "Date des semestres"}
        rawModelData={semestersDates}
        editor={UniversitySemestersDatesEditor}
        renderCore={renderCore}
        coreClasses={classes}
      />
    );
  }
}



UniversitySemestersDates.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  univId: PropTypes.string.isRequired
};

const mapStateToProps = (state) => {
  return {
    universitySemestersDates: state.api.universitiesSemestersDatesOne,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    api: {
      universitySemestersDates: (params) => dispatch(getActions("universitiesSemestersDates").readOne(params)),
    },
    invalidateData: () => dispatch(getActions("universitiesSemestersDates").invalidateOne())
  };
};


export default compose(
  withUnivInfo(),
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(UniversitySemestersDates);
