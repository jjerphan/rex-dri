import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import compose from "recompose/compose";
import {connect} from "react-redux";

import Module from "./common/Module";

import ModuleWrapper from "./common/ModuleWrapper";
import ModuleGroupWrapper from "./common/ModuleGroupWrapper";
import Scholarship from "./common/Scholarship";

import UniversityScholarshipEditor from "../editors/UniversityScholarshipEditor";
import getActions from "../../../redux/api/getActions";
import {withUnivInfo} from "../common/withUnivInfo";
import {RequestParams} from "../../../redux/api/RequestParams";

// eslint-disable-next-line no-unused-vars
const styles = theme => ({
});

function renderCore(rawModelData, classes, outsideData) {
  const { comment, frequency, currency, short_description } = rawModelData,
    amountMin = rawModelData.amount_min,
    amountMax = rawModelData.amount_max,
    otherAdvantages = rawModelData.other_advantages;

  return (
    <Scholarship
      currency={currency}
      currencies={outsideData.currencies}
      frequency={frequency}
      shortDescription={short_description}
      amountMin={amountMin}
      amountMax={amountMax}
      otherAdvantages={otherAdvantages}
      comment={comment}
    />
  );
}

class UniversityScholarships extends Module {

  apiParams = {
    universityScholarships: ({props}) => RequestParams.Builder.withQueryParam("universities", props.univId).build(),
  };

  customRender() {
    const univScholarshipsItems = this.getOnlyReadData("universityScholarships"),
      { universities, currencies, classes } = this.props,
      outsideData = {
        universities,
        currencies
      };

    return (
      <ModuleGroupWrapper
        groupTitle={"Bourses liées à l'université"}
        endPoint={"universityScholarship"}
        editor={UniversityScholarshipEditor}
        invalidateGroup={this.props.invalidateData}
        defaultModelData={{ universities: [this.props.univId], importance_level: "-", currency: "EUR", obj_moderation_level: 0 }}
        propsForEditor={{
          outsideData: outsideData,
        }}
      >
        {
          univScholarshipsItems.map((rawModelData, idx) => (
            <ModuleWrapper
              key={idx}
              buildTitle={(modelData) => modelData.title}
              rawModelData={rawModelData}
              editor={UniversityScholarshipEditor}
              renderCore={renderCore}
              coreClasses={classes}
              outsideData={outsideData}
              moduleInGroupInfos={{ isInGroup: true, invalidateGroup: () => this.props.invalidateData() }}
            />
          ))
        }
      </ModuleGroupWrapper >

    );
  }
}

UniversityScholarships.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  univId: PropTypes.string.isRequired,
  universities: PropTypes.array.isRequired,
  currencies: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => {
  return {
    universityScholarships: state.api.universityScholarshipsAll,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    api: {
      universityScholarships: (params) => dispatch(getActions("universityScholarships").readAll(params)),
    },
    invalidateData: () => dispatch(getActions("universityScholarships").invalidateAll())
  };
};


export default compose(
  withUnivInfo(["universities", "currencies"]),
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(UniversityScholarships);
