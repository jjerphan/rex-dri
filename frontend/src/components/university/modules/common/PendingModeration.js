import React from "react";
import CustomComponentForAPI from "../../../common/CustomComponentForAPI";
import { connect } from "react-redux";
import { openFullScreenDialog, closeFullScreenDialog } from "../../../../redux/actions/fullScreenDialog";

import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import compose from "recompose/compose";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import Divider from "@material-ui/core/Divider";
import getActions from "../../../../redux/api/getActions";

import editorStyle from "../../../editor/editorStyle";
import Loading from "../../../common/Loading";
import {RequestParams} from "../../../../redux/api/RequestParams";



/**
 * Class to handle models that are pending moderation.
 *
 * @class PendingModeration
 * @extends {CustomComponentForAPI}
 */
class PendingModeration extends CustomComponentForAPI {

  apiParams = {
    pendingModeration: ({props}) => {
      const { contentTypeId, id } = props.modelInfo;
      return RequestParams.Builder.withEndPointAttrs([contentTypeId, id]).build();
    },
  };

  /**
   * Force update to force call of componentDidUpdate en mount and force the rendering of the moderation panel.
   *
   * @memberof PendingModeration
   */
  componentDidMount() {
    super.componentDidMount();
    this.forceUpdate();
  }

  /**
   * Customization to handle opening the app full screen dialog when needed.
   *
   * @memberof PendingModeration
   */
  componentDidUpdate(prevProps, prevState, snapshot) {
    super.componentDidUpdate(prevProps, prevState, snapshot);
    this.props.openFullScreenDialog(this.renderPanel());
  }

  /**
   * Retrieve the raw model data from the pending moderation data
   *
   * @returns
   * @memberof PendingModeration
   */
  getRawModelDataFromPending() {
    const pendingObject = this.getLatestReadData("pendingModeration")[0];
    return Object.assign(
      {},
      pendingObject.new_object,
      { id: pendingObject.object_id });
  }

  /**
   * Renders top of the panel
   *
   * @param {object} rawModelData
   * @returns
   * @memberof PendingModeration
   */
  renderPendingModerationInfo(rawModelData) {
    return (
      <>
        <Button
          variant="contained"
          color="primary"
          className={this.props.classes.editButton}
          onClick={() => this.props.editFromPendingModeration(rawModelData)}
        >
          Éditer à partir de cette version
        </Button>
        {
          this.props.userCanModerate ? <></> : <Typography variant='caption' align='center'>Vous n'avez pas les droits nécessaires pour modérer cet élément.</Typography>
        }
        <Button
          variant='outlined'
          color="primary"
          disabled={!this.props.userCanModerate}
          className={this.props.classes.editButton}
          onClick={() => this.props.moderatePendingModeration(rawModelData)}
        >
          Valider cette version
        </Button>
        <Divider />
      </>
    );
  }


  /**
   * Close the panel and make sure to update parent
   *
   * @memberof PendingModeration
   */
  closePanel() {
    this.props.closeFullScreenDialog();
    this.props.closePendingModerationPanel();
    this.props.resetPendingModeration();
  }

  /**
   * Renders the panel content
   *
   * @returns
   * @memberof PendingModeration
   */
  renderPanel() {
    if (!this.allApiDataIsReady()) {
      return <Loading />;
    }

    const { classes } = this.props,
      pendingModelData = this.getRawModelDataFromPending();

    return (
      <>
        <AppBar className={classes.appBar} >
          <Toolbar>
            <IconButton color="inherit" onClick={() => this.closePanel()} aria-label="Close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" className={classes.flex}>
              Version en attente de modération
            </Typography>
          </Toolbar>
        </AppBar>
        <Paper className={classes.paper}>
          {this.renderPendingModerationInfo(pendingModelData)}
          <br></br>
          {this.props.renderer.renderTitle(pendingModelData)}
          {this.props.renderer.renderCore(pendingModelData)}
        </Paper>
      </>
    );
  }

  /**
   * @override
   *
   * @returns
   * @memberof PendingModeration
   */
  render() {
    return <></>;
  }
}

PendingModeration.propTypes = {
  classes: PropTypes.object.isRequired,
  modelInfo: PropTypes.shape({
    contentTypeId: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired,
  }).isRequired,
  editFromPendingModeration: PropTypes.func.isRequired,
  closePendingModerationPanel: PropTypes.func.isRequired,
  moderatePendingModeration: PropTypes.func.isRequired,
  userCanModerate: PropTypes.bool.isRequired,
  openFullScreenDialog: PropTypes.func.isRequired,
  closeFullScreenDialog: PropTypes.func.isRequired,
  renderer: PropTypes.object.isRequired,
};

PendingModeration.defaultProps = {
  // eslint-disable-next-line no-console
  closePendingModerationPanel: () => console.error("Dev forgot something...")
};


const mapStateToProps = (state) => {
  return {
    pendingModeration: state.api.pendingModerationObjAll
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    api: {
      pendingModeration: (params) => dispatch(getActions("pendingModerationObj").readAll(params)),
    },
    resetPendingModeration: () => dispatch(getActions("pendingModerationObj").invalidateAll()),
    openFullScreenDialog: (innerNodes) => dispatch(openFullScreenDialog(innerNodes)),
    closeFullScreenDialog: () => dispatch(closeFullScreenDialog()),
  };
};


const styles = theme => ({
  ...editorStyle(theme),
  editButton: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  }
});


export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(PendingModeration);
