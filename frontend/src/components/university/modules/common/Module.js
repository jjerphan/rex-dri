import CustomComponentForAPI from "../../../common/CustomComponentForAPI";

/**
 * @class Module
 * @extends {CustomComponentForAPI}
 * @extends React.Component
 * @abstract
 */
class Module extends CustomComponentForAPI {
  componentDidUpdate(prevProps, prevState, snapshot) {
    super.componentDidUpdate(prevProps, prevState, snapshot);

    this.getListPropsNeedRead()
      .forEach(propName => this.performReadFromApi(propName));
  }
}

Module.propTypes = {};

export default Module;
