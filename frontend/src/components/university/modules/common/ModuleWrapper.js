import React, { Component } from "react";
import PropTypes from "prop-types";

import withStyles from "@material-ui/core/styles/withStyles";
import compose from "recompose/compose";

import Paper from "@material-ui/core/Paper";
import red from "@material-ui/core/colors/red";
import orange from "@material-ui/core/colors/orange";
import green from "@material-ui/core/colors/green";

import Alert from "../../../common/Alert";


import renderUsefulLinks from "./moduleWrapperFunctions/renderUsefulLinks";
import renderFirstRow from "./moduleWrapperFunctions/renderFirstRow";
import renderTitle from "./moduleWrapperFunctions/renderTitle";
import History from "./History";
import PendingModeration from "./PendingModeration";


/**
 * Wrapper around modules to handle in a standard way the edition/moderation/history version;
 * displaying of certain attributes, etc.
 *
 * @class ModuleWrapper
 * @extends React.Component
 */
class ModuleWrapper extends Component {


  /**
   * Stores a reference to the editor linked to the this module wrapper so that we can manipulate it directly.
   *
   * @memberof ModuleWrapper
   */
  editorInstance = null;

  state = {
    editorOpen: false,
    historyOpen: false,
    pendingModerationOpen: false,
    rawModelDataForEditor: this.props.rawModelData,
    alert: { open: false }
  };

  /**
   * Function to open the editor panels.
   * It also checks that there is no model pending moderation
   *
   * @param {boolean} [ignorePendingModeration=false]
   * @memberof ModuleWrapper
   */
  openEditorPanel(ignorePendingModeration = false) {
    if (ignorePendingModeration || !this.props.rawModelData.has_pending_moderation) {
      this.setState({ editorOpen: true });
    } else {
      this.alertThereIsSomethingPendingModeration();
    }
  }

  /**
   * Function to close the editor panel
   * If something was saved and we are in a group module, we need to refresh every thing.
   *
   * @param {boolean} [somethingWasSaved=false]
   * @memberof ModuleWrapper
   */
  closeEditorPanel(somethingWasSaved = false) {
    this.setState({ editorOpen: false });
    if (somethingWasSaved && this.props.moduleInGroupInfos.isInGroup) {
      this.props.moduleInGroupInfos.invalidateGroup();
    }
  }

  /**
   * Function to edit the data that is pending moderation
   *
   * @param {object} pendingModelData
   * @memberof ModuleWrapper
   */
  editFromPendingModeration(pendingModelData) {
    this.setState({
      rawModelDataForEditor: pendingModelData,
    });
    this.closePendingModerationPanel();
    this.openEditorPanel(true);
  }

  /**
   * Function to edit the current data
   *
   * @memberof ModuleWrapper
   */
  editCurrent() {
    this.setState({
      rawModelDataForEditor: this.props.rawModelData,
    });
    this.openEditorPanel();
  }

  /**
   * Function to edit from a version
   *
   * @param {object} versionModelData
   * @memberof ModuleWrapper
   */
  editFromVersion(versionModelData) {

    this.setState({
      rawModelDataForEditor: versionModelData,
    });

    this.closeHistoryPanel();
    this.openEditorPanel();
  }

  /**
   * Function to moderate a pendingModeration model
   *
   * @param {object} modelDataInModeration
   * @memberof ModuleWrapper
   */
  moderatePendingModeration(modelDataInModeration) {
    this.editorInstance.performSave(modelDataInModeration);
    this.closePendingModerationPanel();
  }

  /**
   * Renders the tiltle of the model
   *
   * @param {object} rawModelData
   * @returns
   * @memberof ModuleWrapper
   */
  renderTitle(rawModelData) {
    return renderTitle(rawModelData, this.props.classes, this.props.buildTitle);
  }

  /**
   * Renders the core of the model
   *
   * @param {object} rawModelData
   * @returns
   * @memberof ModuleWrapper
   */
  renderCore(rawModelData) {
    return (
      <>
        {this.props.renderCore(rawModelData, this.props.coreClasses, this.props.outsideData)}
        {renderUsefulLinks(rawModelData, this.props.classes, this.props.theme)}
      </>
    );
  }


  /**
   * Open the pending moderation panel
   *
   * @memberof ModuleWrapper
   */
  openPendingModerationPanel() {
    this.setState({ pendingModerationOpen: true });
  }

  /**
   * Close the pending moderation panel
   *
   * @memberof ModuleWrapper
   */
  closePendingModerationPanel() {
    this.setState({ pendingModerationOpen: false });
  }

  /**
   * Close the history panel
   *
   * @memberof ModuleWrapper
   */
  closeHistoryPanel() {
    this.setState({ historyOpen: false });
  }

  /**
   * FYI
   * The editor is always rendered whereas the pending Moderation, History and Alert and rendered only when needed.
   *
   * @returns
   * @memberof ModuleWrapper
   */
  render() {
    const { classes, rawModelData } = this.props;

    return (
      <>
        <this.props.editor
          open={this.state.editorOpen}
          closeEditorPanel={(somethingWasSaved) => this.closeEditorPanel(somethingWasSaved)}
          subscribeToModuleWrapper={(editorInstance) => this.editorInstance = editorInstance}
          rawModelData={this.state.rawModelDataForEditor}
          outsideData={this.props.outsideData}
        />
        {this.state.alert.open ?
          <Alert
            {...this.state.alert}
            handleClose={() => this.setState({ alert: { open: false } })}
          />
          :
          <></>
        }
        {this.state.historyOpen ?
          <History
            renderer={this}
            modelInfo={{ contentTypeId: rawModelData.content_type_id, id: rawModelData.id }}
            closeHistoryPanel={() => this.closeHistoryPanel()}
            editFromVersion={(modelData) => this.editFromVersion(modelData)}
          />
          :
          <></>
        }
        {this.state.pendingModerationOpen ?
          <PendingModeration
            renderer={this}
            modelInfo={{ contentTypeId: rawModelData.content_type_id, id: rawModelData.id }}
            closePendingModerationPanel={() => this.closePendingModerationPanel()}
            editFromPendingModeration={(pendingModelData) => this.editFromPendingModeration(pendingModelData)}
            moderatePendingModeration={(modelData) => this.moderatePendingModeration(modelData)}
            userCanModerate={rawModelData.obj_info.user_can_moderate}
          />
          :
          <></>
        }
        <Paper className={classes.root} square={true}>
          {renderFirstRow.bind(this)()}
          {this.renderCore(this.props.rawModelData)}
        </Paper>
      </>
    );
  }


  /**
   *
   * Alert functions
   *
   *
   *
   */

  alertThereIsSomethingPendingModeration() {
    this.setState({
      alert: {
        open: true,
        info: false,
        title: "Une version est en attente de modération",
        description: "Vous vous apprêter à éditer le module tel qu'il est présenté sur la page principale. Toutefois, il conseillé d'éditer le module à partir de la version en attente de modération car cette dernière sera écrasée par votre contribution.",
        agreeText: "Ok, je vais voir la version en attente de modération",
        disagreeText: "Je ne veux pas me baser sur le travail en attente de modération",
        handleResponse: (agree) => {
          if (agree) {
            this.openPendingModerationPanel();
          } else {
            this.openEditorPanel(true);
          }
        },
        multilineButtons: true,
      }
    });
  }

}


ModuleWrapper.defaultProps = {
  buildTitle: () => null,
  moduleInGroupInfos: { isInGroup: false, invalidateGroup: () => null },
};

ModuleWrapper.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  rawModelData: PropTypes.object.isRequired,
  buildTitle: PropTypes.func.isRequired,
  renderCore: PropTypes.func.isRequired,
  coreClasses: PropTypes.object.isRequired,
  outsideData: PropTypes.object,
  moduleInGroupInfos: PropTypes.shape({ isInGroup: PropTypes.bool.isRequired, invalidateGroup: PropTypes.func }).isRequired,
};


const styles = theme => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(1),
    // marginBottom: theme.spacing(2)
  },
  rootLinks: {
    display: "flex",
    justifyContent: "flex-start",
    flexWrap: "wrap",
  },
  green: {
    color: green.A700,
  },
  orange: {
    color: orange.A700,
  },
  red: {
    color: red.A700
  },
  disabled: {
    color: theme.palette.action.disabled
  },
  button: {
    width: theme.spacing(5),
    height: theme.spacing(5),
    // margin: theme.spacing(0.5),
  },
  chip: {
    margin: theme.spacing(1),
  },
  titleContainer: {
    display: "flex",
    alignItems: "center",
  },
  titleIcon: {
    paddingRight: theme.spacing(1),
    ...theme.typography.h4,
  }
});


export default compose(
  withStyles(styles, { withTheme: true }),
)(ModuleWrapper);
