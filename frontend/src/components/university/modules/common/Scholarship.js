import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";

import Markdown from "../../../common/markdown/Markdown";
import moneyConversion from "../../../../utils/convertAmountToEur";
import getCurrencySymbol from "../../../../utils/getCurrencySymbol";
import Typography from "@material-ui/core/Typography";

const styles = theme => {
  return {
    content: {
      display: "flex",
      alignItems: "center",
    },
    textAmount: {
      ...theme.typography.subtitle1,
      color: theme.palette.text.primary,
      fontWeight: theme.typography.fontWeightMedium
    },
    icon: {
      ...theme.typography.subtitle1,
      paddingRight: theme.spacing(0.5),
      paddingLeft: theme.spacing(0.5),
      color: theme.palette.text.primary
    },
    frequency: {
      ...theme.typography.subtitle1,
      color: theme.palette.text.primary,
      paddingLeft: theme.spacing(1)
    },
    item: {
      color: theme.palette.text.secondary,
      fontWeight: theme.typography.fontWeightMedium
    }
  };
};


class Scholarship extends React.Component {

  getFrequency() {
    const table = {
      w: "/semaine",
      m: "/mois",
      s: "/semestre",
      y: "/année",
      o: "(une seule fois)"
    };
    const { frequency } = this.props;
    return table[frequency];
  }

  getSymbol() {
    return getCurrencySymbol(this.props.currency);
  }

  convertAmountToEur(amount) {
    const { currency } = this.props;
    return moneyConversion(amount, currency);
  }

  getAmounts() {
    const { amountMin, amountMax } = this.props;
    if (amountMax != null && amountMax != amountMin) {
      return `${amountMin}${this.getSymbol()} – ${amountMax}${this.getSymbol()}`;
    } else {
      return `${amountMin}${this.getSymbol()}`;
    }
  }

  getConvertedAmounts() {
    const { amountMin, amountMax, currency } = this.props,
      convertedMin = this.convertAmountToEur(amountMin);

    // we only need for errors in conversion on min.
    // They share the same currency, so if one fails, the other will too.
    if (convertedMin === null) {
      return `Monnaie non reconnue: ${currency}`;
    }

    if (amountMax != null && amountMax != amountMin) {
      return `${convertedMin}€ – ${this.convertAmountToEur(amountMax)}€`;
    } else {
      return `${convertedMin}€`;
    }
  }

  renderFinancialAdvantage() {
    const { amountMin, currency, classes } = this.props;

    return (
      <>
        <Typography className={classes.item} variant='h5'> Avantage financier : </Typography>
        {
          amountMin !== null ? (
            <>
              <Typography className={classes.textAmount}> {this.getAmounts()}{" "}{this.getFrequency()}</Typography>
              {
                currency != "EUR" ?
                  <Typography variant='caption' ><em>(≈ {this.getConvertedAmounts()}{" "}{this.getFrequency()})</em></Typography>
                  :
                  <></>
              }
            </>
          ) : (
            <Typography variant='caption'><em>Aucun avantage financier est notifié.</em></Typography>
          )
        }
      </>
    );
  }

  renderOtherAdvantages() {
    const { classes, otherAdvantages } = this.props;

    return (
      <>
        <Typography className={classes.item} variant='h5' > Autre(s) avantage(s) : </Typography>
        {
          otherAdvantages != "" && otherAdvantages !== null ?
            <Markdown source={otherAdvantages} />
            :
            <Typography variant='caption'><em>Aucun autre avantage a été notifié.</em></Typography>
        }
      </>
    );
  }

  render() {
    const { shortDescription, comment, classes, theme } = this.props;

    return (
      <>
        <Typography className={classes.item} variant='h5'>{shortDescription}</Typography>
        {this.renderFinancialAdvantage()}
        <div style={{ height: theme.spacing(2) }} />
        {this.renderOtherAdvantages()}
        <Typography className={classes.item} variant='h5'> Informations complémentaires : </Typography>
        <Markdown source={comment} />
      </>
    );
  }
}

Scholarship.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  currency: PropTypes.string.isRequired,
  frequency: PropTypes.string,
  shortDescription: PropTypes.string.isRequired,
  comment: PropTypes.string.isRequired,
  otherAdvantages: PropTypes.string,
  amountMin: PropTypes.string,
  amountMax: PropTypes.string,
  currencies: PropTypes.array.isRequired,
};

export default withStyles(styles, { withTheme: true })(Scholarship);
