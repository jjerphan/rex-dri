import React, { Component } from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import compose from "recompose/compose";
import { getLatestReadDataFromStore } from "../../../../redux/api/utils";

import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Tooltip from "@material-ui/core/Tooltip";
import Fab from "@material-ui/core/Fab";

import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";

import AddIcon from "@material-ui/icons/Add";


import green from "@material-ui/core/colors/green";


/**
 * Class for wrapping modules that consist of multiple similar stuff.
 *
 * It basically enables add feature.
 *
 * @class ModuleGroupWrapper
 * @extends React.Component
 */
class ModuleGroupWrapper extends Component {
  state = {
    editorOpen: false,
  };

  /**
   * Opens the editor panel
   *
   * @memberof ModuleGroupWrapper
   */
  openEditorPanel() {
    this.setState({ editorOpen: true });
  }

  /**
   * Close the editor panel and reset the data if there had been a save.
   *
   * @param {boolean} [somethingWasSaved=false]
   * @memberof ModuleGroupWrapper
   */
  closeEditorPanel(somethingWasSaved = false) {
    this.setState({ editorOpen: false });
    if (somethingWasSaved) {
      this.props.invalidateGroup();
    }
  }

  render() {
    const { classes, groupTitle, endPoint, defaultModelData } = this.props,
      userCanPostTo = getLatestReadDataFromStore("userDataOne").owner_can_post_to,
      disabled = userCanPostTo.indexOf(endPoint) < 0;

    return (
      <Paper className={classes.root}>
        <this.props.editor
          {...this.props.propsForEditor}
          rawModelData={defaultModelData}
          open={this.state.editorOpen}
          closeEditorPanel={(somethingWasSaved) => this.closeEditorPanel(somethingWasSaved)}
          subscribeToModuleWrapper={() => { }}
        />
        <Grid container spacing={1} alignItems='center'>

          <Grid item xs>
            <Typography variant='h6' align='center' color="textSecondary"><em>{groupTitle}</em></Typography>
          </Grid>

          <Grid item xs={2} style={{ textAlign: "right" }}>
            <Tooltip title={disabled ? "Vous ne pouvez pas ajouter d'élément" : "Ajouter un élément"} placement="top">
              <div style={{ display: "inline-block" }}> {/* Needed to fire events for the tooltip when below is disabled! */}
                <Fab
                  size='small'
                  aria-label="Ajouter un élément"
                  disabled={disabled}
                  style={disabled ? {} : { backgroundColor: green.A700 }}
                  className={classes.button}
                  onClick={() => this.openEditorPanel()}
                >
                  <AddIcon />
                </Fab>
              </div>
            </Tooltip>
          </Grid>

        </Grid>
        <Divider />
        {this.props.children}
      </Paper>
    );
  }
}

ModuleGroupWrapper.propTypes = {
  classes: PropTypes.object.isRequired,
  groupTitle: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  endPoint: PropTypes.string.isRequired,
  invalidateGroup: PropTypes.func.isRequired,
  propsForEditor: PropTypes.shape({
    outsideData: PropTypes.object,
  }),
  defaultModelData: PropTypes.object.isRequired, // to populate the fields of the form with default values
};


const styles = theme => ({
  root: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    paddingRight: theme.spacing(2),
    paddingLeft: theme.spacing(2),
    // marginBottom: theme.spacing(2),
    flexGrow: 1,
  },
  green: {
    color: green.A200,
  },
  button: {
    width: theme.spacing(5),
    height: theme.spacing(5),
    boxShadow: "none",
    marginBottom: theme.spacing(1),
  },
});

export default compose(
  withStyles(styles, { withTheme: true }),
)(ModuleGroupWrapper);
