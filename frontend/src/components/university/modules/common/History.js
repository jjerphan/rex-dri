import React from "react";
import CustomComponentForAPI from "../../../common/CustomComponentForAPI";
import { openFullScreenDialog, closeFullScreenDialog } from "../../../../redux/actions/fullScreenDialog";

import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import compose from "recompose/compose";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import Divider from "@material-ui/core/Divider";

import MobileStepper from "@material-ui/core/MobileStepper";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";

import Loading from "../../../common/Loading";
import dateTimeStrToStr from "../../../../utils/dateTimeStrToStr";

import editorStyle from "../../../editor/editorStyle";

import getActions from "../../../../redux/api/getActions";
import {RequestParams} from "../../../../redux/api/RequestParams";



/**
 * Component to display the previous versions of module
 *
 * @class History
 * @extends {CustomComponentForAPI}
 */
class History extends CustomComponentForAPI {
  // Store the version that is being viewed
  state = {
    versionInView: 1
  };

  apiParams = {
    versions: ({props}) => {
      const { contentTypeId, id } = props.modelInfo;
      return RequestParams.Builder.withEndPointAttrs([contentTypeId, id]).build();
    },
  };

  /**
   * Move to next or previous version with step
   *
   * @param {number} step
   * @memberof History
   */
  handleGoBy(step) {
    this.setState({ versionInView: this.state.versionInView + step });
  }

  /**
   * Returns the versions matching the props.
   *
   * @returns {Array}
   * @memberof History
   */
  getVersions() {
    return this.getLatestReadData("versions").map((el) => { return el.data; });
  }

  /**
   * As soon as the History is added to the DOM, we make sure to update it,
   * To force the call to `componentDidUpdate`.
   *
   * @memberof Editor
   */
  componentDidMount() {
    super.componentDidMount();
    this.forceUpdate();
  }

  /**
   * Extends function to open panel in redux and load Data
   *
   * @memberof History
   */
  componentDidUpdate(prevProps, prevState, snapshot) {
    super.componentDidUpdate(prevProps, prevState, snapshot);
    this.props.openFullScreenDialog(this.renderPanel());
  }

  /**
   * Function to close history panel
   *
   * @memberof History
   */
  closeHistory() {
    this.props.closeHistoryPanel();
    this.props.closeFullScreenDialog();
    this.props.resetVersions();
  }

  /**
   * Renders the panel that contain the history data
   *
   * @returns
   * @memberof History
   */
  renderPanel() {
    if (!this.allApiDataIsReady()) {
      return <Loading />;
    }

    const { classes } = this.props;
    return (
      <>
        <AppBar className={classes.appBar} >
          <Toolbar>
            <IconButton color="inherit" onClick={() => this.closeHistory()} aria-label="Close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" className={classes.flex}>
              Parcours de l'historique
            </Typography>
          </Toolbar>
        </AppBar>
        <Paper className={classes.paper}>
          {this.renderHistory()}
        </Paper>
      </>
    );
  }

  /**
   * Renders the versions previewer and steppers
   *
   * @returns
   * @memberof History
   */
  renderHistory() {
    const { theme } = this.props,
      versions = this.getVersions(),
      activeStep = this.state.versionInView,
      maxSteps = versions.length;

    // eslint-disable-next-line react/prop-types
    let { rawModelData } = this.props.renderer.props;
    const newRawModelData = Object.assign({}, rawModelData, versions[activeStep]);
    return (
      <>
        <MobileStepper
          steps={versions.length}
          position="static"
          activeStep={activeStep}
          // className={classes.mobileStepper}
          nextButton={
            <Button color="secondary" size="small" onClick={() => this.handleGoBy(1)} disabled={activeStep >= maxSteps - 1}>
              Version précédente
              {theme.direction === "rtl" ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
            </Button>
          }
          backButton={
            <Button color="secondary" size="small" onClick={() => this.handleGoBy(-1)} disabled={activeStep === 0}>
              {theme.direction === "rtl" ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
              Version suivante
            </Button>
          }
        />
        <br></br>
        {this.renderVersionInfo(newRawModelData)}
        <br></br>
        {this.props.renderer.renderTitle(newRawModelData)}
        {this.props.renderer.renderCore(newRawModelData)}
      </>
    );
  }


  /**
   * Render a version
   *
   * @param {object} rawModelData
   * @returns
   * @memberof History
   */
  renderVersionInfo(rawModelData) {
    let dateInfo = <em>(Information non connue.)</em>;
    const { updated_on } = rawModelData,
      versions = this.getVersions();
    if (updated_on) {
      const data = dateTimeStrToStr(updated_on);
      dateInfo = `${data.date} à ${data.time}`;
    }
    return (
      <>
        <Typography variant='caption' align='center'>Les versions successives d'un même utilisateur ne sont pas enregistrés (dans de tels cas, seul la dernière est conservée).</Typography>
        <Typography variant='h6' align='center'>Version n°{versions.length - this.state.versionInView} du {dateInfo}</Typography>
        <Button
          variant='outlined'
          color="primary"
          className={this.props.classes.editButton}
          onClick={() => this.props.editFromVersion(rawModelData)}
        >
          Éditer à partir de cette version
        </Button>
        <Divider />
      </>
    );
  }

  /**
   * @override
   *
   * @returns
   * @memberof History
   */
  render() {
    return <></>;
  }
}

History.propTypes = {
  classes: PropTypes.object.isRequired,
  editFromVersion: PropTypes.func.isRequired,
  closeHistoryPanel: PropTypes.func.isRequired,
  resetVersions: PropTypes.func.isRequired,
  versions: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  openFullScreenDialog: PropTypes.func.isRequired,
  closeFullScreenDialog: PropTypes.func.isRequired,
  modelInfo: PropTypes.object.isRequired,
  renderer: PropTypes.object.isRequired,
};

History.defaultProps = {
  // eslint-disable-next-line no-console
  closeHistoryPanel: () => console.error("Dev forgot something..."),
};


const mapStateToProps = (state) => {
  return {
    versions: state.api.versionsAll
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    api: {
      versions: (params) => dispatch(getActions("versions").readAll(params)),
    },
    resetVersions: () => dispatch(getActions("versions").invalidateAll()),
    openFullScreenDialog: (innerNodes) => dispatch(openFullScreenDialog(innerNodes)),
    closeFullScreenDialog: () => dispatch(closeFullScreenDialog()),
  };
};


const styles = theme => ({
  ...editorStyle(theme),
  editButton: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  }
});


export default compose(
  withStyles(styles, { withTheme: true }),
  connect(mapStateToProps, mapDispatchToProps)
)(History);
