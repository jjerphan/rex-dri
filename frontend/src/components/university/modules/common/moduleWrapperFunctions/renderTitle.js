import React from "react";
import NotificationImportantIcon from "@material-ui/icons/NotificationImportant";
import Typography from "@material-ui/core/Typography";
import classNames from "classnames";

export default function renderTitle(rawModelData, classes, buildTitle) {

  const importanceLevel = rawModelData.importance_level;
  const title = buildTitle(rawModelData);
  if (title) {
    if (importanceLevel && importanceLevel != "-") {
      let c = classNames(classes.titleIcon);
      if (importanceLevel == "++") {
        c = classNames(classes.titleIcon, classes.red);
      } else if (importanceLevel == "+") {
        c = classNames(classes.titleIcon, classes.orange);
      }
      return (
        <div className={classes.titleContainer}>
          <NotificationImportantIcon className={c} />
          <Typography variant='h4'>{title}</Typography>
        </div>
      );
    } else {
      return (
        <Typography variant='h4'>{title}</Typography>
      );
    }
  } else {
    return (<></>);
  }
}
