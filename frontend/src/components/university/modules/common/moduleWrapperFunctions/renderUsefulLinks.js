import React from "react";
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";
import Avatar from "@material-ui/core/Avatar";
import LinkIcon from "@material-ui/icons/Link";

export default function renderUsefulLinks(rawModelData, classes, theme) {
  const usefulLinks = rawModelData.useful_links;

  if (!usefulLinks) {
    return (<></>);
  }

  const nbItems = usefulLinks.length;
  if (nbItems == 0) {
    return (<></>);
  }
  const s = nbItems > 1 ? "s" : "";
  return (
    <>
      <Typography variant='caption' style={{ paddingTop: theme.spacing(2) }}> Source{s} :</Typography>
      <div className={classes.rootLinks}>
        {
          usefulLinks.map((el, index) => {
            return (
              <Chip
                key={index}
                avatar={
                  <Avatar>
                    <LinkIcon />
                  </Avatar>
                }
                label={el.description}
                className={classes.chip}
                color="secondary"
                onClick={() => open(el.url, "_blank")}
                variant="outlined"
              />
            );
          })
        }

      </div>
    </>
  );
}
