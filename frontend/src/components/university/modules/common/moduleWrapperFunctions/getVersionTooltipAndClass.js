export default function getVersionTooltipAndClass(nbVersions) {
  if (typeof nbVersions == "undefined" || isNaN(nbVersions)) {
    return {
      versionTooltip: "Ce contenu n'est pas versionné.",
      versionClass: "disabled"
    };
  }

  if (nbVersions <= 1) {
    return {
      versionTooltip: "Il n'existe pas d'autres versions de ce contenu.",
      versionClass: "disabled"
    };
  } else if (nbVersions == 2) {
    return {
      versionTooltip: "Le contenu actuel constitue la première mise à jour de ce module, vous pouvez voir la version initiale en cliquant sur cette icône.",
      versionClass: "green"
    };
  } else {
    return {
      versionTooltip: "Le contenu actuel constitue la " + nbVersions + "ème mise à jour de ce module, vous pouvez voir les versions précédentes en cliquant sur cette icône.",
      versionClass: "green"
    };
  }
}
