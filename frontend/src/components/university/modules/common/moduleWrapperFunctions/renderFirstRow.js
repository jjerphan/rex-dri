import React from "react";
import renderTitle from "./renderTitle";
import renderUpdateInfo from "./renderUpdateInfo";
import getEditTooltipAndClass from "./getEditTooltipAndClass";
import getVersionTooltipAndClass from "./getVersionTooltipAndClass";
import getModerationTooltipAndClass from "./getModerationTooltipAndClass";

import Grid from "@material-ui/core/Grid";
import MyBadge from "../../../../common/MyBadge";
import IconButton from "@material-ui/core/IconButton";

import VerifiedUserIcon from "@material-ui/icons/VerifiedUser";
import CreateIcon from "@material-ui/icons/Create";

import SettingsBackRestoreIcon from "@material-ui/icons/SettingsBackupRestore";
import Tooltip from "@material-ui/core/Tooltip";


export default function renderFirstRow() {
  const { classes, theme, rawModelData } = this.props,
    nbVersions = Math.max(0, rawModelData.nb_versions),
    hasPendingModeration = rawModelData.has_pending_moderation;

  const { user_can_edit: userCanEdit, user_can_moderate: userCanModerate } = rawModelData.obj_info,
    { versionTooltip, versionClass } = getVersionTooltipAndClass(nbVersions),
    { moderTooltip, moderClass } = getModerationTooltipAndClass(hasPendingModeration, userCanEdit),
    { editTooltip, editClass } = getEditTooltipAndClass(userCanEdit, userCanModerate);

  return (
    <Grid container spacing={1}>
      <Grid item xs style={{ paddingBottom: theme.spacing(1) }}>
        {renderTitle(this.props.rawModelData, this.props.classes, this.props.buildTitle)}
        {renderUpdateInfo.bind(this)()}
      </Grid>
      <Grid item xs={4} style={{ textAlign: "right" }}>

        <Tooltip title={moderTooltip} placement="top">
          <div style={{ display: "inline-block" }}> {/* Needed to fire events for the tooltip when below is disabled! when below is disabled!! */}
            <MyBadge badgeContent={hasPendingModeration ? 1 : 0} color="secondary" minNumber={1}>
              <IconButton aria-label="Modération" disabled={moderClass == "disabled" || moderClass == "green"} onClick={() => this.openPendingModerationPanel()} className={classes.button}>
                <VerifiedUserIcon className={classes[moderClass]} />
              </IconButton>
            </MyBadge>
          </div>
        </Tooltip>

        <Tooltip title={editTooltip} placement="top">
          <div style={{ display: "inline-block" }}> {/* Needed to fire events for the tooltip when below is disabled!! */}
            <IconButton aria-label="Éditer" className={classes.button} disabled={editClass == "disabled"} onClick={() => this.editCurrent()}>
              <CreateIcon className={classes[editClass]} />
            </IconButton>
          </div>
        </Tooltip>

        <Tooltip title={versionTooltip} placement="top">
          <div style={{ display: "inline-block" }}> {/* Needed to fire events for the tooltip when below is disabled!! */}
            <MyBadge badgeContent={nbVersions} color="secondary" minNumber={2}>
              <IconButton aria-label="Restorer" disabled={versionClass == "disabled"} className={classes.button} onClick={() => this.setState({ historyOpen: true })}>
                <SettingsBackRestoreIcon className={classes[versionClass]} />
              </IconButton>
            </MyBadge>
          </div>
        </Tooltip>
      </Grid>

    </Grid>
  );
}
