import React from "react";
import Typography from "@material-ui/core/Typography";
import dateTimeStrToStr from "../../../../../utils/dateTimeStrToStr";
import LinkToUser from "../../../../common/LinkToUser";

export default function renderUpdateInfo() {
  const {rawModelData} = this.props,
    {updated_by, updated_on} = rawModelData,
    updaterId = updated_by.user_id,
    updaterPseudo = updated_by.user_pseudo,
    {user_can_edit: userCanEdit} = rawModelData.obj_info;

  if (!userCanEdit) {
    return (
      <Typography variant='caption'>Ils s'agit de données automatiquement récupérées et non modifiables.</Typography>
    );
  } else {
    const info = <LinkToUser userId={updaterId} pseudo={updaterPseudo}/>;

    if (updated_on) {
      const {date, time} = dateTimeStrToStr(updated_on);
      return (
        <Typography variant='caption'>Mis à jour par {info} le {date} à {time}</Typography>
      );
    } else {
      return (
        <Typography variant='caption'>Mis à jour par {info} à une date inconnue.</Typography>
      );
    }
  }

}


