import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import compose from "recompose/compose";
import { connect } from "react-redux";
import ScholarshipForm from "./common/ScholarshipForm";

import Editor from "../../editor/Editor";
import editorStyle from "../../editor/editorStyle";

import getMapStateToPropsForEditor from "../../editor/getMapStateToPropsForEditor";
import getMapDispatchToPropsForEditor from "../../editor/getMapDispatchToPropsForEditor";
import { withSnackbar } from "notistack";

const styles = theme => ({
  ...editorStyle(theme)
});


class UniversityScholarshipForm extends ScholarshipForm {
  renderLinkedField() {
    return this.renderUniversitiesField();
  }
}

class UniversityScholarshipEditor extends Editor {
  extraFieldMappings = ["university"];

  renderForm() {
    return (
      <UniversityScholarshipForm
        modelData={this.props.rawModelData}
        outsideData={this.props.outsideData}
        ref={this.formRef}
      />
    );
  }
}

UniversityScholarshipEditor.propTypes = {
  outsideData: PropTypes.object.isRequired,
};

export default compose(
  withSnackbar,
  withStyles(styles, { withTheme: true }),
  connect(
    getMapStateToPropsForEditor("universityScholarships"),
    getMapDispatchToPropsForEditor("universityScholarships")
  )
)(UniversityScholarshipEditor);
