import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import compose from "recompose/compose";
import {connect} from "react-redux";

import Editor from "../../editor/Editor";
import Form from "../../form/Form";
import editorStyle from "../../editor/editorStyle";

import getMapStateToPropsForEditor from "../../editor/getMapStateToPropsForEditor";
import getMapDispatchToPropsForEditor from "../../editor/getMapDispatchToPropsForEditor";
import {withSnackbar} from "notistack";

import TextField from "../../form/fields/TextField";

const styles = theme => ({
  ...editorStyle(theme)
});


class UniversityGeneralForm extends Form {
  render() {
    return (
      <>
        <TextField label={"Nom de l'université"}
                   {...this.getReferenceAndValue("name")}
                   required={true}
                   maxLength={200}/>

        <TextField label={"Acronyme de l'université"}
                   {...this.getReferenceAndValue("acronym")}
                   maxLength={20}/>

        <TextField label={"Site internet de l'université"}
                   {...this.getReferenceAndValue("website")}
                   maxLength={300}
                   isUrl={true}/>

        <TextField label={"Logo de l'université"}
                   {...this.getReferenceAndValue("logo")}
                   maxLength={300}
                   isUrl={true}
                   urlExtensions={["jpg", "png", "svg"]}/>
      </>
    );
  }
}


class UniversityGeneralEditor extends Editor {
  renderForm() {
    return <UniversityGeneralForm
      modelData={this.props.rawModelData}
      ref={this.formRef}
    />;
  }
}


export default compose(
  withSnackbar,
  withStyles(styles, {withTheme: true}),
  connect(
    getMapStateToPropsForEditor("universities"),
    getMapDispatchToPropsForEditor("universities")
  )
)(UniversityGeneralEditor);
