import React from "react";
import PropTypes from "prop-types";

import Form from "../../../form/Form";
import FormLevelError from "../../../form/FormLevelError";

const frequencyOptions = [
  {value: "w", label: "Il s'agit du montant hebdomadaire"},
  {value: "m", label: "Il s'agit du montant mensuel"},
  {value: "s", label: "Il s'agit du montant semestriel"},
  {value: "y", label: "Il s'agit du montant annuel"},
  {value: "o", label: "Il s'agit d'un montant donné une seule fois"}
];

class ScholarshipForm extends Form {


  /**
   * @override
   */
  formLevelErrors = [
    new FormLevelError(
      ["amount_min", "amount_max"],
      (amountMin, amountMax) => amountMin !== null && amountMax !== null && amountMax < amountMin,
      "La logique voudrait que la borne supérieure de la bourse soit... supérieure à la borne inférieure."),
  ];

  render() {
    return (
      <>
        {this.renderObjModerationLevelField()}
        {this.renderTitleField()}
        {this.renderLinkedField()}
        {this.renderTextField({
          required: true,
          label: "Présentation succincte",
          maxLength: 200,
          fieldMapping: "short_description",
        })}
        {this.renderCurrencyField({label: "Devise", required: false, fieldMapping: "currency"})}
        {this.renderNumberField({
          label: "Borne inférieure du montant de la bourse",
          minValue: 0,
          fieldMapping: "amount_min",
        })}
        {this.renderNumberField({
          label: "Borne supérieure du montant de la bourse",
          minValue: 0,
          fieldMapping: "amount_max",
        })}
        {this.renderSelectField({
          label: "Fréquence de la bourse",
          options: frequencyOptions,
          fieldMapping: "frequency",
        })}
        {this.renderMarkdownField({
          label: "Autres avantages",
          maxLength: 500,
          formManager: this,
          fieldMapping: "other_advantages",
        })}
        {this.renderCommentField()}
        {this.renderUsefulLinksField()}
      </>
    );
  }
}

ScholarshipForm.propTypes = {
  modelData: PropTypes.object.isRequired,
};

export default ScholarshipForm;
