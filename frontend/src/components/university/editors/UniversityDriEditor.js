import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import compose from "recompose/compose";
import { connect } from "react-redux";

import Editor from "../../editor/Editor";
import Form from "../../form/Form";
import editorStyle from "../../editor/editorStyle";

import getMapStateToPropsForEditor from "../../editor/getMapStateToPropsForEditor";
import getMapDispatchToPropsForEditor from "../../editor/getMapDispatchToPropsForEditor";
import { withSnackbar } from "notistack";

const styles = theme => ({
  ...editorStyle(theme)
});

class UniversityDriForm extends Form {
  render() {
    return (
      <>
        {this.renderTitleField()}
        {this.renderImportanceLevelField()}
        {this.renderUniversitiesField()}
        {this.renderCommentField()}
        {this.renderUsefulLinksField()}
      </>
    );
  }
}

class UniversityDriEditor extends Editor {

  renderForm() {
    return <UniversityDriForm
      modelData={this.props.rawModelData}
      outsideData={this.props.outsideData}
      ref={this.formRef}
    />;
  }
}

UniversityDriEditor.propTypes = {
  outsideData: PropTypes.object.isRequired,
};


export default compose(
  withSnackbar,
  withStyles(styles, { withTheme: true }),
  connect(
    getMapStateToPropsForEditor("universityDri"),
    getMapDispatchToPropsForEditor("universityDri")
  )
)(UniversityDriEditor);
