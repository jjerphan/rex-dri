import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import compose from "recompose/compose";
import {connect} from "react-redux";

import dateStrToDate from "../../../utils/dateStrToDate";

import DateField from "../../form/fields/DateField";

import Editor from "../../editor/Editor";
import Form from "../../form/Form";
import editorStyle from "../../editor/editorStyle";

import getMapStateToPropsForEditor from "../../editor/getMapStateToPropsForEditor";
import getMapDispatchToPropsForEditor from "../../editor/getMapDispatchToPropsForEditor";

import {withSnackbar} from "notistack";
import FormLevelError from "../../form/FormLevelError";


const styles = theme => ({
  ...editorStyle(theme)
});


class UniversitySemestersDatesForm extends Form {

  /**
   * @override
   */
  formLevelErrors = [
    new FormLevelError(
      ["autumn_begin", "autumn_end"],
      (begin, end) => onlyOneIsNull(begin, end),
      "Si une date est saisie pour le semestre d'automne, l'autre doit l'être aussi."),
    new FormLevelError(
      ["spring_begin", "spring_end"],
      (begin, end) => onlyOneIsNull(begin, end),
      "Si une date est saisie pour le semestre de printemps, l'autre doit l'être aussi."),
    new FormLevelError(
      ["autumn_begin", "autumn_end"],
      (begin, end) => begin && end && begin > end,
      "Le début du semestre d'automne doit être antérieur à sa fin..."),
    new FormLevelError(
      ["spring_begin", "spring_end"],
      (begin, end) => begin && end && begin > end,
      "Le début du semestre de printemps doit être antérieur à sa fin..."),
  ];

  render() {
    return (
      <>
        {this.renderObjModerationLevelField()}
        <DateField label={"Date de début du semestre de printemps"}
                   {...this.getReferenceAndValue("spring_begin", dateStrToDate)}
        />
        <DateField label={"Date de fin du semestre de printemps"}
                   {...this.getReferenceAndValue("spring_end", dateStrToDate)}
        />
        <DateField label={"Date de début du semestre d'automne"}
                   {...this.getReferenceAndValue("autumn_begin", dateStrToDate)}
        />
        <DateField label={"Date de fin du semestre d'automne"}
                   {...this.getReferenceAndValue("autumn_end", dateStrToDate)}
        />
        {this.renderCommentField()}
        {this.renderUsefulLinksField()}
      </>
    );
  }
}


function onlyOneIsNull(a, b) {
  return (a == null && b != null) || (b == null && a != null);
}

class UniversitySemestersDatesEditor extends Editor {
  extraFieldMappings = ["university"];

  renderForm() {
    return (
      <UniversitySemestersDatesForm
        modelData={this.props.rawModelData}
        ref={this.formRef}
      />
    );
  }
}


export default compose(
  withSnackbar,
  withStyles(styles, {withTheme: true}),
  connect(
    getMapStateToPropsForEditor("universitiesSemestersDates"),
    getMapDispatchToPropsForEditor("universitiesSemestersDates")
  )
)(UniversitySemestersDatesEditor);
