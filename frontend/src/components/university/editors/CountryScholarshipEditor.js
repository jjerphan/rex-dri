import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import compose from "recompose/compose";
import { connect } from "react-redux";

import ScholarshipForm from "./common/ScholarshipForm";
import Editor from "../../editor/Editor";
import editorStyle from "../../editor/editorStyle";

import getMapStateToPropsForEditor from "../../editor/getMapStateToPropsForEditor";
import getMapDispatchToPropsForEditor from "../../editor/getMapDispatchToPropsForEditor";
import { withSnackbar } from "notistack";


const styles = theme => ({
  ...editorStyle(theme)
});


class CountryScholarshipForm extends ScholarshipForm {
  renderLinkedField() {
    return this.renderCountriesField();
  }
}

class CountryScholarshipEditor extends Editor {
  renderForm() {
    return (
      <CountryScholarshipForm
        modelData={this.props.rawModelData}
        outsideData={this.props.outsideData}
        ref={this.formRef}
      />
    );
  }
}

CountryScholarshipEditor.propTypes = {
  outsideData: PropTypes.object.isRequired,
};

export default compose(
  withSnackbar,
  withStyles(styles, { withTheme: true }),
  connect(
    getMapStateToPropsForEditor("countryScholarships"),
    getMapDispatchToPropsForEditor("countryScholarships")
  )
)(CountryScholarshipEditor);
