// Inspired by from https://github.com/mui-org/material-ui/blob/master/docs/src/pages/style/color/ColorTool.js

import React from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import {rgbToHex} from "@material-ui/core/styles/colorManipulator";
import ColorDemo from "./ColorDemo";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";

import CustomComponentForAPI from "../../common/CustomComponentForAPI";


import getActions from "../../../redux/api/getActions";
import {compose} from "recompose";
import Input from "@material-ui/core/Input";
import {getLatestRead} from "../../../redux/api/utils";
import SaveButton from "../../common/SaveButton";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import isEqual from "lodash/isEqual";
import defaultSiteTheme from "../../../config/defaultTheme.json";
import Divider from "@material-ui/core/Divider";
import TextLink from "../../common/TextLink";
import {deepCopy} from "../../../utils/deepCopy";
import {RequestParams} from "../../../redux/api/RequestParams";
import {getTheme} from "../../common/theme/utils";

const isRgb = string => /#?([0-9a-f]{6})/i.test(string);

/**
 * Component to handle website color customization
 *
 * @class ColorTool
 * @extends {CustomComponentForAPI}
 * @extends React.Component
 */
class ColorTool extends CustomComponentForAPI {

  constructor(props) {
    super(props);

    const theme = this.getInitialTheme();
    this.state = this.getStateForTheme(theme);
  }

  /**
   * Get the theme from the server
   * @returns {object}
   */
  getInitialTheme() {
    return deepCopy(getLatestRead(this.props.userData).data.theme);
  }

  /**
   * Map a theme (from the server) to the state like object
   *
   * @param theme
   * @returns {object}
   */
  getStateForTheme(theme) {
    return {
      theme,
      "light-primary-input": theme.light.primary,
      "light-secondary-input": theme.light.secondary,
      "dark-primary-input": theme.dark.primary,
      "dark-secondary-input": theme.dark.secondary,
    };
  }

  /**
   * Shortcu to the set the theme
   * @param newTheme
   */
  updateStateTheme(newTheme) {
    this.setState({theme: newTheme});
  }

  /**
   * Handler to change the night mode
   */
  handleChangeNightMode() {
    const oldTheme = deepCopy(this.state.theme),
      newTheme = Object.assign(oldTheme, {mode: oldTheme.mode === "light" ? "dark" : "light"});
    this.updateStateTheme(newTheme);
  }

  /**
   * Handler to take into account changes from color inputs
   * @param intent
   * @returns {Function}
   */
  handleChangeColor = intent => event => {
    const oldTheme = this.state.theme;
    const {target: {value: color}} = event;

    this.setState({
      [`${oldTheme.mode}-${intent}-input`]: color,
    });

    if (isRgb(color)) {
      let newTheme = Object.assign({}, oldTheme);
      oldTheme.mode === "light" ?
        newTheme.light[intent] = color
        :
        newTheme.dark[intent] = color;
      this.setState({theme: newTheme});
    }
  };

  /**
   * Restores the default theme of the app.
   * Doesn't change the night mode status.
   * Restores only the colors for the current night mode.
   */
  handleRestoreDefault() {
    let newTheme = deepCopy(defaultSiteTheme);
    const requestedMode = this.state.theme.mode,
      otherMode = requestedMode === "light" ? "dark" : "light";
    newTheme.mode = requestedMode;
    newTheme[otherMode] = this.state.theme[otherMode];
    this.setState(this.getStateForTheme(newTheme));
  }

  /**
   * Save the new theme on the server
   */
  handleSendToServer() {
    const userData = deepCopy(this.getLatestReadData("userData")),
      newUserData = Object.assign(userData, {theme: this.state.theme});
    this.props.saveUserDataToServer(newUserData);
  }

  /**
   *
   *
   * RENDERERS
   *
   *
   */

  /**
   * Renders A grid of the three color variants
   * @param {string} color
   */
  renderColorBar(color) {
    const {classes} = this.props,
      {augmentColor, getContrastText} = this.props.theme.palette,
      background = augmentColor({main: color});

    return (
      <Grid container className={classes.colorBar}>
        {["dark", "main", "light"].map(key => (
          <div
            className={classes.colorSquare}
            style={{backgroundColor: background[key]}}
            key={key}
          >
            <Typography
              variant="caption"
              style={{color: getContrastText(background[key])}}
            >
              {rgbToHex(background[key])}
            </Typography>
          </div>
        ))}
      </Grid>
    );
  }

  /**
   * Renders a color picker.
   *
   * @param {"primary"|"secondary"} intent
   */
  renderColorPicker(intent) {
    const requestedMode = this.state.theme.mode,
      inputValue = this.state[`${requestedMode}-${intent}-input`],
      color = this.state.theme[requestedMode][intent],
      caption = intent === "primary" ? "Couleur primaire" : "Couleur secondaire";

    return (
      <>
        <Typography gutterBottom variant="h6">
          {caption}
        </Typography>
        <Input
          id={intent}
          value={inputValue}
          onChange={(e) => this.handleChangeColor(intent)(e)}
          type="color"
          fullWidth
        />
        {this.renderColorBar(color)}
      </>
    );
  }

  /**
   * Renders the advanced configuration parts.
   */
  renderAdvancedSettings() {
    const {classes} = this.props;
    return (
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
          <Typography>Réglages avancés des couleurs</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails style={{display: "block"}}>

          <Typography variant={"caption"}>
            Ci-dessous, vous pouvez choisir les couleurs dominantes du site.
            Celles-ci dépendent de l'état d'activation du « mode nuit ».
            Nous vous invitons à utiliser <TextLink href="https://material.io/tools/color/">ce site</TextLink> pour
            guider votre choix.
            À tout moment, vous pouvez rétablir le thème par défaut (pour le «mode nuit » en cours)
            du site en cliquant ci-dessous.
          </Typography>

          <Grid container spacing={2}>
            {
              ["primary", "secondary"].map((intent, idx) => (
                <Grid item xs={6} key={idx}>
                  {this.renderColorPicker(intent)}
                </Grid>
              ))
            }
          </Grid>

          <Divider variant="fullWidth" className={classes.divider}/>

          <div style={{width: "100%"}}>
            <Button variant="contained"
                    color="primary"
                    onClick={() => this.handleRestoreDefault()}
                    className={classes.centered}
                    style={{display: "block"}}>
              Rétablir le thème par défaut
            </Button>
          </div>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }


  customRender() {
    const {classes} = this.props,
      themeForDemo = getTheme(this.state.theme),
      hasChanges = !isEqual(this.state.theme, this.getInitialTheme()),
      darkModeActivated = this.state.theme.mode === "dark";

    return (
      <>
        <Typography variant="h6">
          Réglages
        </Typography>
        <FormControlLabel
          labelPlacement='start'
          label={!darkModeActivated ? "Activer le monde nuit : " : "Désactiver le mode nuit :"}
          control={
            <Switch
              checked={darkModeActivated}
              onChange={() => this.handleChangeNightMode()}
              value="osef"
            />
          }
        />
        {this.renderAdvancedSettings()}

        <Divider variant="fullWidth" className={classes.divider}/>

        <Typography variant={"h6"}>Prévisualisation</Typography>
        <MuiThemeProvider theme={themeForDemo}>
          <ColorDemo/>
        </MuiThemeProvider>

        <Divider variant="fullWidth" className={classes.divider}/>

        <SaveButton label={"Enregistrer et appliquer au site"}
                    successLabel={"Enregistré"}
                    disabled={false}
                    handleSaveRequested={() => this.handleSendToServer()}
                    success={!hasChanges} extraRootClass={classes.centered}/>
      </>
    );
  }
}

ColorTool.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  saveUserDataToServer: PropTypes.func.isRequired,
  userData: PropTypes.object.isRequired,
};


const mapStateToProps = (state) => {
  return {
    userData: state.api.userDataOne
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    // eslint-disable-next-line no-undef
    saveUserDataToServer: (data) => dispatch(getActions("userData").update(RequestParams.Builder.withId(__AppUserId).withData(data).build())),
    api: {
      // __AppUserId is defined in the html rendered by django
      // eslint-disable-next-line no-undef
      userData: () => dispatch(getActions("userData").readOne(RequestParams.Builder.withId(__AppUserId).build())), // id not needed userData
    },
  };
};


const styles = theme => ({
  colorBar: {
    marginTop: theme.spacing(2),
  },
  colorSquare: {
    width: 64,
    height: 64,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  centered: {
    margin: "0 auto",
  },
  divider: {
    margin: theme.spacing(3),
  }
});


export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withStyles(styles, {withTheme: true})
)(ColorTool);
