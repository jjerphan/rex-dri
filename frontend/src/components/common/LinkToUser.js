import CustomLink from "./CustomLink";
import {APP_ROUTES} from "../../config/appRoutes";
import React from "react";
import PropTypes from "prop-types";

export default function LinkToUser(props) {
  return <CustomLink to={APP_ROUTES.forUser(props.userId)}><em>{props.pseudo} (#{props.userId})</em></CustomLink>;
}

LinkToUser.propTypes = {
  userId: PropTypes.number.isRequired,
  pseudo: PropTypes.string.isRequired,
};
