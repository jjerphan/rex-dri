import React from "react";
import PropTypes from "prop-types";

import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import {connect} from "react-redux";
import CustomComponentForAPI from "../CustomComponentForAPI";
import {getTheme, updatePhoneStatusBarColor} from "./utils";
import getActions from "../../../redux/api/getActions";
import {RequestParams} from "../../../redux/api/RequestParams";


/**
 * Component that handles theme customization and saving at the scale of the entire app
 *
 * @class ThemeProvider
 * @extends {CustomComponentForAPI}
 * @extends React.Component
 *
 */
class ThemeProvider extends CustomComponentForAPI {
  customRender() {
    const {userData} = this.getAllLatestReadData("userData"),
      themeData = userData.theme,
      theme = getTheme(themeData);

    updatePhoneStatusBarColor(theme.palette.primary.main);

    return (
      <MuiThemeProvider theme={theme}>
        {this.props.children}
      </MuiThemeProvider>
    );
  }
}

ThemeProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

const mapStateToProps = (state) => {
  return {
    userData: state.api.userDataOne
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    api: {
      // __AppUserId is defined in the html rendered by django
      // eslint-disable-next-line no-undef
      userData: () => dispatch(getActions("userData").readOne(RequestParams.Builder.withId(__AppUserId).build())),
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ThemeProvider);
