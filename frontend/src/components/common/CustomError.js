/**
 * Class to handle app errors (such as form errors) in a nice wrapped way
 * It's for non fatal errors.
 *
 * @export
 * @class CustomError
 */
export default class CustomError {
  /**
   *Creates an instance of CustomError.
   *
   * @param {Array[string] = []} messages
   * @memberof CustomError
   */
  constructor(messages = []) {
    this.messages = messages;
    this.status = messages.length > 0;
  }

  /**
   * Method to combine to error class
   *
   * @param {CustomError} other
   * @returns {CustomError}
   * @memberof CustomError
   */
  combine(other) {
    return new CustomError(Array.prototype.concat(this.messages, other.messages));
  }

  /**
   * Combines an array of CustomError
   *
   * @static
   * @param {Array[CustomError]} arrOfCustomErrors
   * @returns
   * @memberof CustomError
   */
  static superCombine(arrOfCustomErrors) {
    return new CustomError(
      Array.prototype.concat(
        arrOfCustomErrors
          .flatMap(el => el.messages)));
  }
}
