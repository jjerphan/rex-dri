import PropTypes from "prop-types";
import Menu from "@material-ui/core/Menu";
import React from "react";
import MenuItem from "@material-ui/core/MenuItem";

/**
 * Component to render a simple popup menu
 * @param props
 * @returns {*}
 * @constructor
 */
function SimplePopupMenu(props) {
  const [anchorEl, setAnchorEl] = React.useState(null);

  function handleClick(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  return (
    <>
      {
        props.renderHolder({onClick: (e) => handleClick(e)})
      }
      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {
          props.items.map(({label, onClick}, key) => (
            <MenuItem key={key} onClick={() => {
              onClick();
              handleClose();
            }}>
              {label}
            </MenuItem>
          ))
        }
      </Menu>
    </>
  );
}


SimplePopupMenu.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
  })).isRequired,
  renderHolder: PropTypes.func.isRequired,
};

export default SimplePopupMenu;