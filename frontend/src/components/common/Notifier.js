import React, { Component } from "react";
import { withSnackbar } from "notistack";
import PropTypes from "prop-types";

/**
 * Dumb component to display a one shot notification:
 * notification will be created as the component is added to the DOM.
 *
 * @class Notifier
 * @extends {Component}
 */
class Notifier extends Component {
  constructor(props) {
    super(props);

    props.enqueueSnackbar(props.message, props.options);
  }

  render() {
    return <></>;
  }

}

Notifier.defaultProps = {
  options: { variant: "error", autoHideDuration: 5000 }
};

Notifier.propTypes = {
  enqueueSnackbar: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired,
  options: PropTypes.object,
};

export default withSnackbar(Notifier);
