import React, {Component} from "react";
import Loading from "./Loading";
import PropTypes from "prop-types";
import {getLatestRead, successActionsWithReads} from "../../redux/api/utils";
import Notifier from "./Notifier";
import {RequestParams} from "../../redux/api/RequestParams";


/**
 * Custom react component to be used when called to the api are required to display data of the component.
 *
 * **When extending, you shouldn't use the `render` function but the `customRender` (that will be called once all the data is ready)**
 *
 * See here for more info on how to use it: https://rex-dri.gitlab.utc.fr/rex-dri/documentation/#/Application/Frontend/redux?id=conventions-for-reading-data
 * And get inspired by the code base ;)
 *
 * @class CustomComponentForAPI
 * @extends {Component}
 * @abstract
 */
class CustomComponentForAPI extends Component {

  /**
   * A mapping from propName to a function that returns the appropriate RequestParams instance.
   * This functions takes as argument the props and the state of the component.
   * @type {object.<string, function({props, state}):RequestParams>}
   */
  apiParams = {};

  /**
   * Should 'smartRefresh' (based on the comparaison of the previous RequestParam obj and what it would look like
   * now) be enabled?
   * @type {boolean}
   */
  enableSmartDataRefreshOnComponentDidUpdate = false;

  /**
   * a little bit of optimization, Stores the list of props that are "automatically" connected to the API
   * @type {string[]}
   * @private
   */
  _apiAutoPropNames = Array();

  /**
   * Stores the last requestParams intances used for each props
   * @type {object.<string,RequestParams>}
   * @private
   */
  _lastRequestParamsObjs = {};

  constructor(props) {
    super(props);

    if (typeof props === "object" && "api" in props) {
      const {api} = props;
      this._apiAutoPropNames = Object.keys(api);
    }

    /**
     * configuration checks
     */
    // eslint-disable-next-line no-undef
    if (process.env.NODE_ENV !== "production") {
      Object.entries(this.apiParams).forEach(([propName, mapper]) => {
        if (typeof props[propName] === "undefined") {
          throw new Error("Miss-configuration: " +
            `The propName ${propName} you defined in apiParams doesn't exist in the props ` +
            "See: https://rex-dri.gitlab.utc.fr/rex-dri/documentation/#/Application/Frontend/redux?id=dynamic-parametrization-of-the-requests");
        }
        if (typeof props.api[propName] !== "function") {
          throw new Error("Miss-configuration: " +
            `The propName ${propName}: 'props.api.${propName}' must be a function (with a transfer parameter 'params') ` +
            "See: https://rex-dri.gitlab.utc.fr/rex-dri/documentation/#/Application/Frontend/redux?id=dynamic-parametrization-of-the-requests");
        }
        if (typeof mapper !== "function") {
          throw new Error("Miss-configuration: " +
            `apiParams.'${propName}' must be a function. ` +
            "See: https://rex-dri.gitlab.utc.fr/rex-dri/documentation/#/Application/Frontend/redux?id=dynamic-parametrization-of-the-requests");
        }
        if (!(mapper({props, state: this.state}) instanceof RequestParams)) {
          throw new Error("Miss-configuration: " +
            `The function defined in apiParams.${propName} must return an instance of RequestParams. ` +
            "See: https://rex-dri.gitlab.utc.fr/rex-dri/documentation/#/Application/Frontend/redux?id=dynamic-parametrization-of-the-requests");
        }
      });
    }
    /**
     * End of configuration checks
     */

    // Finally we try to restore the previous RequestParams objects
    // So that we don't refetch the same data twice.
    this._apiAutoPropNames.forEach(propName => {
      this._lastRequestParamsObjs[propName] = props[propName].readSucceeded.requestParams;
    });
  }

  /**
   * @private
   */
  smartRefresh() {
    Object.entries(this.apiParams).forEach(([propName, mapper]) => {
      if (!mapper({props: this.props, state: this.state})
        .equals(this._lastRequestParamsObjs[propName])) {
        this.performReadFromApi(propName);
      }
    });
  }

  /////
  // React default functions override for uniform custom handling
  /////
  componentDidMount() {
    this.readPropsIfNeeded();
    // auto refetch data if outdated
    this.smartRefresh();
  }

  // eslint-disable-next-line no-unused-vars
  componentDidUpdate(prevProps, prevState, snapshot) {
    // extends the default behavior of react to load props (api related) if needed on update.
    this.readPropsIfNeeded();
    if (this.enableSmartDataRefreshOnComponentDidUpdate) {
      // auto refetch data if outdated
      this.smartRefresh();
    }
  }

  render() {
    // Override of the default react render behavior to wait for data to arrive.
    // You should use `customRender` instead of `render` in your components.
    if (this.checkPropsFailed()) {
      return (
        <>
          <Notifier
            message={"Une erreur est survenue sur le serveur. Merci de recharger le page."}
          />
          <p>Une erreur est survenue lors du téléchargement des données. Merci de recharger la page et si l'erreur
            persiste, merci de contacter les administrateurs du site.</p>
        </>
      );
    }

    if (!this.allApiDataIsReady()) {
      return <Loading/>;
    }

    return this.customRender();
  }

  /**
   * Function to use instead of `render`.
   *
   * @virtual
   * @memberof CustomComponentForAPI
   */
  customRender() {
    // eslint-disable-next-line no-console
    throw new Error("Dev: you forget to define the `customRender` function that is used when rendering within a subClass of CustomComponentForAPI");
  }

  // End of react functions override


  /**
   * Check that none of the API props have failed during reading
   *
   * @returns {boolean}
   * @memberof CustomComponentForAPI
   */
  checkPropsFailed() {
    return this._apiAutoPropNames.some((propName) => {
      if (typeof this.props[propName] === "undefined") {
        // eslint-disable-next-line no-console
        console.error(this.props, propName);
        throw Error(`${propName} is not in the class props (or might be undefined due to a wrong state extract) ! Dev, check what your are doing`);
      }
      const prop = this.props[propName];
      return prop.readFailed.failed; // general handling of all types of API reducers
    });
  }


  /**
   * Check that the prop `propName` has been read from the server
   *
   * @param {string} propName
   * @returns {boolean}
   * @memberof CustomComponentForAPI
   */
  propIsUsable(propName) {
    const prop = this.props[propName];
    return (!prop.isInvalidated)
      && successActionsWithReads
        .filter(action => action in prop) // general handling of all types of API reducers
        .some(action => prop[action].readAt !== 0) // makes sure will consider all success actions
      && ["isReading"]//, "isUpdating", "isCreating"] Don't put those in here it may cause unwanted rerendering whole tree when saving
        .filter(action => action in prop)
        .every(action => prop[action] === false);
  }

  /**
   * Get the list of props for which a read action needs to be triggered
   *
   * @returns {array.<string>}
   * @memberof CustomComponentForAPI
   */
  getListPropsNeedRead() {
    return this._apiAutoPropNames.filter((propName) => {
      if (this.propIsUsable(propName)) {
        return false;
      } else {
        const prop = this.props[propName];
        return !prop.readFailed.failed && !prop.isReading;  // general handling of all types of API reducers
      }
    });
  }

  /**
   * Read all the props from api that need to be read.
   *
   * @memberof CustomComponentForAPI
   */
  readPropsIfNeeded() {
    this.getListPropsNeedRead().forEach((propName) => {
      this.performReadFromApi(propName);
    });
  }

  /**
   * Read the prop corresponding to `propName` from the API.
   *
   * @param {string} propName
   * @memberof CustomComponentForAPI
   */
  performReadFromApi(propName) {
    if (propName in this.apiParams) {
      const requestParams = this.apiParams[propName]({props: this.props, state: this.state});
      this._lastRequestParamsObjs[propName] = requestParams;
      this.props.api[propName](requestParams);
    } else {
      this.props.api[propName]();
    }
  }

  /**
   * Checks that all the data from the API has been read
   *
   * @returns
   * @memberof CustomComponentForAPI
   */
  allApiDataIsReady() {
    return this._apiAutoPropNames.every((propName) => this.propIsUsable(propName));
  }

  /**
   * Get the latest data that was read from the api given the `propName` and the time at which is was read
   * read, create and update are taken into account
   *
   * @param {string} propName
   * @returns {object}
   * @memberof CustomComponentForAPI
   */
  getLatestReadDataAndTime(propName) {
    const prop = this.props[propName],
      out = getLatestRead(prop);

    if (!("data" in out)) {
      throw Error(`No read data from the api could be retrieved for: ${propName}`);
    } else {
      return out;
    }
  }


  /**
   * Get the latest data that was read from the api given the `propName`
   *  read, create and update are taken into account
   *
   * @param {string} propName
   * @returns {object}
   * @memberof CustomComponentForAPI
   */
  getLatestReadData(propName) {
    return this.getLatestReadDataAndTime(propName).data;
  }

  /**
   * In some very rare case, we need to consider only the data that was read and not create ou updated.
   * That what this function does.
   *
   * @param {string} propName
   * @returns
   * @memberof CustomComponentForAPI
   */
  getOnlyReadData(propName) {
    return this.props[propName].readSucceeded.data;
  }

  /**
   * Get the latest time at which the latest data from the api corresponding to `propName` was read
   *
   * read, create and update are taken into account
   *
   * @param {string} propName
   * @returns {number}
   * @memberof CustomComponentForAPI
   */
  getLatestReadTime(propName) {
    return this.getLatestReadDataAndTime(propName).readAt;
  }

  /**
   * Access to all the latest data loaded from the api given the props
   *
   * read, create and update are taken into account
   *
   * @returns {object}
   * @memberof CustomComponentForAPI
   */
  getAllLatestReadData() {
    let out = {};
    this._apiAutoPropNames.forEach((propName) => {
      out[propName] = this.getLatestReadData(propName);
    });
    return out;
  }

  /**
   * Access to the latest read data from the propNames array
   * This should be used instead of getAllLatestReadData to optimize things
   *
   * read, create and update are taken into account
   *
   * @param {array} propNames Array of the prop names you want to read data from
   * @returns {object}
   * @memberof CustomComponentForAPI
   */
  getLatestReadDataFor(propNames) {
    let out = {};
    propNames.forEach(propName => out[propName] = this.getLatestReadData(propName));
    return out;
  }


  // utilities functions shared by subclasses

  /**
   * Function to retrieve all the information relative to a campus
   *
   * @param {object} campus instance of Campus
   * @returns {object} Campus with replaced university, city and country with the matching instance
   * @memberof CustomComponentForAPI
   */
  joinCampus(campus) {
    const {universities, countries, cities} = this.getLatestReadDataFor(["universities", "countries", "cities"]);
    let res = Object.assign({}, campus); //copy for safety
    res.university = universities.find(univ => univ.id == campus.university);
    res.city = cities.find(city => city.id == campus.city);
    res.country = countries.find(country => country.id == res.city.country);
    return res;
  }


  /**
   * Function to get the city and the country of a university given a university id
   *
   * @param {number} univId
   * @returns {object} Object with city and country instance of the university (main campus)
   * @memberof CustomComponentForAPI
   */
  getUnivCityAndCountry(univId) {
    const univMainCampus = this.findMainCampus(univId),
      {countries, cities} = this.getLatestReadDataFor(["countries", "cities"]),
      city = cities.find(city => city.id == univMainCampus.city),
      country = countries.find(country => country.id == city.country);
    return {city, country};
  }

  /**
   * Function that returns the main campus instance associated with a university
   * identified by its id.
   *
   * @param {number} univId
   * @returns {object}
   * @memberof CustomComponentForAPI
   */
  findMainCampus(univId) {
    const mainCampuses = this.getLatestReadData("mainCampuses");
    return mainCampuses.find(campus => campus.university == univId);
  }
}

CustomComponentForAPI.propTypes = {
  api: PropTypes.objectOf(PropTypes.func).isRequired, // should be given through redux connect of `mapDispatchToProps`
};

export default CustomComponentForAPI;
