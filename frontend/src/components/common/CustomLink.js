import {Link} from "react-router-dom";
import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import {styles} from "./TextLink";

/**
 * Renders an app internal link with correct styling.
 */
class CustomLink extends React.Component {
  render() {
    const {classes, to, children} = this.props;
    return (
      <Link to={to} className={classes.link} style={{textDecoration: "none"}}>
        {children}
      </Link>
    );
  }
}

CustomLink.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.node,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles, {withTheme: true})(CustomLink);