// Inspired by : https://material-ui.com/demos/autocomplete/

import React from "react";
import PropTypes from "prop-types";
import keycode from "keycode";
import Downshift from "downshift";
import withStyles from "@material-ui/core/styles/withStyles";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import MenuItem from "@material-ui/core/MenuItem";
import Chip from "@material-ui/core/Chip";
import fuzzysort from "fuzzysort";


function renderInput(inputProps, autoFocus) {
  const {InputProps, classes, ref, ...other} = inputProps;

  return (
    <TextField
      autoFocus={autoFocus}
      InputProps={{
        inputRef: ref,
        classes: {root: classes.inputRoot},
        ...InputProps,
      }}
      {...other}
    />
  );
}

function renderSuggestion({suggestion, index, itemProps, highlightedIndex, selectedItem}) {
  const isHighlighted = highlightedIndex === index;
  const isSelected = (selectedItem || "").indexOf(suggestion) > -1;

  return (
    <MenuItem
      {...itemProps}
      key={suggestion.value}
      selected={isHighlighted}
      component="div"
      style={{
        fontWeight: isSelected ? 500 : 400,
      }}
    >
      {suggestion.label}
    </MenuItem>
  );
}

renderSuggestion.propTypes = {
  highlightedIndex: PropTypes.number,
  index: PropTypes.number,
  itemProps: PropTypes.object,
  selectedItem: PropTypes.string,
  suggestion: PropTypes.shape({label: PropTypes.string, value: PropTypes.number}).isRequired,
};

class DownshiftMultiple extends React.Component {

  static CACHE = {};

  getFinalOptions() {
    return this.props.options.map(({value, label}) => ({value, label, valueStr: value.toString()}));
  }

  // holds the mapping from value to label of all options
  // options cannot be changed once the component in mounted
  reverseOptions = new Map();

  constructor(props) {
    super(props);
    const {value, inputValue} = props;
    this.state = {value: [...value], inputValue, options: this.getFinalOptions()};
    this.reverseOptions = new Map();
  }

  componentDidMount() {
    const {options} = this.props;
    this.reverseOptions = new Map();
    options.forEach(el => this.reverseOptions.set(el.value, el.label));

    let proposal = {value: this.props.value, inputValue: this.props.inputValue};
    const {cacheId} = this.props;
    if (cacheId !== null && DownshiftMultiple.CACHE[cacheId]) {
      Object.assign(proposal, DownshiftMultiple.CACHE[cacheId]);
    }

    this.setState({value: [...proposal.value], inputValue: proposal.inputValue, options: this.getFinalOptions()});
  }

  componentWillUnmount() {
    const {cacheId} = this.props;
    if (cacheId !== null) {
      const {value, inputValue} = this.state;
      DownshiftMultiple.CACHE[cacheId] = {value, inputValue};
    }
  }


  getSuggestions(inputValue) {
    const {value, options} = this.state;
    let possible = options.filter(el => !value.includes(el.value));

    const filter = fuzzysort.go(inputValue, possible, {limit: 5, keys: ["label", "valueStr"]});
    if (filter.length > 0) {
      return filter.map(item => item.obj);
    } else {
      return possible.slice(0, 4);
    }
  }

  handleKeyDown = event => {
    const {inputValue, value} = this.state;
    if (value.length && !inputValue.length && keycode(event) === "backspace") {
      this.setState({
        value: value.slice(0, value.length - 1),
      });
    }
  };

  handleInputChange = event => {
    this.setState({inputValue: event.target.value});
  };

  handleChange = val => {
    let {value} = this.state;
    if (value.indexOf(val) === -1) {
      value = [...value, val];
      if (!this.props.multiple) {
        value = value.length === 0 ? [] : [value[value.length - 1]];
      }
      // Tell subscriber
      this.onChange(value);
    }


    this.setState({
      inputValue: "",
      value,
    });
  };

  handleDelete = val => () => {
    this.setState(state => {
      const value = [...state.value];
      value.splice(value.indexOf(val), 1);
      this.onChange(value);
      return {value};
    });
  };

  onChange(value) {
    if (this.props.multiple) {
      this.props.onChange(value);
    } else {
      // we return only one value or null
      this.props.onChange(value.length === 0 ? null : value[0]);
    }
  }

  render() {
    const {classes, fieldLabel, fieldPlaceholder, autoFocusInput} = this.props,
      {inputValue, value} = this.state;

    return (
      <div className={classes.root}>
        <Downshift inputValue={inputValue}
                   onChange={this.handleChange}>
          {
            ({getInputProps, getItemProps, isOpen, inputValue: inputValue2, selectedItem: selectedItem2, highlightedIndex,}) =>
              (
                <div className={classes.container}>
                  {renderInput({
                    fullWidth: true,
                    classes,
                    InputProps: getInputProps({
                      startAdornment: value.map(val => (
                        <Chip
                          key={val}
                          tabIndex={-1}
                          label={this.reverseOptions.get(val)}
                          className={classes.chip}
                          onDelete={this.handleDelete(val)}
                          variant="outlined"
                          color="primary"
                        />
                      )),
                      onChange: this.handleInputChange,
                      onKeyDown: this.handleKeyDown,
                      placeholder: fieldPlaceholder,
                    }),
                    label: fieldLabel,
                  }, autoFocusInput)}
                  {isOpen ? (
                    <Paper className={classes.paper} square>
                      {this.getSuggestions(inputValue2).map((suggestion, index) =>
                        renderSuggestion({
                          suggestion,
                          index,
                          itemProps: getItemProps({item: suggestion.value}),
                          highlightedIndex,
                          selectedItem: selectedItem2,
                        }),
                      )}
                    </Paper>
                  ) : null}
                </div>
              )
          }
        </Downshift>
      </div>
    );
  }
}

DownshiftMultiple.propTypes = {
  classes: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.arrayOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  ).isRequired, // only used on mount // the initial value must be provided as an array,
  inputValue: PropTypes.string.isRequired, // only used on mount
  autoFocusInput: PropTypes.bool.isRequired,
  fieldLabel: PropTypes.string.isRequired,
  fieldPlaceholder: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  })).isRequired,
  multiple: PropTypes.bool.isRequired, // do we allow multiple values to be selected.
  cacheId: PropTypes.string, // if not null enable cachig of value and input Value
  // If multiple is true than null or one value in returned
};

DownshiftMultiple.defaultProps = {
  autoFocusInput: false,
  fieldLabel: "",
  fieldPlaceholder: "Placeholder",
  multiple: true,
  value: [],
  inputValue: "",
  cacheId: null,
};

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  container: {
    flexGrow: 1,
    position: "relative",
  },
  paper: {
    position: "absolute",
    zIndex: 200000,
    marginTop: theme.spacing(1),
    left: 0,
    right: 0,
  },
  chip: {
    margin: theme.spacing(0.5, 0.25),
  },
  inputRoot: {
    flexWrap: "wrap",
  }
});

export default withStyles(styles)(DownshiftMultiple);
