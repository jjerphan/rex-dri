import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import CircularProgress from "@material-ui/core/CircularProgress";


/**
 * React component to display a loading indicator
 *
 * @class Loading
 * @extends {React.Component}
 */
class Loading extends React.Component {
  timer = null;

  state = {
    completionPercentage: 0,
  };

  componentDidMount() {
    this.timer = setInterval(this.progress.bind(this), 20);
  }

  componentWillUnmount() {
    // Delete the intervaler on unmount
    clearInterval(this.timer);
  }


  /**
   * Make circular progress turn on its self in conjunction with componentDidMount
   *
   * @memberof Loading
   */
  progress() {
    const { completionPercentage } = this.state;
    this.setState({
      completionPercentage: completionPercentage >= 100 ? 0 : completionPercentage + 1
    });
  }

  render() {
    const { classes, size } = this.props,
      { completionPercentage } = this.state;

    return (
      <CircularProgress
        className={classes.progress}
        variant="determinate"
        size={size}
        value={completionPercentage}
      />
    );
  }
}

Loading.propTypes = {
  classes: PropTypes.object.isRequired,
  size: PropTypes.number.isRequired
};

Loading.defaultProps = {
  size: 50
};

const styles = theme => ({
  progress: {
    margin: theme.spacing(2),
  }
});

export default withStyles(styles)(Loading);
