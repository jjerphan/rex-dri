import Tooltip from "@material-ui/core/Tooltip";
import React, {useState} from "react";
import clipboardCopy from "../../utils/clipboardCopy";
import PropTypes from "prop-types";

/**
 * Render prop component that wraps element in a Tooltip that shows "Copied to clipboard!" when the
 * copy function is invoked
 */
function CopyToClipboard(props) {
  const [showToolTip, setShowTooltip] = useState(false);

  return (
    <Tooltip
      open={showToolTip}
      title={props.message}
      leaveDelay={1500}
      onClose={() => setShowTooltip(false)}

    >
      {props.render((text) => {
        clipboardCopy(text);
        setShowTooltip(true);
      })}
    </Tooltip>
  );
}

CopyToClipboard.propTypes = {
  render: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired,
};

CopyToClipboard.defaultProps = {
  message: "Lien copié 😁",
};


export default React.memo(CopyToClipboard);
