import React from "react";
import Menu from "@material-ui/core/Menu";
import PropTypes from "prop-types";
import Fab from "@material-ui/core/Fab";
import {MenuItem} from "@material-ui/core";
import {NavLink} from "react-router-dom";

/**
 * There were errors during migrations to material-ui v4 as Navlink couldn't hold a ref...
 */
// eslint-disable-next-line react/display-name
const ForwardedNavLink = React.forwardRef((props, ref)=> <div ref={ref}><NavLink {...props}/></div>);

class IconWithMenu extends React.Component {
  state = {
    anchorEl: null,
  };

  handleOpenMenu = event => {
    this.setState({anchorEl: event.currentTarget});
  };

  handleCloseMenu = () => {
    this.setState({anchorEl: null});
  };

  render() {
    const {anchorEl} = this.state;
    const open = Boolean(anchorEl);
    return (
      <>
        <Fab size="medium" color="inherit" onClick={this.handleOpenMenu} {...this.props.fabProps}>
          {<this.props.Icon {...this.props.iconProps}/>}
        </Fab>
        <Menu anchorEl={anchorEl}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={open}
              onClose={this.handleCloseMenu}>
          {this.props.menuItems.map(
            ({label, route, hardRedirect}, idx) =>
              hardRedirect ?
                <MenuItem key={idx}
                          component="a"
                          href={route}
                          onClick={this.handleCloseMenu}>
                  {label}
                </MenuItem>
                :
                <MenuItem key={idx}
                          component={ForwardedNavLink}
                          to={route}
                          onClick={this.handleCloseMenu}>
                  {label}
                </MenuItem>
          )}
        </Menu>
      </>
    );
  }

}

IconWithMenu.defaultProps = {
  fabProps: {},
  iconProps: {},
};

IconWithMenu.propTypes = {
  Icon: PropTypes.object.isRequired, // should be a react component
  menuItems: PropTypes.array.isRequired, // should be an array of menu items
  fabProps: PropTypes.object,
  iconProps: PropTypes.object,
};

export default IconWithMenu;