import React from "react";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

import withStyles from "@material-ui/core/styles/withStyles";
import compose from "recompose/compose";


/**
 * Component to render an "alert" that prevents all other interaction on the site.
 *
 * @class Alert
 * @extends {React.Component}
 */
class Alert extends React.Component {

  render() {
    const { classes } = this.props;

    if (!this.props.open) {
      return <></>;
    }

    return (
      <Dialog
        open={true}
      >
        {
          <>
            <DialogTitle>{this.props.title}</DialogTitle>
            <DialogContent>
              <DialogContentText style={{ whiteSpace: "pre-wrap" }}>
                {this.props.description}
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              {
                this.props.info ?
                  <Button onClick={() => { this.props.handleClose(); this.props.handleResponse(); }} color="primary">
                    {this.props.infoText}
                  </Button>
                  :
                  <div>
                    <Button
                      onClick={() => { this.props.handleClose(); this.props.handleResponse(false); }}
                      color='secondary'
                      className={this.props.multilineButtons ? classes.multilineButton : classes.button}
                    >
                      {this.props.disagreeText}
                    </Button>
                    <Button
                      onClick={() => { this.props.handleClose(); this.props.handleResponse(true); }}
                      className={this.props.multilineButtons ? classes.multilineButton : classes.button}
                      variant='outlined'
                      color="primary"
                      autoFocus
                    >
                      {this.props.agreeText}
                    </Button>
                  </div>
              }
            </DialogActions>
          </>
        }
      </Dialog >
    );
  }
}


Alert.propTypes = {
  open: PropTypes.bool.isRequired, // is the alert open
  title: PropTypes.string.isRequired, // Title display on the vindow
  description: PropTypes.string, // textual description (below the titile)
  info: PropTypes.bool.isRequired, // If it's just un info alert with one button
  infoText: PropTypes.string.isRequired, // content of the alert when it's an info
  agreeText: PropTypes.string.isRequired, // Text display in the "agree" button
  disagreeText: PropTypes.string.isRequired, // Text display in the "disagree" button
  multilineButtons: PropTypes.bool.isRequired, // should the agree/disagree button displayed on multiple lines
  handleClose: PropTypes.func.isRequired, // function called when the alert is closed
  handleResponse: PropTypes.func.isRequired, // function called allong the previous one with false if the user disagreed and true otherwise. Or no parameters if info.
  classes: PropTypes.object.isRequired
};

Alert.defaultProps = {
  open: false,
  title: "Mon titre",
  description: "",
  info: false,
  agreeText: "OK",
  disagreeText: "Annuler",
  infoText: "J'ai compris",
  multilineButtons: false,
  // eslint-disable-next-line no-undef
  handleClose: () => { Console.error("Missing function for closing alert."); },
  // eslint-disable-next-line no-undef
  handleResponse: () => { Console.error("Missing function for handling post close."); }
};


const styles = {
  multilineButton: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
  },
  button: {}
};


export default compose(
  withStyles(styles, { withTheme: true }),
)(Alert);
