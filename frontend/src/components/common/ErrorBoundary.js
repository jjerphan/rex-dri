import React from "react";
import Alert from "./Alert";
import PropTypes from "prop-types";
import {setDisplayName} from "recompose";
import getActions from "../../redux/api/getActions";
import compose from "recompose/compose";
import {connect} from "react-redux";
import {RequestParams} from "../../redux/api/RequestParams";

function clear() {
  return {error: null, errorInfo: null, alertOpen: true};
}

/**
 * Component to act as an error boundary, to prevent the app from deep unrecoverable crashes.
 */
class ErrorBoundary extends React.Component {
  state = clear();

  componentDidCatch(error, errorInfo) {
    // Catch errors in any components below and re-render with error message
    this.setState({
      error,
      errorInfo,
      alertOpen: true,
    });

    const data = "stack" in error ? {componentStack: error.stack} : errorInfo;

    const params = RequestParams
      .Builder
      .withData(data)
      .build();
    this.props.logErrorOnServer(params);
  }

  render() {
    const {error, errorInfo} = this.state;
    // In case of error
    if (errorInfo) {
      // eslint-disable-next-line no-console
      console.log(error, errorInfo);
      return (
        <Alert open={this.state.alertOpen}
               info={true}
               title={"Une erreur inconnue c'est produite dans l'application. Nous vous prions de nous en excuser."}
               description={"Nous vous invitons à recharger la page. Si l'erreur persiste, merci de contacter les administrateurs du site; l'erreur leur a été transmise."}
               infoText={"C'est noté, je sais que vous faîtes de votre mieux :)"}
               handleResponse={() => undefined}
               handleClose={() => this.setState(clear())}
        />
      );
    }

    // Normally, just render children
    return this.props.children;
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.node.isRequired,
  logErrorOnServer: PropTypes.func.isRequired,
};


const mapDispatchToProps = (dispatch) => ({
  logErrorOnServer: (params) => dispatch(getActions("frontendErrors").create(params)),
});


const ConnectedErrorBoundary = compose(
  connect(() => ({}), mapDispatchToProps)
)(ErrorBoundary);


/**
 * HOC (higher order component) wrapper to provide an error boundary to the sub components.
 *
 * @returns {function(*): function(*): *}
 */
export function withErrorBoundary() {
  return Component => setDisplayName("error-boundary")(
    // We need to forward the ref otherwise the styles are not correctly applied.
    // eslint-disable-next-line react/display-name
    React.forwardRef((props, ref) =>
      <ConnectedErrorBoundary>
        <Component {...props} ref={ref}/>
      </ConnectedErrorBoundary>
    )
  );
}
