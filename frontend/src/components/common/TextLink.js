import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";

export function getLinkColor(theme) {

  return theme.palette.type === "dark" ?
    theme.palette.secondary.dark
    :
    theme.palette.secondary.light;
}

export const styles = theme => ({
  link: {
    color: theme.palette.text.primary,
    textDecoration: "none",
    boxShadow: `0px 0.08em 0.01em -0.01em ${getLinkColor(theme)}, inset 0px -0.1em 0.03em -0.03em ${getLinkColor(theme)}`,
    borderTopRightRadius: "100%",
    borderTopLeftRadius: "100%",
    borderBottomLeftRadius: "10%",
    borderBottomRightRadius: "30%",
  },
});

/**
 * Component to render the links in custom manner
 *
 */
function TextLink(params) {
  const {classes, ...props} = params;
  return (
    <a href={props.href} className={classes.link} target="_blank" rel="noopener noreferrer">
      {props.children}
    </a>
  );
}

TextLink.propTypes = {
  href: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

export default withStyles(styles, {withTheme: true})(TextLink);
