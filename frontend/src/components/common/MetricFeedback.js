import React from "react";
import {getGradient, Spectral, viridis} from "../../utils/colormaps";
import PropTypes from "prop-types";
import {classNames} from "../../utils/classNames";
import ThumbsUp from "@material-ui/icons/ThumbUp";
import ThumbsDown from "@material-ui/icons/ThumbDown";
import Close from "@material-ui/icons/Close";
import Warning from "@material-ui/icons/Warning";
import Done from "@material-ui/icons/Done";
import DoneAll from "@material-ui/icons/DoneAll";
import Favorite from "@material-ui/icons/Favorite";
import {makeStyles} from "@material-ui/styles";

const possibleIcons = [Close, Warning, Done, DoneAll, Favorite];
const breakPoints = [20, 40, 60, 80, 100];


const indicatorWidth = 14,
  indicatorExtraHeight = 10;

const useStyles = makeStyles(theme => ({
  barContainer: {
    flexWrap: "nowrap",
    display: "flex",
    alignItems: "center",
    paddingTop: 25,
    paddingBottom: 10,
  },
  barContent: {},
  linearBackground: {
    background: getGradient([...viridis].reverse())
  },
  divergentBackground: {
    background: getGradient(Spectral)
  },
  colorBar: {
    height: props => props.height,
    borderRadius: props => props.height / 2,
    marginLeft: 10,
    marginRight: 10,
  },
  indicator: {
    height: props => props.height + indicatorExtraHeight,
    width: indicatorWidth,
    top: -(indicatorExtraHeight / 2),
    borderRadius: indicatorExtraHeight,
    position: "relative",
    background: theme.palette.background.default,
    borderStyle: "solid",
    borderColor: theme.palette.text.primary,
  },
  indicatorIcon: {
    left: "50%",
    transform: "translateX(-50%)",
    position: "relative",
    top: -29
  }
}));


/**
 *
 * @param {number} percent
 * @returns {node}
 */
function getIcon(percent) {
  let i = 0;
  for (const p of breakPoints) {
    if (percent <= p) break;
    i += 1;
  }
  return possibleIcons[i];
}

function MetricFeedback(props) {
  const
    classes = useStyles(props),
    {
      min,
      max,
      value,
      width,
      showThumbs
    } = props,
    minValue = parseFloat(min),
    maxValue = parseFloat(max),
    percent = 100 * Math.abs(minValue - value) / Math.abs(maxValue - minValue),
    Icon = getIcon(percent),
    colorBarClasses = classNames(
      classes.colorBar,
      props.type === "linear" ? classes.linearBackground : classes.divergentBackground);


  return (
    <>
      <div className={classes.barContainer}>
        {showThumbs ? <ThumbsDown color={"disabled"}/> : <></>}
        <div className={classes.barContent}>
          <div
            className={colorBarClasses}
            style={{width}}>
            <div className={classes.indicator}
                 style={{
                   left: `${percent}%`,
                   transform: `translateX(-${percent}%)`,
                 }}>
              <Icon color={"secondary"} className={classes.indicatorIcon}/>
            </div>
          </div>
        </div>
        {showThumbs ? <ThumbsUp color={"disabled"}/> : <></>}
      </div>
    </>
  );
}


MetricFeedback.propTypes = {
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  type: PropTypes.oneOf(["divergent", "linear"]).isRequired,
  min: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  max: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  height: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
  showThumbs: PropTypes.bool.isRequired,
};

MetricFeedback.defaultProps = {
  type: "divergent",
  min: "-5",
  max: "+5",
  value: 0,
  width: 150,
  height: 20,
  showThumbs: false,
};

export default MetricFeedback;
