/* eslint-disable react/prop-types */
/* eslint-disable react/display-name */
// Inspired by : https://github.com/mui-org/material-ui/blob/master/docs/src/pages/page-layout-examples/blog/Markdown.js

import React from "react";
import PropTypes from "prop-types";

import ReactMarkdown from "react-markdown";
import Typography from "@material-ui/core/Typography";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

import {darken, lighten} from "@material-ui/core/styles/colorManipulator";
import Divider from "@material-ui/core/Divider";
import TextLink, {getLinkColor} from "../TextLink";
import {makeStyles} from "@material-ui/styles";

// Custom styling for the rendered markdown
// To be used in hooks based components only
const useStyles = makeStyles(theme => {
  const {palette} = theme,
    backgroundTable = palette.type === "dark" ? lighten(palette.background.paper, 0.07) : darken(palette.background.paper, 0.02),
    headerTable = palette.type === "dark" ? lighten(palette.background.paper, 0.13) : darken(palette.background.paper, 0.07);

  return {
    list: {
      color: palette.text.primary,
      marginBottom: theme.spacing(2),
    },
    code: {
      fontFamily: "monospace"
    },
    listItem: {
      marginTop: theme.spacing(0.75),
    },
    blockquote: {
      color: palette.text.secondary,
      borderLeftWidth: "5px",
      borderLeftStyle: "solid",
      borderLeftColor: getLinkColor(theme),
      borderRadius: "1px",
      margin: "1.5em 10px",
      paddingTop: "0px",
      paddingBottom: "0px",
      paddingRight: "10px",
      paddingLeft: "10px",
    },
    table: {
      //color: palette.text.primary,
    },
    tableHead: {
      fontSize: "14px",
      background: headerTable,
    },
    backgroundTable: {
      background: backgroundTable
    },
    bold: {
      fontWeight: 700
    },
    divider: {
      paddingBottom: theme.spacing(1),
      marginBottom: theme.spacing(1)
    }
  };
});

/////////////////////////////////////////
/// Renderers definition
////////////////////////////////////////

const headingScale = ["h1", "h2", "h3", "h4", "h5", "h6", "subtitle1", "body2"];

function getHeadingRender(offset) {

  return (props) => {
    const {level} = props;

    let variant,
      idx = level + offset - 1;

    if (idx < headingScale.length) {
      variant = headingScale[idx];
    } else {
      variant = headingScale[headingScale.length - 1];
    }

    return (
      <Typography gutterBottom variant={variant} style={{fontWeight: 500}}>
        {props.children}
      </Typography>
    );
  };
}


const ListRenderer = (props) => {
  if (props.ordered) {
    return (
      <ol className={useStyles().list}>
        {props.children}
      </ol>
    );
  } else {
    return (
      <ul className={useStyles().list}>
        {props.children}
      </ul>
    );
  }
};

const ListItemRenderer = (props) => (
  <li className={useStyles().listItem}>
    <Typography component="span">
      {props.children}
    </Typography>
  </li>
);

const CodeRenderer = (props) => (
  // className={classes.code} would be override by typography variant
  <Typography variant={"body2"} component='code' style={{fontFamily: "monospace"}}>
    {props.value}
  </Typography>
);

const InlineCodeRenderer = (props) => (
  <code className={useStyles().code}>
    {props.children}
  </code>
);


const BlockquoteRenderer = (props) => (
  <blockquote className={useStyles().blockquote}>
    <em>
      {props.children}
    </em>
  </blockquote>
);

const TableRenderer = (props) => (
  <Paper className={useStyles().backgroundTable}>
    <Table className={useStyles().table}>{props.children}</Table>
  </Paper>
);

const TableHeadRenderer = (props) => (
  <TableHead
    classes={{root: useStyles().bold}}
    className={useStyles().tableHead}
  >
    {props.children}
  </TableHead>
);

//// End of renderers definition

function getRenderers(headingOffset) {
  return ({
    heading: getHeadingRender(headingOffset),
    list: ListRenderer,
    listItem: ListItemRenderer,
    paragraph: (props) => <Typography paragraph>{props.children}</Typography>,
    link: props => <TextLink {...props} />,
    code: CodeRenderer,
    inlineCode: InlineCodeRenderer,
    blockquote: BlockquoteRenderer,
    table: TableRenderer,
    tableHead: TableHeadRenderer,
    tableBody: props => (<TableBody>{props.children}</TableBody>),
    tableRow: props => (<TableRow hover={true}>{props.children}</TableRow>),
    tableCell: props => (<TableCell>{props.children}</TableCell>),
    thematicBreak: () => (<Divider className={useStyles().divider}/>),
  });
}


/**
 * Custom Markdown component renderer to make use of material UI
 *
 * @class BaseMarkdown
 * @extends {Component}
 */
function BaseMarkdown(props) {

  const compiledSource = props.compileSource(props.source);
  const renderers = getRenderers(props.headingOffset);

  return (
    <div style={{wordBreak: "break-word"}}>
      <ReactMarkdown
        renderers={renderers}
        allowedTypes={[...Object.keys(renderers), "text", "emphasis", "root", "strong"]} // Only allow custom nodes and basic ones
        mode={"escape"}
        source={compiledSource}
      />
    </div>
  );

}

BaseMarkdown.propTypes = {
  source: PropTypes.string.isRequired,
  compileSource: PropTypes.func.isRequired,
  /**
   * Shift heading so that they are not too big
   */
  headingOffset: PropTypes.number.isRequired,
};

BaseMarkdown.defaultProps = {
  headingOffset: 4,
  compileSource: source => source
};

export default React.memo(BaseMarkdown);
