import React, {useState} from "react";
import Markdown from "./Markdown";
import PropTypes from "prop-types";
import {compose} from "recompose";
import withStyles from "@material-ui/core/styles/withStyles";
import Button from "@material-ui/core/Button";
import Collapse from "@material-ui/core/Collapse";


function TruncatedMarkdown(props) {
  const [truncated, setTruncated] = useState(false),
    {comment, truncateFromLength, classes} = props,
    truncatable = comment.length > truncateFromLength;


  if (!truncatable) {
    return <Markdown source={comment}/>;
  } else {
    return (
      <>
        <Collapse in={truncated} collapsedHeight={"4rem"}>
          <Markdown source={comment}/>
        </Collapse>
        {!truncated ? <div className={classes.gradientBorder}/> : <></>}
        <Button variant={"contained"}
                className={classes.moreButton}
                onClick={() => setTruncated(!truncated)}>
          {!truncated ? "En lire plus..." : "En lire moins..."}
        </Button>
      </>
    );
  }
}

TruncatedMarkdown.propTypes = {
  classes: PropTypes.object.isRequired,
  comment: PropTypes.string.isRequired,
  truncateFromLength: PropTypes.number.isRequired,
};

TruncatedMarkdown.defaultProps = {
  truncateFromLength: 100,
};

const styles = theme => ({
  truncated: {
    maxHeight: "6rem",
    overflow: "hidden",
  },
  gradientBorder: {
    position: "relative",
    marginTop: "-2rem",
    bottom: 0,
    width: "100%",
    height: "3rem",
    background: `linear-gradient(to bottom, transparent, ${theme.palette.background.paper})`,
  },
  moreButton: {
    display: "block",
    margin: "0 auto",
  }
});

export default compose(
  withStyles(styles)
)(TruncatedMarkdown);