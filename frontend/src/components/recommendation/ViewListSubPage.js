import React from "react";
import PropTypes from "prop-types";
import {compose} from "recompose";
import getActions from "../../redux/api/getActions";
import {RequestParams} from "../../redux/api/RequestParams";
import {connect} from "react-redux";
import CustomComponentForAPI from "../common/CustomComponentForAPI";
import List from "./view/View";
import Button from "@material-ui/core/Button";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import {APP_ROUTES} from "../../config/appRoutes";
import {withRouter} from "react-router-dom";


/**
 * Component to display one recommendation list
 */
class ViewListSubPage extends CustomComponentForAPI {
  apiParams = {
    "list": ({props}) =>
      RequestParams.Builder
        .withId(props.listId)
        .build(),
  };

  customRender() {
    const list = this.getLatestReadData("list");

    return (
      <>
        <Button onClick={() => this.props.history.push(APP_ROUTES.lists)}>
          <ArrowBackIcon/>
        </Button>
        <List list={list}/>
      </>
    );
  }
}

ViewListSubPage.propTypes = {
  list: PropTypes.object.isRequired,
};

ViewListSubPage.defaultProps = {};


const mapStateToProps = (state) => {
  return {
    list: state.api.recommendationListsOne,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    api: {
      list: (params) => dispatch(getActions("recommendationLists").readOne(params))
    },
  };
};

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(ViewListSubPage);
