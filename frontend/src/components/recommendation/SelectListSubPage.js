import React from "react";
import PropTypes from "prop-types";
import {compose} from "recompose";
import getActions from "../../redux/api/getActions";
import {connect} from "react-redux";
import CustomComponentForAPI from "../common/CustomComponentForAPI";
import TableHead from "@material-ui/core/TableHead";
import Table from "@material-ui/core/Table";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import {APP_ROUTES} from "../../config/appRoutes";
import {withRouter} from "react-router-dom";
import {withStyles} from "@material-ui/styles";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import {RequestParams} from "../../redux/api/RequestParams";
import DeleteIcon from "@material-ui/icons/DeleteForever";
import PublicIcon from "@material-ui/icons/LockOpen";
import PrivateIcon from "@material-ui/icons/Lock";
import IconButton from "@material-ui/core/IconButton";
import SimplePopupMenu from "../common/SimplePopupMenu";
import LinkToUser from "../common/LinkToUser";


const emptyList = {
  title: "Une nouvelle liste",
  is_public: false,
  description: "Description de la liste",
  content: [],
  // eslint-disable-next-line no-undef
  owner: __AppUserId,
};


/**
 * Component to render the list of all available recommendation lists
 */
class SelectListSubPage extends CustomComponentForAPI {
  state = {display: "owned"};

  redirectToList(listId) {
    this.props.history.push(APP_ROUTES.lists + listId);
  }

  customRender() {
    const lists = this.getLatestReadData("lists"),
      {display} = this.state,
      {classes} = this.props;

    const ownedLists = lists.filter(list => list.is_user_owner),
      followedLists = lists.filter(list => !list.is_user_owner),
      displayedLists = display === "owned" ? ownedLists : followedLists;

    return (
      <>
        <Button variant="contained" color={display === "owned" ? "primary" : "default"}
                className={classes.button}
                onClick={() => this.setState({display: "owned"})}>
          Mes listes ({ownedLists.length})
        </Button>
        <Button variant="contained"
                color={display === "followed" ? "primary" : "default"}
                className={classes.button}
                onClick={() => this.setState({display: "followed"})}>
          Les listes que je suis ({followedLists.length})
        </Button>
        <Button variant={"contained"}
                color={"secondary"}
                onClick={() =>
                  this.props.createList(
                    emptyList,
                    (data) => {
                      this.props.invalidateReadAll();
                      this.props.history.push(APP_ROUTES.lists + data.id);
                    })
                }
                className={classes.button}>
          <AddIcon className={classes.leftIcon}/> Créer une liste
        </Button>
        {
          displayedLists.length === 0 ?
            <></>
            :
            <div style={{overflowX: "scroll", height: "fit-content", display: "flex"}}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell size={"small"} />
                    <TableCell style={{minWidth: 300}}>Intitulé</TableCell>
                    <TableCell>Créateur</TableCell>
                    <TableCell size={"small"}>Visibilité</TableCell>
                    <TableCell>#Followers</TableCell>
                    <TableCell style={{minWidth: 300}}>Description</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {displayedLists.sort((a, b) => new Date(b.last_update) - new Date(a.last_update)).map(list => (
                    <TableRow hover={true}
                              key={list.id}
                              className={classes.row}>
                      <TableCell size={"small"}>
                        <SimplePopupMenu
                          items={[
                            {
                              label: "Confirmer",
                              onClick: () => this.props.deleteList(list.id, () => this.props.invalidateReadAll())
                            },
                          ]}
                          renderHolder={({onClick}) => (
                            <IconButton color="secondary"
                                        onClick={onClick}
                                        disabled={display !== "owned"}>
                              <DeleteIcon/>
                            </IconButton>
                          )}/>
                      </TableCell>
                      {/*A lot of onClick to be able to have a different one for delete*/}
                      <TableCell onClick={() => this.redirectToList(list.id)}>
                        {list.title}
                      </TableCell>
                      <TableCell>
                        {list.is_user_owner ? <em>Moi</em> : <LinkToUser userId={list.owner} pseudo={list.owner_pseudo}/>}
                      </TableCell>
                      <TableCell size={"small"}
                                 onClick={() => this.redirectToList(list.id)}>
                        {list.is_public ? <PublicIcon/> : <PrivateIcon color={"secondary"}/>}
                      </TableCell>
                      <TableCell onClick={() => this.redirectToList(list.id)}>
                        {list.nb_followers}
                      </TableCell>
                      <TableCell onClick={() => this.redirectToList(list.id)}>
                        {list.description}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </div>
        }
      </>
    );
  }
}

SelectListSubPage.propTypes = {
  classes: PropTypes.object.isRequired,
  lists: PropTypes.object.isRequired,
};

SelectListSubPage.defaultProps = {};


const mapStateToProps = (state) => {
  return {
    lists: state.api.recommendationListsAll,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    api: {
      lists: () => dispatch(getActions("recommendationLists").readAll())
    },
    invalidateReadAll: () => dispatch(getActions("recommendationLists").invalidateAll()),
    createList: (data, onSuccess) => dispatch(getActions("recommendationLists").create(RequestParams.Builder.withData(data).withOnSuccessCallback(onSuccess).build())),
    deleteList: (id, onSuccess) => dispatch(getActions("recommendationLists").delete(RequestParams.Builder.withId(id).withOnSuccessCallback(onSuccess).build()))
  };
};

const styles = theme => ({
  row: {
    "&:hover": {
      cursor: "pointer"
    }
  },
  button: {
    margin: theme.spacing(1),
  },
  leftIcon: {
    marginRight: theme.spacing(1),
  }
});

export default compose(
  withRouter,
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(SelectListSubPage);
