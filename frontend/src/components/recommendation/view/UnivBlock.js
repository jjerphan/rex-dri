import React from "react";
import PropTypes from "prop-types";
import keycode from "keycode";
import useBlock from "./useBlock";
import {getLatestReadDataFromStore} from "../../../redux/api/utils";
import DownshiftMultiple from "../../common/DownshiftMultiple";
import CustomNavLink from "../../common/CustomNavLink";
import {APP_ROUTES} from "../../../config/appRoutes";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import MetricFeedback from "../../common/MetricFeedback";
import OnBlurContainer from "../../common/OnBlurContainer";
import Paper from "@material-ui/core/Paper";
import {makeStyles} from "@material-ui/styles";


class UniversityHelper {
  /**
   * @private
   * @type {Map}
   */
  static _lookup = undefined;

  static getUniv(univId) {
    if (typeof UniversityHelper._lookup === "undefined") {
      UniversityHelper._lookup = new Map();
      getLatestReadDataFromStore("universitiesAll")
        .forEach(univ => UniversityHelper._lookup.set(univ.id, univ));
    }
    return UniversityHelper._lookup.get(univId);
  }

  /**
   * @private
   * @type {array.<object>}
   */
  static _options = undefined;

  static getUniversitiesOptions() {
    if (typeof UniversityHelper._options === "undefined") {
      UniversityHelper._options = getLatestReadDataFromStore("universitiesAll")
        .map(el => ({value: el.id, label: el.name}));
    }
    return UniversityHelper._options;
  }
}


const useStyle = makeStyles(theme => ({
  paper: {
    ...theme.myPaper,
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
}));

/**
 * Comment block that will hold markdown text
 */
function UnivBlock(props) {
  const [
    content,
    updateContent,
    renderAsInput,
    handleSwitchToInput,
    handleSwitchToDisplay
  ] = useBlock({university: props.university, appreciation: props.appreciation},
    (v) => props.onChange(v),
    v => {
      if (v.university !== null) v.university = parseInt(v.university);
      return v;
    },
    v => v.university === null,
    props.readOnly);

  const classes = useStyle();

  const univId = content.university,
    appreciation = content.appreciation,
    univInfo = UniversityHelper.getUniv(univId);

  const options = UniversityHelper.getUniversitiesOptions();

  function handleAppreciationChange(value) {
    const regex = /[^\d]*/gi;
    let cleaned = parseInt(value.replace(regex, "").replace("-", ""));
    if (isNaN(cleaned)) {
      cleaned = null;
    } else if (cleaned < 0) {
      cleaned = 0;
    } else if (cleaned > 10) {
      cleaned = 10;
    }
    updateContent(c => Object.assign({}, c, {appreciation: cleaned}));
  }

  // link = APP_ROUTES.forUniversity(univ.id),
  // source = `# [${univ.name}](${link}) \n ${univ.city}, ${univ.country}`;

  const univPlaceHolder = "Entrez le nom de l'université à associer au block";
  return (
    <div onClick={handleSwitchToInput} onKeyDown={(e) => {
      if (e.ctrlKey && keycode(e) === "enter") handleSwitchToDisplay();
    }}>
      {
        renderAsInput ?
          <OnBlurContainer onBlur={handleSwitchToDisplay}>
            <DownshiftMultiple multiple={false}
                               options={options}
                               fieldPlaceholder={univPlaceHolder}
                               autoFocusInput={true}
                               onChange={(id) => updateContent(c => Object.assign({}, c, {university: id}))}
                               value={univId !== null ? [univId] : undefined}/>

            <div style={univId ? {} : {display: "none"}}>
              <Typography>Appréciation</Typography>
              <TextField placeholder={"Votre appréciation de l'établissement (entre 0 et 10)"}
                         fullWidth={true}
                         multiline={false}
                         value={appreciation === null ? "" : appreciation}
                         type={"number"}
                         inputProps={{min: 0, max: 10}}
                         required={false}
                         onChange={(e) => handleAppreciationChange(e.target.value)}/>
            </div>

          </OnBlurContainer>
          :
          <>

            {
              univInfo ?
                <Paper className={classes.paper}>
                  <div onClick={handleSwitchToInput}
                       onFocus={handleSwitchToInput}>
                    <Typography variant={"h4"}>{univInfo.name}</Typography>

                    {
                      appreciation !== null ?
                        <MetricFeedback value={appreciation} min={0} max={10}/>
                        :
                        <></>
                    }
                  </div>
                  <CustomNavLink to={APP_ROUTES.forUniversity(univId)}>
                    <Button variant={"outlined"}>
                      Se rendre sur la page de l'université
                    </Button>
                  </CustomNavLink>
                </Paper>
                : <></>
            }

          </>
      }
    </div>
  );
}

UnivBlock.propTypes = {
  onChange: PropTypes.func.isRequired,
  university: PropTypes.number,
  appreciation: PropTypes.number,
  readOnly: PropTypes.bool.isRequired,
};

UnivBlock.defaultProps = {};


export default React.memo(UnivBlock);