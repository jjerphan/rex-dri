import {useCallback, useState} from "react";

export default function useBlock(initialValue, onChange, cleanValue = v => v, forceEdit = () => false, readOnly = false) {
  const [value, setValue] = useState(initialValue),
    [renderAsDisplay, setRenderAsDisplay] = useState(value),
    renderAsEdit = forceEdit(value) ? true : !renderAsDisplay;

  const updateValue = useCallback((value) => {
    setValue(value);
    setRenderAsDisplay(false);
    onChange(value);
  }, []);

  const handleSwitchToEdit = useCallback(() => {
    if (!renderAsEdit) {
      setRenderAsDisplay(false);
    }
  }, [renderAsEdit]);

  const handleSwitchToDisplay = useCallback(() => {
    if (renderAsEdit) {
      const cleaned = cleanValue(value);
      updateValue(cleaned);
      setRenderAsDisplay(true);
    }
  }, [value, renderAsEdit]);

  return [value, updateValue, readOnly ? false : renderAsEdit, handleSwitchToEdit, handleSwitchToDisplay];
}