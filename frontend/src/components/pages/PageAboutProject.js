import React from "react";
import Typography from "@material-ui/core/Typography";
import Markdown from "../common/markdown/Markdown";
import {compose} from "recompose";
import {withErrorBoundary} from "../common/ErrorBoundary";
import {withPaddedPaper} from "./shared";


const source = `
# Contexte

# Contribuer

Rendez vous sur [le GitLab de l'UTC](https://gitlab.utc.fr/rex-dri/rex-dri) !
`;


/**
 * Component corresponding to page about the project.
 */
function PageAboutProject() {
  return (
    <>
      <Typography variant="h3">
        Le projet <em><b>REX-DRI</b></em>
      </Typography>
      <Markdown source={source}/>
    </>
  );
}

export default compose(
  withPaddedPaper(),
  withErrorBoundary()
)(PageAboutProject);
