import React from "react";
import PropTypes from "prop-types";
import {setDisplayName} from "recompose";
import {makeStyles} from "@material-ui/styles";
import {classNames} from "../../utils/classNames";
import {Paper} from "@material-ui/core";


const useStyle = makeStyles(theme => ({
  generalPadding: {
    [theme.breakpoints.up("md")]: {
      padding: theme.spacing(2, 3, 1, 3),
    },
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(1),
    },
  },
  paper: theme.myPaper,
}));


export function PaddedPageDiv(props) {
  const classes = useStyle();
  return (
    <div className={classes.generalPadding}>
      {props.children}
    </div>
  );
}

PaddedPageDiv.propTypes = {
  children: PropTypes.node.isRequired,
};

/**
 * HOC (higher order component) wrapper to provide an error boundary to the sub components.
 *
 * @returns {function(*): function(*): *}
 */
export function withPaddedDiv() {
  return Component => setDisplayName("Padded page div")(
    // We need to forward the ref otherwise the styles are not correctly applied.
    // eslint-disable-next-line react/display-name
    React.forwardRef((props, ref) =>
      <PaddedPageDiv>
        <Component {...props} ref={ref}/>
      </PaddedPageDiv>
    )
  );
}

export function PaddedPaper(props) {
  const classes = useStyle();
  return (
    <div className={classes.generalPadding}>
      <Paper className={classNames(classes.paper)}>
        {props.children}
      </Paper>
    </div>
  );
}

PaddedPaper.propTypes = {
  children: PropTypes.node.isRequired,
};

export function withPaddedPaper() {
  return Component => setDisplayName("Padded paper")(
    // We need to forward the ref otherwise the styles are not correctly applied.
    // eslint-disable-next-line react/display-name
    React.forwardRef((props, ref) =>
      <PaddedPaper>
        <Component {...props} ref={ref}/>
      </PaddedPaper>
    )
  );
}