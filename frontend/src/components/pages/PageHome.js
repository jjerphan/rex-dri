import React from "react";
import {compose} from "recompose";
import {withErrorBoundary} from "../common/ErrorBoundary";
import Typography from "@material-ui/core/Typography";
import Markdown from "../common/markdown/Markdown";
import {withPaddedPaper} from "./shared";


const source = `
**Ce service est à l'heure actuelle au stade de version _alpha_ afin de montrer certaines fonctionnalités. Les données seront vraisemblablement remises à zéro lors du passage à la phase _beta_ (quand toutes les fonctionnalités seront en place).**

Si vous trouvez des bugs ou si vous avez des suggestions, merci de les signaler [ici](https://gitlab.utc.fr/chehabfl/outgoing_rex/issues) ou par mail à l'adresse [florent.chehab@etu.utc.fr](mailto:florent.chehab@etu.utc.fr).

Pour rendre plus parlantes certaines fonctionnalités liées à la modération (grandement paramétrable) des informations, durant cette phase _alpha_ vous pouvez rejoindre les différents groupes d'accès tout seul :
- Pour rejoindre le groupe des modérateurs, cliquez [ici](/role_change/moderator/)
- Pour rejoindre le groupe d'accès « DRI », cliquez [ici](/role_change/dri/)
- Pour rejoindre le groupe _classique_, cliquez [ici](/role_change/normal/)

Les données actuellement présente sur la plateforme sont extraites d'un document récapitulant les destinations offertes aux GI il y a quelques semestre de cela ; et elles sont complétés par mes ajouts personnels (voir en particulier pour l'EPFL).

## Fonctionnalités manquantes

Voici les fonctionnalités qui seront rajoutées « prochainement » :
- Possibilité de filtrer les universités sur la page avec la carte ou celle avec la recherche. Les critères seront (ou devraient être) : destinations disponibles à tel semestre, destinations où sont partis des étudiants de telles branches/filières, destinations ouvertes à des étudiants de telles branches/filières, niveau de langue requis, etc.
- Possibilité de faire des listes commentées avec des universités et de les partager (ou non).
- Ajout des autres modules sur les pages des universités.
- Gestion des retours des étudiants sur leurs départs.
- Meilleure compatibilité avec les mobiles.

NB : les images de couverture sur les pages des universités seront aussi différentes ! (actuellement cet élément n'est pas _connecté_ au serveur)

--------

L'objectif est de mettre en place la phase _beta_ d'ici la prochaine session de candidature pour les départs à l'étranger.

--------

Les objectifs de ce service sont :
- Regrouper les informations sur les départs à l'étranger réalisés par les étudiants de l'UTC ;
- Les renders accessibles et commensurables.


Âge des données de l'UTC :

| **Feature** | **Support** |
| ------ | ----------- |
| Ancien départs | ✔ |
| Départs possibles | ✔ |
| Informatio sur les universités | ✔ |

## Autre fun feature

You can format money: \`:100CHF:\` => :100CHF:
You can format money: \`:100LSD:\` => :100LSD:

`;


/**
 * Component corresponding to the landing page of the site
 */
function PageHome() {
  return (
    <>
      <Typography variant="h3">
        Bienvenue sur <em><b>REX-DRI</b></em> !
      </Typography>
      <Markdown source={source} headingOffset={2} />
    </>
  );
}

PageHome.propTypes = {};

export default compose(
  withPaddedPaper(),
  withErrorBoundary()
)(PageHome);
