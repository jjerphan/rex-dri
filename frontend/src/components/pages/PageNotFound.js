import React from "react";
import {compose} from "recompose";
import Typography from "@material-ui/core/Typography";
import {withPaddedPaper} from "./shared";


/**
 * Component corresponding to the landing page of the site
 */
function PageHome() {
  return (
    <>
      <Typography variant="h3">
        Cet URL ne mène à aucune page 😢
      </Typography>
    </>
  );
}

PageHome.propTypes = {};

export default compose(
  withPaddedPaper(),
)(PageHome);
