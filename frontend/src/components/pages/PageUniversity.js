import React from "react";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import CustomComponentForAPI from "../common/CustomComponentForAPI";
import {connect} from "react-redux";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";

import UniversityTemplate from "../university/UniversityTemplate";
import UnivInfoProvider from "../university/common/UnivInfoProvider";

import {Redirect} from "react-router-dom";

import getActions from "../../redux/api/getActions";

import compose from "recompose/compose";
import {withStyles} from "@material-ui/core";
import {withErrorBoundary} from "../common/ErrorBoundary";
import {APP_ROUTES} from "../../config/appRoutes";
import CustomNavLink from "../common/CustomNavLink";
import CustomLink from "../common/CustomLink";
import {PaddedPaper} from "./shared";

let previousUnivId = -1;

/**
 * Component holding the page with the university details
 *
 * @class PageUniversity
 * @extends {CustomComponentForAPI}
 * @extends React.Component
 */
class PageUniversity extends CustomComponentForAPI {

  componentDidMount() {
    super.componentDidMount();
    const requestedUnivId = this.getUnivIdFromProps();

    if (requestedUnivId && requestedUnivId !== "previousOne") {
      previousUnivId = requestedUnivId;
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    super.componentDidUpdate(prevProps, prevState, snapshot);
  }

  getUnivIdFromProps() {
    return this.props.match.params.univId;
  }

  renderUniversityNotFound() {
    return (
      <Dialog
        open={true}
        //onClose={this.handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"L'université demandée n'est pas reconnue !"}</DialogTitle>
        <DialogActions>
          <CustomNavLink to={APP_ROUTES.university}>
            <Button variant="contained" color="primary">
              C'est noté, ramenez-moi sur le droit chemin.
            </Button>
          </CustomNavLink>
        </DialogActions>
      </Dialog>
    );
  }

  renderUniversityUndefinedButHavePrevious(univId) {
    return (
      <Redirect to={APP_ROUTES.forUniversity(univId)}/>
    );
  }

  renderFirstTimeHere() {
    return (
      <PaddedPaper>
        <Typography>
          C'est la première fois que vous consulter cet onglet. Nous vous invitons à <CustomLink to={APP_ROUTES.map}>parcourir
          les universités</CustomLink> dans un
          premier temps. 😁
        </Typography>
      </PaddedPaper>
    );
  }


  renderDefaultView(univId, tabName) {
    return (
      <UnivInfoProvider univId={univId}>
        <UniversityTemplate tabName={tabName} univId={univId}/>
      </UnivInfoProvider>
    );
  }

  customRender() {
    const {match} = this.props,
      requestedUnivId = match.params.univId,
      tabName = match.params.tabName,
      universities = this.getLatestReadData("universities");

    if (requestedUnivId === "previousOne" || typeof requestedUnivId === "undefined") {
      if (previousUnivId !== -1) {
        return this.renderUniversityUndefinedButHavePrevious(previousUnivId);
      } else {
        return this.renderFirstTimeHere();
      }
    } else {
      if (universities.find(univ => parseInt(univ.id) === parseInt(requestedUnivId))) {
        return this.renderDefaultView(requestedUnivId, tabName);
      } else {
        return this.renderUniversityNotFound();
      }
    }
  }
}

PageUniversity.propTypes = {
  universities: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  return {
    universities: state.api.universitiesAll,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    api: {
      universities: () => dispatch(getActions("universities").readAll()),
    },
  };
};

const styles = theme => ({
  paper: theme.myPaper
});

export default compose(
  // volontarly no withPaddedPaper(), here
  withStyles(styles, {withTheme: true}),
  connect(mapStateToProps, mapDispatchToProps, null, {
    pure: false
  }),
  withErrorBoundary()
)(PageUniversity);
