import React from "react";
import compose from "recompose/compose";
import {withErrorBoundary} from "../common/ErrorBoundary";
import SelectList from "../recommendation/SelectListSubPage";
import ViewList from "../recommendation/ViewListSubPage";
import PropTypes from "prop-types";

import {withPaddedPaper} from "./shared";

/**
 * Component to render the page associated with recommendation lists
 *
 * @param props
 * @returns {*}
 * @constructor
 */
function PageLists(props) {
  const listId = props.match.params.listId;
  return (
    <>
      {typeof listId === "undefined" ?
        <SelectList/>
        :
        <ViewList listId={listId}/>
      }
    </>
  );
}

PageLists.propTypes = {
  match: PropTypes.object.isRequired,
};

export default compose(
  withPaddedPaper(),
  withErrorBoundary(),
)(PageLists);
