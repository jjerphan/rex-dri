import React from "react";
import Typography from "@material-ui/core/Typography";
import Search from "../search/Search";
import {withErrorBoundary} from "../common/ErrorBoundary";
import {compose} from "recompose";
import Filter from "../filter/Filter";
import {withPaddedPaper} from "./shared";


/**
 * Component corresponding to the page with the search university capabilities
 */
function PageSearch() {
  return (
    <>
      <Typography variant="h4" gutterBottom>
        Recherche d'une université
      </Typography>
      <Filter/>
      <Search/>
    </>
  );
}

PageSearch.propTypes = {};

export default compose(
  withPaddedPaper(),
  withErrorBoundary(),
)(PageSearch);
