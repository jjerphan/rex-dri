import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import PropTypes from "prop-types";
import {classNames} from "../../utils/classNames";
import {appBarHeight, siteMaxWidth} from "../../config/sharedStyles";
import {makeStyles} from "@material-ui/styles";

const useStyle = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  appBar: {
    width: "100%",
    height: appBarHeight(theme),
  },
  toolBar: {
    width: "100%",
    maxWidth: siteMaxWidth(),
    display: "flex",
  },
  content: {
    maxWidth: siteMaxWidth(),
  },
  centered: {
    margin: "0 auto",
  },
  middleBlock: {
    flex: 1
  },
}));

export default function BaseTemplate(props) {
  const classes = useStyle();
  return (
    <div className={classes.root}>
      <AppBar position="sticky" className={classes.appBar}>
        <Toolbar className={classNames(classes.toolBar, classes.centered)}>
          {props.toolbarContent}
        </Toolbar>
      </AppBar>

      {props.inBetween}

      <div className={classNames(classes.content, classes.centered)}>
        {props.children}
      </div>
    </div>
  );

}

BaseTemplate.propTypes = {
  toolbarContent: PropTypes.node.isRequired,
  children: PropTypes.node.isRequired,
  inBetween: PropTypes.node.isRequired,
};

BaseTemplate.defaultProps = {
  inBetween: <></>
};
