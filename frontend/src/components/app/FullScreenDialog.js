import React, { Component } from "react";
import Dialog from "@material-ui/core/Dialog";
import Slide from "@material-ui/core/Slide";
import PropTypes from "prop-types";
import { connect } from "react-redux";


/**
 * Component to enable the FullScreenDialog to have nice transitions
 * @returns
 */
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});


/**
 * Class to display a full screen dialog.
 * It is connected to the redux state to have only one of such dialog across the app.
 *
 * @class FullScreenDialog
 * @extends {Component}
 */
class FullScreenDialog extends Component {
  render() {
    return (
      <Dialog
        fullScreen
        open={this.props.open}
        TransitionComponent={Transition}
      >
        {this.props.innerNodes}
      </Dialog>
    );
  }
}


FullScreenDialog.propTypes = {
  open: PropTypes.bool.isRequired, // Is the full screen dialog open
  innerNodes: PropTypes.node.isRequired, // content of the fullScreen Dialog
};

// Get the props from the redux store.
const mapStateToProps = (state) => ({ ...state.app.fullScreenDialog });

export default connect(mapStateToProps)(FullScreenDialog);
