/** General app JS entry
 */

import React from "react";
import FullScreenDialog from "./FullScreenDialog";

import {connect} from "react-redux";
import CustomComponentForAPI from "../common/CustomComponentForAPI";
import {compose} from "recompose";
import {withErrorBoundary} from "../common/ErrorBoundary";
import {Route, Switch} from "react-router-dom";

import getActions from "../../redux/api/getActions";

import PageMap from "../pages/PageMap";
import PageHome from "../pages/PageHome";
import PageUniversity from "../pages/PageUniversity";
import PageSearch from "../pages/PageSearch";
import PageSettings from "../pages/PageThemeSettings";
import PageUser from "../pages/PageUser";
import PageLists from "../pages/PageLists";
import MainAppFrame from "./MainAppFrame";
import {APP_ROUTES} from "../../config/appRoutes";
import PageAboutProject from "../pages/PageAboutProject";
import {PageCgu, PageRgpd} from "../pages/PagesRgpdCgu";
import PageNotFound from "../pages/PageNotFound";

/**
 * @class App
 * @extends {CustomComponentForAPI}
 * @extends React.Component
 */
class App extends CustomComponentForAPI {
  customRender() {
    return (
      <MainAppFrame>
        <FullScreenDialog/>
        <main>
          <Switch>
            <Route exact path={APP_ROUTES.base} component={PageHome}/>
            <Route path={APP_ROUTES.search} component={PageSearch}/>
            <Route path={APP_ROUTES.map} component={PageMap}/>
            <Route path={APP_ROUTES.themeSettings} component={PageSettings}/>
            <Route path={APP_ROUTES.listsWithParams} component={PageLists}/>
            <Route path={APP_ROUTES.universityWithParams} component={PageUniversity}/>
            <Route path={APP_ROUTES.userWithParams} component={PageUser}/>
            <Route path={APP_ROUTES.aboutProject} component={PageAboutProject}/>
            <Route path={APP_ROUTES.aboutRgpd} component={PageRgpd}/>
            <Route path={APP_ROUTES.aboutCgu} component={PageCgu}/>
            <Route component={PageNotFound}/>
          </Switch>
        </main>
      </MainAppFrame>
    );
  }
}

App.propTypes = {};

// Already load some of the data even if it's not use here.
//    /!\ Don't delete it
const mapStateToProps = (state) => {
  return {
    countries: state.api.countriesAll,
    currencies: state.api.currenciesAll,
    universities: state.api.universitiesAll,
    languages: state.api.languagesAll,
    serverModerationStatus: state.api.serverModerationStatusAll,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    api: {
      countries: () => dispatch(getActions("countries").readAll()),
      currencies: () => dispatch(getActions("currencies").readAll()),
      universities: () => dispatch(getActions("universities").readAll()),
      languages: () => dispatch(getActions("languages").readAll()),
      serverModerationStatus: () => dispatch(getActions("serverModerationStatus").readAll()), // not needed for server moderation status
    },
  };
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withErrorBoundary(),
)(App);
