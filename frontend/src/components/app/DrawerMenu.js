import React from "react";
import PropTypes from "prop-types";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import {infoMenuItems, mainMenuHome, mainMenuItems, secondaryMenuItems, settingsMenuItems,} from "./menuItems";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import CustomNavLink from "../common/CustomNavLink";
import SettingsIcon from "@material-ui/icons/Settings";
import InfoIcon from "@material-ui/icons/Info";

class DrawerMenu extends React.Component {

  toListItem(items, inset = false) {
    return items.map(({label, route, Icon}, idx) => (
      <CustomNavLink key={idx} to={route} onClick={() => this.props.closeDrawer()}>
        <ListItem button onClick={() => this.props.closeDrawer()}>
          {
            Icon !== null ?
              <ListItemIcon>
                <Icon/>
              </ListItemIcon>
              :
              <></>
          }
          <ListItemText primary={label} inset={inset}/>
        </ListItem>
      </CustomNavLink>
    ));
  }

  toListItemBasic(label, Icon) {
    return (
      <ListItem>
        <ListItemIcon>
          <Icon/>
        </ListItemIcon>
        <ListItemText primary={label}/>
      </ListItem>
    );
  }


  render() {
    return (
      <div style={{zIndex: 200000}}>
        <Drawer open={this.props.open} onClose={() => this.props.closeDrawer()}>
          <List>{this.toListItem([mainMenuHome])}</List>
          <Divider/>
          <List>{this.toListItem(mainMenuItems)}</List>
          <Divider/>
          <List>{this.toListItem(secondaryMenuItems)}</List>
          <Divider/>
          <List>
            {this.toListItemBasic(<em>Informations</em>, InfoIcon)}
            <Divider variant="inset"/>
            {this.toListItem(infoMenuItems, true)}
          </List>
          <Divider/>
          <List>
            {this.toListItemBasic(<em>Paramètres</em>, SettingsIcon)}
            <Divider variant="inset"/>
            {this.toListItem(settingsMenuItems, true)}
          </List>
        </Drawer>
      </div>
    );
  }
}

DrawerMenu.propTypes = {
  open: PropTypes.bool.isRequired,
  closeDrawer: PropTypes.func.isRequired,
};

export default DrawerMenu;