import React from "react";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import SettingsIcon from "@material-ui/icons/Settings";
import InfoIcon from "@material-ui/icons/Info";
import Button from "@material-ui/core/Button";
import {infoMenuItems, mainMenuItems, secondaryMenuItems, settingsMenuItems} from "./menuItems";
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";
import Fab from "@material-ui/core/Fab";
import IconWithMenu from "../common/IconWithMenu";
import DrawerMenu from "./DrawerMenu";
import {APP_ROUTES} from "../../config/appRoutes";
import CustomNavLink from "../common/CustomNavLink";
import {classNames} from "../../utils/classNames";
import Logo from "./Logo";
import BaseTemplate from "./BaseTemplate";

const styles = theme => ({
  menuButton: {
    [theme.breakpoints.up("lg")]: {
      display: "none",
    },
    marginLeft: -12,
    marginRight: 20,
  },
  menuButtonIcon: {
    fontSize: "1.5em",
  },
  mainMenuButton: {
    fontSize: "1.2rem",
    fontWeight: 700,
    [theme.breakpoints.down("md")]: {
      display: "none"
    },
    [theme.breakpoints.up("lg")]: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
    }
  },
  mainMenuButtonIcon: {
    marginLeft: theme.spacing(1),
    fontSize: "1.2em",
    [theme.breakpoints.down("md")]: {
      display: "none"
    }
  },
  contrastTextPrimary: {
    color: theme.palette.getContrastText(theme.palette.primary.main),
  },
  contrastTextSecondary: {
    color: theme.palette.getContrastText(theme.palette.secondary.main),
  },
  mobileIcon: {},
  mobileIconContainer: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  primaryDark: {
    backgroundColor: theme.palette.primary.dark,
    boxShadow: theme.shadows[1],
  },
  desktopOnly: {
    [theme.breakpoints.down("md")]: {
      display: "none"
    }
  },
  mobileOnly: {
    [theme.breakpoints.up("lg")]: {
      display: "none",
    },
  },
  ifNotTooSmallAndInlined: {
    ["@media (max-width:790px)"]: {
      display: "none",
    },
    display: "inline",
  },
  centered: {
    margin: "0 auto",
  },
  leftBlock: {
    flex: 2
  },
  middleBlock: {
    flex: 1
  },
  rightBlock: {
    flex: 2,
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    textAlign: "center",
  },
  widthFitContent: {
    width: "fit-content",
  },
  filler: {
    width: "100%",
  },
});

class MainAppFrame extends React.Component {
  state = {
    drawerOpened: false,
  };

  openDrawer() {
    this.setState({drawerOpened: true});
  }

  closeDrawer() {
    this.setState({drawerOpened: false});
  }


  /**
   * Helper to render the full size versions of the menu items
   * @param items
   */
  renderMenuItems(items) {
    const {classes} = this.props;
    return (
      <div className={classNames(classes.desktopOnly, classes.centered, classes.widthFitContent)}>
        {items.map(({label, Icon, route}, idx) => (
          <CustomNavLink to={route} key={idx}>
            <Button color="default" className={classNames(classes.mainMenuButton, classes.contrastTextPrimary)}>
              {label}
              {<Icon className={classes.mainMenuButtonIcon}/>}
            </Button>
          </CustomNavLink>
        ))}
      </div>
    );
  }


  /**
   * Helper to render simplified versions (icon based) of menu
   * @param items
   */
  renderSimplified(items) {
    const {classes} = this.props;
    return (
      <div className={classes.ifNotTooSmallAndInlined}>
        {items.map(({label, Icon, route}, idx) => (
          <CustomNavLink to={route} key={idx}>
            <Fab size="medium"
                 color="primary"
                 classes={{primary: classes.primaryDark}}
                 aria-label={label}
                 className={classes.mobileIconContainer}>
              {<Icon className={classNames(classes.mobileIcon, classes.contrastTextSecondary)}/>}
            </Fab>
          </CustomNavLink>
        ))}
      </div>
    );
  }

  renderSimplifiedLeft() {
    const {classes} = this.props;
    return (
      <div className={classes.mobileOnly}>
        <IconButton className={classes.menuButton}
                    color="inherit"
                    aria-label="Menu"
                    onClick={() => this.openDrawer()}>
          <MenuIcon className={classes.menuButtonIcon}/>
        </IconButton>
        {this.renderSimplified(mainMenuItems)}
      </div>
    );
  }

  renderSimplifiedRight() {
    const {classes} = this.props;
    return (
      <div className={classes.mobileOnly}>
        {this.renderSimplified(secondaryMenuItems)}
      </div>
    );
  }

  renderSecondaryIcons() {
    const {classes} = this.props;
    return (
      <div className={classes.ifNotTooSmallAndInlined}>
        <IconWithMenu Icon={InfoIcon}
                      iconProps={{color: "inherit", className: classes.mobileIcon}}
                      fabProps={{
                        color: "primary",
                        className: classes.mobileIconContainer,
                        classes: {primary: classes.primaryDark}
                      }}
                      menuItems={infoMenuItems}/>

        <IconWithMenu Icon={SettingsIcon}
                      iconProps={{color: "inherit", className: classes.mobileIcon}}
                      fabProps={{
                        color: "primary",
                        className: classes.mobileIconContainer,
                        classes: {primary: classes.primaryDark}
                      }}
                      menuItems={settingsMenuItems}/>
      </div>
    );
  }

  render() {
    const {classes} = this.props;

    const inBetween = <DrawerMenu open={this.state.drawerOpened} closeDrawer={() => this.closeDrawer()}/>;

    const toolbarContent = (
      <>
        <div className={classes.leftBlock}>
          {this.renderMenuItems(mainMenuItems)}
          {this.renderSimplifiedLeft()}
        </div>

        <div className={classes.middleBlock}>
          <Logo linkTo={APP_ROUTES.base}/>
        </div>

        <div className={classes.rightBlock}>
          <div style={{flex: 2}}/>
          <div style={{flex: 3, flexBasis: "auto"}}>
            {this.renderMenuItems(secondaryMenuItems)}
            {this.renderSimplifiedRight()}
          </div>
          <div style={{flex: 4, flexBasis: "auto"}}>
            {this.renderSecondaryIcons()}
          </div>
        </div>
      </>
    );

    return (
      <BaseTemplate toolbarContent={toolbarContent} inBetween={inBetween}>
        {this.props.children}
      </BaseTemplate>
    );
  }
}

MainAppFrame.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.node.isRequired,
};

export default withStyles(styles, {withTheme: true})(MainAppFrame);