import {classNames} from "../../utils/classNames";
import CustomNavLink from "../common/CustomNavLink";
import Chip from "@material-ui/core/Chip";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import React from "react";

import {makeStyles} from "@material-ui/styles";
import PropTypes from "prop-types";


const useStyle = makeStyles(theme => ({
  siteName: {
    fontWeight: 900,
  },
  logoContainer: {
    position: "relative",
    width: "fit-content",
  },
  iconChip: {
    backgroundColor: "transparent",
    borderRadius: 0,
    paddingLeft: 1,
    paddingRight: 1,
    "&:hover": {
      cursor: "pointer"
    }
  },
  centered: {
    margin: "0 auto",
  },
  logoUnderline: {
    position: "absolute",
    left: 0,
    bottom: 2,
    height: 9,
    backgroundColor: theme.palette.secondary.main,
    width: "100%",
    zIndex: -1,
  },
  iconAvatar: {
    width: "2.5em",
    height: "2.5em",
    overflow: "initial",
    backgroundColor: "transparent",
    right: -4
  },
}));

function Core() {
  const classes = useStyle();

  return (
    <>
      <Chip avatar={<Avatar className={classes.iconAvatar} alt="icon"
                            src={"/static/base_app/favicon/favicon.svg"}/>}
            label={<Typography variant="h5" color="inherit"
                               className={classes.siteName}>REX-DRI</Typography>}
            className={classes.iconChip}
            color="primary"/>
      <div className={classes.logoUnderline}/>
    </>
  );
}

export default function Logo(props) {
  const classes = useStyle();
  return (
    <div className={classNames(classes.logoContainer, classes.centered)}>
      {
        props.linkTo ?
          <CustomNavLink to={props.linkTo}><Core/></CustomNavLink>
          :
          <Core/>
      }
    </div>
  );
}

Logo.defaultProps = {
  linkTo: ""
};

Logo.propTypes = {
  linkTo: PropTypes.string.isRequired,
};