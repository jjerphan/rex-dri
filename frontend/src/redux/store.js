import {applyMiddleware, createStore} from "redux";
import rootReducer from "./reducers/index";

import thunkMiddleware from "redux-thunk";
import {createLogger} from "redux-logger";

const loggerMiddleware = createLogger(
  {
    collapsed:
      (getState, action, logEntry) => { // eslint-disable-line no-unused-vars
        // const {type} = action;
        // if () {
        //   return false;
        // }
        return true;
      }
  });

let middlewares = [
  thunkMiddleware, // lets us dispatch() functions
];

// A bit of optimization for production builds
// eslint-disable-next-line no-undef
if (process.env.NODE_ENV !== "production") {
  middlewares.push(loggerMiddleware); // neat middleware that logs actions
}

const store = createStore(
  rootReducer,
  applyMiddleware(
    ...middlewares
  )
);


export default store;
