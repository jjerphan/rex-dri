import { apiActions } from "./buildApiActionsAndReducers";

// To ease auto completion, not really used here!
// eslint-disable-next-line no-unused-vars
import CrudActions from "./CrudActions";

/**
 * Function to get the the actions corresponding to an API end point.
 *
 * @export
 * @param {string} name
 * @returns {CrudActions}
 */
export default function getActions(name) {
  if (!(name in apiActions)) {
    console.error("available actions", apiActions); // eslint-disable-line no-console
    throw Error(`Requested api action is not defined, check the name: ${name}`);
  }
  return apiActions[name];
}
