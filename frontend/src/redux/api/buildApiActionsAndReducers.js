/*
 * This file dynamically create the redux actions and reducers related to the REST API and custom apis
 */

import CrudActions from "./CrudActions";
import CrudReducers from "./CrudReducers";


let apiActionsTmp = {};
let apiReducersTmp = {};

/**
 * Create api actions and reducers for given the info in each arr obj
 * Add those to apiActions and apiReducers objects
 *
 * @param {Array} arr
 */
function addAPIs(arr) {
  arr
    .forEach(route => {
      const actions = new CrudActions(route);
      apiActionsTmp[route] = actions;

      const reducers = new CrudReducers(route);
      apiReducersTmp[`${route}All`] = reducers.getForAll();
      apiReducersTmp[`${route}One`] = reducers.getForOne();
    });
}

// __AllBackendEndPointsRoutes is directly injected in the HTML page
// from the django template.
// eslint-disable-next-line no-undef
addAPIs(__AllBackendEndPointsRoutes);

export const apiActions = apiActionsTmp;
export const apiReducers = apiReducersTmp;
