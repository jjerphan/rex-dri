/**
 * This file contains utilities linked to the use of the API.
 */
import store from "../store";

// Stores the name of the reducers/actions that result in read data
export const successActionsWithReads = ["readSucceeded", "createSucceeded", "updateSucceeded"];

/**
 * Smartly retrieve the latest read (create and update included) data from the server
 *
 * @export
 * @param {object} stateExtract
 */
export function getLatestRead(stateExtract) {
  return successActionsWithReads
    .filter(action => action in stateExtract) // general handling of all types of API reducers
    .map(action => stateExtract[action])
    .reduce(
      (prev, curr) => prev.readAt < curr.readAt ? curr : prev,
      {readAt: 0});
}


/**
 * Function that returns the latest read data directly from the store.
 * To be used with parsimony.
 *
 * It assumes that the data has already been fetched at some point.
 *
 * @export
 * @param {string} entry eg: userDataOne
 * @returns
 */
export function getLatestReadDataFromStore(entry) {
  return getLatestRead(store.getState().api[entry]).data;
}
