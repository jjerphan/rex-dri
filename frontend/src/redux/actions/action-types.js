/*
 * This file contains action types for the custom redux behaviors (that are not REST related)
 */

// handling of full screen dialog and notifications in an original manner,
// ie common fot the entire app
export const OPEN_FULL_SCREEN_DIALOG = "OPEN_FULL_SCREEN_DIALOG";
export const CLOSE_FULL_SCREEN_DIALOG = "CLOSE_FULL_SCREEN_DIALOG";

// Other redux actions
export const SAVE_MAIN_MAP_STATUS = "SAVE_MAIN_MAP_STATUS";
export const SAVE_SELECTED_UNIVERSITIES = "SAVE_SELECTED_UNIVERSITIES";
