/*
 * This file contains the redux actions related to handling the app fullScreenDialog
 */

import {
  OPEN_FULL_SCREEN_DIALOG,
  CLOSE_FULL_SCREEN_DIALOG,
} from "./action-types";


/**
 * Action: Open the full screen dialog and pass attributes.
 *
 * @export
 * @param {Node} innerNodes
 * @returns {object}
 */
export function openFullScreenDialog(innerNodes) {
  return {
    type: OPEN_FULL_SCREEN_DIALOG,
    innerNodes
  };

}


/**
 * Action: Close the full screen dialog
 *
 * @export
 * @returns {object}
 */
export function closeFullScreenDialog() {
  return {
    type: CLOSE_FULL_SCREEN_DIALOG,
  };

}
