/*
 * This file contains the redux actions related to the filter component
 */

import {SAVE_SELECTED_UNIVERSITIES} from "./action-types";


/**
 * Action: save a new universities selection
 *
 * @export
 * @param {array} newSelection
 * @returns {object}
 */
export function saveSelectedUniversities(newSelection) {
  return {
    type: SAVE_SELECTED_UNIVERSITIES,
    newSelection
  };
}
