/*
 * This file contains the redux reducers related to the filter component
 */

import {SAVE_SELECTED_UNIVERSITIES} from "../actions/action-types";

/**
 * Reducer: Save the selected universities
 *
 * @export
 * @param {array} [state=[]]
 * @param {object} action
 * @returns
 */
export function saveSelectedUniversities(state = [], action) {
  if (action.type === SAVE_SELECTED_UNIVERSITIES) {
    return action.newSelection;
  } else {
    return state;
  }
}
