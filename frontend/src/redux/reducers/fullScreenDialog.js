/*
 * This file contains the redux reducers related to the app fullScreenDialog
 */

import React from "react";
import {
  OPEN_FULL_SCREEN_DIALOG,
  CLOSE_FULL_SCREEN_DIALOG
} from "../actions/action-types";

/**
 * Reducer for the fullScreenDialog
 *
 * @export
 * @param {object} state
 * @param {string} action
 * @returns
 */
export function fullScreenDialog(state = { open: false, innerNodes: <></> }, action) {
  switch (action.type) {
    case OPEN_FULL_SCREEN_DIALOG:
      return {
        open: true,
        innerNodes: action.innerNodes,
      };

    case CLOSE_FULL_SCREEN_DIALOG:
      return {
        open: false,
        innerNodes: <></>
      };

    default:
      return state;
  }
}
