import {combineReducers} from "redux";

import {fullScreenDialog} from "./fullScreenDialog";
import {saveSelectedUniversities} from "./filter";
import {apiReducers} from "../api/buildApiActionsAndReducers";

// Combine the reducers related to the app
const app = combineReducers({
  fullScreenDialog,
  selectedUniversities: saveSelectedUniversities,
});

// Get the redux reducer for all api related stuff
const api = combineReducers(apiReducers);

// Create the final reduce reducer
const rootReducer = combineReducers({
  api,
  app
});

export default rootReducer;
